﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Citzen : MonoBehaviour
{
    public GlobalGameSelections itemScript;
    public Transform playerToFollow;

    Transform exitToFollow;
    Rigidbody rig;
    Animator anim;

    public float speed = 0;
    public float distanceToStop = 0;
    public float life = 100;
    
    public bool startQuest = false;
    bool followExited = false;
    bool isDead = false;

    void Start()
    {
        rig = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();       
    }

    void Update()
    {
        /*if (itemScript.multiplayerSelected && startQuest)
        {
            speed = 0.05f;
        }*/

        if (startQuest && life > 0)
        {
            if(playerToFollow != null)
            {
                float dis = Vector3.Distance(transform.position, playerToFollow.position);

                if (dis < distanceToStop && dis > 1)
                {
                    Vector3 targetPostition = new Vector3(playerToFollow.position.x,
                                                this.transform.position.y,
                                                playerToFollow.position.z);
                    this.transform.LookAt(targetPostition);
                    anim.SetBool("Running", true);

                    //transform.position = Vector3.Lerp(transform.position, playerToFollow.position, speed * Time.deltaTime);
                }
                else
                {
                    anim.SetBool("Running", false);
                }
            }             
        }

        if (followExited)
        {
            exitToFollow = GameObject.Find("Exit").transform;

            Vector3 targetPostition = new Vector3(exitToFollow.position.x,
                                            this.transform.position.y,
                                            exitToFollow.position.z);
            this.transform.LookAt(targetPostition);
            anim.SetBool("Running", true);

            //transform.position = Vector3.Lerp(transform.position, exitToFollow.position, speed * Time.deltaTime);
        }

        if(life <= 0)
        {
            if (!isDead)
            {
                GameObject.Find("Mission Canvas").GetComponent<MissionScript>().missionFailed = true;
                anim.SetBool("Dying", true);
                isDead = true;
            }
            
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("Dying") && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
            {
                Destroy(this.gameObject);
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Objective")
        {
            startQuest = false;
            followExited = true;            
        }

        if(other.name == "Exit")
        {
            GameObject.Find("Mission Canvas").GetComponent<MissionScript>().missionCompleted = true;
            GameObject.Find("Mission Canvas").GetComponent<MissionScript>().damagePower = true;
            Destroy(this.gameObject);
        }

        if(other.tag == "LowDamageBox")
        {
            life -= 20;
        }
    }
}
