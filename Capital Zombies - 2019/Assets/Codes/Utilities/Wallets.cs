﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallets : MonoBehaviour
{
    MissionScript missionScript;
    public string walletType = "";
    void Start()
    {
        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player1" || other.tag == "Player2" || 
            other.tag == "Player3" || other.tag == "Player4")
        {
            HeroBehavior playerID = other.gameObject.GetComponent<HeroBehavior>();

            if (walletType == "Health")
            {
                if (playerID.isInMission && missionScript.getWalletMission)//Missao maleta de vida
                {
                    missionScript.walletsCount++;
                }

                playerID.isRecovering = true;

                if (playerID.isZombie)
                {
                    playerID.howManyZombiesContacts = 0;
                    playerID.headIconsUI.gameObject.SetActive(false);

                    playerID.infectionAnimScript.humanAgain = true;
                    playerID.animToInfection = false;
                }
            }
            else if(walletType == "Ammo")
            {
                if (playerID.isInMission && missionScript.getAmmoWallet)//Missao maleta de municao
                {
                    missionScript.ammoWalletsCount++;
                }

                playerID.akAmmo = playerID.heroData.ak47Ammo;
                playerID.m4Ammo = playerID.heroData.m4Ammo;
                playerID.stonerAmmo = playerID.heroData.stonerAmmo;
                playerID.g28Ammo = playerID.heroData.g28Ammo;
                playerID.awpAmmo = playerID.heroData.awpAmmo;
                playerID.scoutAmmo = playerID.heroData.scoutAmmo;
                playerID.shotgunAmmo = playerID.heroData.shotgunAmmo;
                playerID.mp5Ammo = playerID.heroData.mp5Ammo;
                playerID.mp40Ammo = playerID.heroData.mp40Ammo;
                playerID.bizonAmmo = playerID.heroData.bizonAmmo;
                playerID.glockAmmo = playerID.heroData.glockAmmo;
                playerID.coltAmmo = playerID.heroData.coltAmmo;
                playerID.p38Ammo = playerID.heroData.p38Ammo;
                playerID.coltAnaAmmo = playerID.heroData.coltAnaAmmo;
                playerID.socomAmmo = playerID.heroData.socomAmmo;
                playerID.grenadeLimit = playerID.heroData.grenadeQuant;

                playerID.ammoIconValueForRifle = 385;
                playerID.ammoIconValueForPistol = 385;

                playerID.showAmmoParticle = true;
            }
            
            Destroy(this.gameObject);
        }
    }
}
