﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateItens : MonoBehaviour
{
    public float speed = 1;
    public bool isArrow = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isArrow)
        {
            transform.Rotate(Vector3.forward * speed);
        }
        
    }
}
