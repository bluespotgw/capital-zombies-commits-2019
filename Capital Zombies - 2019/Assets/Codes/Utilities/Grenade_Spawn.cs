﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade_Spawn : MonoBehaviour
{
    [Header("UTILITIES VALUE:")]
    public GameObject grenade;
    public float grenadeForce = 5f;

    [Header("HERO BEHAVIOR SCRIPT:")]
    public HeroBehavior heroBehavior;

    Rigidbody rig;
      
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    void Update()
    {
        if(heroBehavior.utilitieForce == "Weak")
        {
            grenadeForce = 5;
        }
        if (heroBehavior.utilitieForce == "Strong")
        {
            grenadeForce = 15;
        }

        if (this.gameObject.activeSelf)
        {
            GameObject obj = (GameObject)Instantiate(grenade, transform.position, transform.rotation);
            rig = obj.GetComponent<Rigidbody>();
            rig.AddForce(-transform.up * grenadeForce, ForceMode.VelocityChange);
            this.gameObject.SetActive(false);
        }
    }
}
