﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeSpawn : MonoBehaviour
{
    [Header("UTILITIES VALUE:")]
    public GameObject[] grenades;
    public float grenadeForce = 5f;

    [Header("HERO BEHAVIOR SCRIPT:")]
    public HeroBehavior heroBehavior;

    //Rigidbody rig;
      
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    void Update()
    {
        if(heroBehavior.utilitieForce == "Weak")
        {
            grenadeForce = 5;
        }
        if (heroBehavior.utilitieForce == "Strong")
        {
            grenadeForce = 15;
        }

        if (this.gameObject.activeSelf)
        {
            GameObject obj = (GameObject)Instantiate(grenades[heroBehavior.grenadeSelected], transform.position, transform.rotation);//***
            Rigidbody rig = obj.GetComponent<Rigidbody>();
            Grenades grenScript = obj.GetComponent<Grenades>();

            grenScript.playerWhoThrow = transform.root.GetChild(1).tag;
            rig.AddForce(-transform.up * grenadeForce, ForceMode.VelocityChange);
            heroBehavior.grenadeLimit--;
            this.gameObject.SetActive(false);
        }
    }
}
