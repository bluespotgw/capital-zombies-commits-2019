﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenades : MonoBehaviour
{
    [Header("UTILITIES PARTICLES:")]
    public GameObject grenadeParticle;
    public GameObject smokePreOpeningParticles;

    MissionScript missionScript;
    Rigidbody rig;
    private bool start = false;
    private bool destroy = false;

    public bool isHE = false;
    public string playerWhoThrow = ""; //Identificacao para dar dano nos outros players.O script do spawner passa essa info

    void Start()
    {
        rig = GetComponent<Rigidbody>();
        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
        StartCoroutine(CompareVelocity());//Usando um timer para comparar a velocidade da granada só após ela ser lançada
    }

    private void FixedUpdate()
    {
        if (start)
        {
            GrenadeOut();
        }

        //Quando a velocidade for menor que o pedido e a variavel for true a granada é destruída
        if (destroy && rig.velocity.magnitude < 0.4f)
        {
            Destroy(this.gameObject);
        }
    }

    private void GrenadeOut()
    {
        if(rig.velocity.magnitude < 0.5f)
        {          
            if (isHE)
            {
                Instantiate(grenadeParticle, transform.position, transform.rotation);
                Collider[] allChars = Physics.OverlapSphere(transform.position, 3.5f);

                foreach(Collider col in allChars)
                {
                    HeroBehavior heroScr = col.GetComponent<HeroBehavior>();
                    StandardZombie zombieScr = col.GetComponent<StandardZombie>();
                    BigZ specialEnemyBigZ = col.GetComponent<BigZ>();

                    if (heroScr != null)
                    {
                        float dist = Vector3.Distance(heroScr.transform.position, this.transform.position);
                        heroScr.life -= heroScr.heroData.heDamage/dist;
                        
                        if(heroScr.life <= 0)
                        {
                            heroScr.oponentTag = playerWhoThrow;
                            heroScr.enemyGunType = "Utilities";
                            heroScr.enemyGunID = 1;
                        }

                        //Missao granada
                        if (GameObject.FindWithTag(playerWhoThrow).GetComponent<HeroBehavior>().isInMission && missionScript.throwGrenadeMission)
                        {
                            missionScript.grenadesEnemyCount++;
                        }
                    }

                    if(zombieScr != null)
                    {
                        float dist = Vector3.Distance(zombieScr.transform.position, this.transform.position);
                        zombieScr.lifeEnemy -= 200f / dist;

                        //Missao granada
                        if (GameObject.FindWithTag(playerWhoThrow).GetComponent<HeroBehavior>().isInMission && missionScript.throwGrenadeMission)
                        {
                            missionScript.grenadesEnemyCount++;
                        }

                        if(zombieScr.lifeEnemy <= 0)
                        {
                            zombieScr.RifleShoot();
                        }
                    }

                    if(specialEnemyBigZ != null)
                    {
                        float dist = Vector3.Distance(specialEnemyBigZ.transform.position, this.transform.position);
                        specialEnemyBigZ.lifeBigZ -= 200f / dist;

                        //Missao granada
                        if (GameObject.FindWithTag(playerWhoThrow).GetComponent<HeroBehavior>().isInMission && missionScript.throwGrenadeMission)
                        {
                            missionScript.grenadesEnemyCount++;
                        }
                    }
                }
            }
            else
            {
                Instantiate(smokePreOpeningParticles, transform.position, transform.rotation);
                GameObject smoke = Instantiate(grenadeParticle, transform.position, transform.rotation);
                smoke.GetComponent<ID>().objID = playerWhoThrow;
            }

            start = false;
            destroy = true;
        }               
    }

    IEnumerator CompareVelocity()
    {
        yield return new WaitForSeconds(1.5f);
        start = true;
    }
}
