﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{
    [Header("PLAYER AND CAM ROOT GAMEOBJECTS:")]
    public GameObject player;
    public GameObject camRoot;

    [Header("SCRIPTS:")]
    public HeroBehavior heroBehavior;
    public HeroData heroData;
    public CameraData cameraData;

    private Vector3 offset;//Limite da camera
    private Vector3 newPos;//Posição da camera se movendo
    private Quaternion oldRot;//Guarda a rotação da camera antes de iniciar o jogo
    
    void Start()
    {
        offset = transform.position - player.transform.position;
        oldRot = transform.rotation;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {       
        //Faz a camera seguir o personagem sem parar
        camRoot.transform.position = player.transform.position + offset;
        if (heroBehavior.rotateCamera)
        {
            transform.LookAt(player.transform.position);
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, oldRot, 0.05f);
        }
        

        float joyHor = Input.GetAxis(heroData.rightJoystickHorizontalAxis);
        float joyVert = Input.GetAxis(heroData.rightJoystickVerticalAxis);

        //Quando o player vai atirar e o mouse for usado,o camRoot move sua posição de acordo com a posição do mouse
        if (Input.GetMouseButton(0) && heroBehavior.keyboardActivated && !heroBehavior.isGrabbing)
        {                        
            newPos.x += Input.GetAxis(heroData.mouseHorizontalAxis) * cameraData.cameraMouseSpeed * Time.deltaTime;
            newPos.z += Input.GetAxis(heroData.mouseVerticalAxis) * cameraData.cameraMouseSpeed * Time.deltaTime;

            newPos.x = Mathf.Clamp(newPos.x, -cameraData.clampMouseLimit.x, cameraData.clampMouseLimit.x);
            newPos.z = Mathf.Clamp(newPos.z, -cameraData.clampMouseLimit.y, cameraData.clampMouseLimit.y);

            transform.localPosition = Vector3.Lerp(transform.localPosition, newPos, cameraData.lerpTime);
        }

        else if (heroBehavior.keyboardActivated && heroBehavior.isGrabbing)
        {
            newPos.x += Input.GetAxis(heroData.mouseHorizontalAxis) * cameraData.cameraMouseSpeed * Time.deltaTime;
            newPos.z += Input.GetAxis(heroData.mouseVerticalAxis) * cameraData.cameraMouseSpeed * Time.deltaTime;

            newPos.x = Mathf.Clamp(newPos.x, -cameraData.clampMouseLimit.x, cameraData.clampMouseLimit.x);
            newPos.z = Mathf.Clamp(newPos.z, -cameraData.clampMouseLimit.y, cameraData.clampMouseLimit.y);

            transform.localPosition = Vector3.Lerp(transform.localPosition, newPos, cameraData.lerpTime);
        }

        //Quando o player vai atirar e o joystick for ativo,o camRoot move sua posição de acordo com a posição do joystick
        else if (joyHor != 0 || joyVert != 0)
        {
            if(heroBehavior.joystickActivated)
            {
                newPos.x += joyHor * cameraData.cameraJoystickSpeed * Time.deltaTime;
                newPos.z += joyVert * cameraData.cameraJoystickSpeed * Time.deltaTime;

                if (heroBehavior.rotateCamera)
                {
                    newPos.x = Mathf.Clamp(newPos.x, -cameraData.rotationClampJoystick.x, cameraData.rotationClampJoystick.x);
                    newPos.z = Mathf.Clamp(newPos.z, -cameraData.rotationClampJoystick.y, cameraData.rotationClampJoystick.y);
                }
                else
                {
                    newPos.x = Mathf.Clamp(newPos.x, -cameraData.clampJoystickLimit.x, cameraData.clampJoystickLimit.x);
                    newPos.z = Mathf.Clamp(newPos.z, -cameraData.clampJoystickLimit.y, cameraData.clampJoystickLimit.y);
                }
               
                transform.localPosition = Vector3.Lerp(transform.localPosition, newPos, cameraData.lerpTime);               
            }
        }
        else
        {
            //Quando não for mais atirar,o camRoot volta a posição normal
            transform.position = Vector3.Lerp(transform.position,camRoot.transform.position, 0.05f);
            newPos = new Vector3(0, 0, 0);
        }
    }
}

