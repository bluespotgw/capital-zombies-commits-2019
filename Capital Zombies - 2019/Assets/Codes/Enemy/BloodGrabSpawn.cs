﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodGrabSpawn : MonoBehaviour
{
    public GameObject bloodPart;
    public GameObject bloodSpawner;

    public void SpawnBlood()
    {
        Instantiate(bloodPart, bloodSpawner.transform.position, bloodSpawner.transform.rotation);
    }
}
