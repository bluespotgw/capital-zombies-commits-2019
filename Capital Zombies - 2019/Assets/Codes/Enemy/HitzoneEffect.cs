﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitzoneEffect : MonoBehaviour
{
    public GameObject hitPart;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "DamageBox")
        {
            Instantiate(hitPart, transform.position, transform.rotation);
        }
    }
}
