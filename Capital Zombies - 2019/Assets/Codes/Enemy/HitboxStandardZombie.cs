﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxStandardZombie : MonoBehaviour
{
    public GameObject zombie;
    public StandardZombie zombieScript;
    public float damageValue = 0;
    public float multiplierDamage = 0;
    public GameObject whoShooting;
    public bool inHead = false;
    public GameObject brainParticles;
    public GameObject bloodParticles;

    void Start()
    {
        zombieScript = zombie.GetComponent<StandardZombie>();
    }

    public void ReceiveDamage()
    {
        zombieScript.lifeEnemy -= damageValue * multiplierDamage;
        zombieScript.whoShooting = whoShooting;
        zombieScript.RifleShoot();

        if (inHead)
        {
            Instantiate(brainParticles, transform.position, transform.rotation);
        }
        else
        {
            Instantiate(bloodParticles, transform.position, transform.rotation);
        }
    }
}
