﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StandardZombie : MonoBehaviour
{
    [Header("LIFE:")]
    public float lifeEnemy = 100;

    [Header("STOP DISTANCE LIMIT:")]
    public float distanceLimitStop = 0;
    public float timeToLerpIdleAnim = 2f;
    float timerLerp = 0;
    float idleActualValue = 0;
    float idleChooser = 0;
    float lerpTime = 0;

    [Header("LAYERMASK RAYCAST:")]
    public LayerMask rayMask;

    [Header("RAYCAST DISTANCE AND OBJECT:")]
    public float rayDistance = 0;
    public GameObject rayHead;
    public GameObject rayFoots;

    [Header("ENEMY STATUS:")]
    public bool idle = false;
    public bool run = false;
    public bool walk = false;
    public bool walk2 = false;
    public bool jump = false;
    public bool hangWall = false;
    public bool headRotateToPlayer = false;
    public bool pistolMission = false;
    bool isDead = false;
    bool isCrawling = false;
    bool isFalling = false;
    bool isInTop = false;
    bool isOnGrab = false;
    bool normalPosition = false;
    bool hanged = false;
    bool attacking = false;
    bool attackFinished = true;
    
    [Header("STUMBLE COLLIDER:")]
    public GameObject stumbleObj;

    [Header("RAGDOLL:")]
    public GameObject ragDoll;

    [Header("FOOT COLLIDER:")]
    public GameObject headObj;
    public GameObject footObj;
    public SphereCollider headCol;  
    public BoxCollider footCol;

    [Header("MAP ICON:")]
    public GameObject mapIcon;

    [Header("DEATH TIME:")]
    bool startDeath = false;
    public float downToFloor = 5f;
    public float floorToDeath = 2f;

    [Header("SPECIAL MISSION ZOMBIE:")]
    public bool specialZombie = false;//Pra missão do zombie
    public GameObject blueArrow;
    public GameObject whoShooting;

    bool lookToPlayer = false;
    bool researchTarget = true;//Se tiver infectado procurar outro player

    public GameObject player; // O script de IK coleta o player daqui
    public string targetTag = "";

    GameObject gameDirectorObject;
    GameDirector directorScript;

    string[] fallAnimations = { "ReleaseWall1", "ReleaseWall2" };
    int fallChoosed = 0;

    //Medir a distancia do player para o zumbi
    float dist = 0;

    // Tipos de ataques do zumbi;
    float[] attacksTypes = {0, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f};
    public int attackChoosed = 0;

    //Missao do veneno
    bool breathPoison = false;
    MissionScript missionScript;

    //Componentes do Inimigo
    NavMeshAgent navi;
    Animator anim;
    Rigidbody rig;
    CapsuleCollider capsCol;

    HeroBehavior heroBehavior;
    
    void Start()
    {
        navi = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        rig = GetComponent<Rigidbody>();
        capsCol = GetComponent<CapsuleCollider>();
        footCol = footObj.GetComponent<BoxCollider>();
        headCol = headObj.GetComponent<SphereCollider>();
        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();

        //Coleta o script controlador da cena automaticamente
        gameDirectorObject = GameObject.FindWithTag("Director");
        directorScript = gameDirectorObject.GetComponent<GameDirector>();      

        stumbleObj.SetActive(false);
        rayFoots.SetActive(false);
        distanceLimitStop = Random.Range(5, 8);
        timerLerp = timeToLerpIdleAnim;//Tempo de interpolação das animações idle
        directorScript.totalEnemies += 1; // Modifica o placar do contador
    }

    // Update is called once per frame
    private void Update()
    {
        if (player == null)
        {
            //Depois coleta o player que estiver na cena atual. Deve ser de forma aleatória
            player = GameObject.FindWithTag(directorScript.playersTag[Random.Range(0, directorScript.joystickOrderSelect.Length)]);
            heroBehavior = player.GetComponent<HeroBehavior>();

            if (heroBehavior.isZombie)
            {
                player = null;
            }
        }

        dist = Vector3.Distance(player.transform.position, transform.position);

        if (dist < distanceLimitStop && !isOnGrab && idle && heroBehavior.playerIsOnTop && !attacking)
        {
            anim.SetBool("Idle", true);
 
            float[] lerpIdleValues = { 0, 0.25f, 0.5f, 0.75f, 1 };

            timerLerp -= Time.deltaTime;
            lerpTime += 1f * Time.deltaTime;

            if (timerLerp <= 0)
            {
                timerLerp = timeToLerpIdleAnim;
                lerpTime = 0;
                idleActualValue = idleChooser;
                idleChooser = lerpIdleValues[Random.Range(0, lerpIdleValues.Length)];               
            }

            anim.SetFloat("Idle_Blend", Mathf.SmoothStep(idleActualValue, idleChooser, lerpTime));

            if (run)
            {
                anim.SetBool("Run", false);
            }
            if (walk)
            {
                anim.SetBool("Walk", false);
            }
            if (walk2)
            {
                anim.SetBool("Walk2", false);
            }
        }
        else
        {
            if (!jump)
            {
                anim.SetBool("Jump", false);
                anim.SetBool("Idle", false);
                rig.constraints &= ~RigidbodyConstraints.FreezePositionX;
                rig.constraints &= ~RigidbodyConstraints.FreezePositionZ;
                rig.constraints &= RigidbodyConstraints.FreezeRotation;

                if (lifeEnemy > 0 && navi.enabled == true)
                {
                    navi.SetDestination(player.transform.position);
                }

                if (run && !attacking)
                {
                    anim.SetBool("Run", true);
                }
                if (walk && !attacking)
                {
                    anim.SetBool("Walk", true);
                }
                if (walk2 && !attacking)
                {
                    anim.SetBool("Walk2", true);
                }
            }
            else
            {
                anim.SetBool("Jump", true);
                navi.enabled = false;
                rig.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;

                if (run && !attacking)
                {
                    anim.SetBool("Run", false);
                }
                if (walk && !attacking)
                {
                    anim.SetBool("Walk", false);
                }
                if (walk2 && !attacking)
                {
                    anim.SetBool("Walk2", false);
                }
            }

            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(0).IsName("Knockback_Tree"))
            {
                anim.SetBool("KnockBack", false);
            }

            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5 && anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_StandingUp"))
            {
                anim.SetBool("Stumble", false);
            }

            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(0).IsName("Attack_Tree"))
            {
                anim.SetBool("Attack", false);
                attackChoosed = 0;
                attackFinished = true;

                if (!isInTop)
                {
                    navi.enabled = true;
                }

                if (run)
                {
                    anim.SetBool("Run", true);
                }
                if (walk)
                {
                    anim.SetBool("Walk", true);
                }
                if (walk2)
                {
                    anim.SetBool("Walk2", true);
                }
            }

            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(0).IsName("Kill_Tree") ||
                anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_ProneDeath"))
            {
                rig.constraints = RigidbodyConstraints.FreezeRotation;
                stumbleObj.SetActive(true);
            }

            GrabToTop();
            RayForClimber();
            RayToFall();
        }

        if(lifeEnemy <= 0)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(0).IsName("Kill_Tree"))
            {
                footCol.isTrigger = false;
                headCol.isTrigger = false;
                capsCol.enabled = false;
            }
        }

        if (startDeath)
        {
            downToFloor -= Time.deltaTime;
            if(downToFloor <= 0)
            {
                downToFloor = 0;
                footCol.isTrigger = true;
                headCol.isTrigger = true;
                floorToDeath -= Time.deltaTime;

                if(floorToDeath <= 0)
                {
                    Destroy(this.gameObject);
                }
            }
        }

        Debug.DrawRay(rayFoots.transform.position, -rayFoots.transform.up * 1f,Color.black);
        Debug.DrawRay(rayHead.transform.position, rayHead.transform.forward * rayDistance, Color.black);
    }

    public void RifleShoot()
    {
        if (lifeEnemy <= 0 && !isDead)
        {
            if (!isOnGrab)
            {
                isDead = true;
                startDeath = true;
                float[] killFloats = { 0, 0.2f, 0.4f, 0.6f, 0.8f, 1 };
                int choosedKill = Random.Range(0, killFloats.Length);
                anim.SetBool("Kill", true);
                anim.SetFloat("Kill_Blend", killFloats[choosedKill]);
                navi.enabled = false;
                lookToPlayer = false;
                directorScript.totalEnemies -= 1;
                Destroy(mapIcon.gameObject);

                //Missao Special Zombie - Qlqr um pode matar e cumprir a missao pro outro player
                if (missionScript.specialZombieMission)
                {
                    missionScript.specialCount++;
                }

                //Missao jump/kill
                HeroBehavior missionPlayer = whoShooting.GetComponent<HeroBehavior>();
                if (missionPlayer.isInMission && missionPlayer.jump && missionScript.jumpAndKillMission)
                {
                    missionScript.missionCompleted = true;
                    missionScript.speedPower = true;
                }

                //Missao nao mate nenhum zumbi
                if (missionPlayer.isInMission && missionScript.dontKillZombiesMission)
                {
                    missionScript.missionFailed = true;
                }

                if (!run)
                {
                    ZombieIK ikScript = GetComponent<ZombieIK>();
                    ikScript.enabled = false;
                }
                
            }
            else
            {
                Instantiate(ragDoll, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
            
        }

        if (lifeEnemy > 0)
        {
            anim.SetBool("KnockBack", true);
            anim.SetFloat("Knock_Tree", 0.5f);
            isFalling = true;
        }
    }

    public void RayForClimber()
    {
        RaycastHit hit;
        if (Physics.Raycast(rayHead.transform.position, rayHead.transform.forward, out hit, rayDistance,rayMask))
        {
            if (hit.collider.tag == "ZombieGrabZone" && heroBehavior.isGrabbing ||
                hit.collider.tag == "ZombieGrabZone" && heroBehavior.playerIsOnTop)
            {
                isOnGrab = true;
                rayDistance = 2f;

                if (!normalPosition)
                {
                    transform.rotation = Quaternion.FromToRotation(Vector3.forward,-hit.normal);
                    //this.transform.position = Vector3.Lerp(transform.position, new Vector3(hit.point.x - 0.01f, this.transform.position.y, hit.point.z - 0.01f), 0.01f);
                    this.transform.position = new Vector3(hit.point.x, this.transform.position.y, hit.point.z);
                }

                if (!hangWall)
                {
                    jump = true;                    
                }
                else
                {
                    if(dist < 5 && !hanged)
                    {
                        hanged = true;
                        anim.SetBool("HangJump", true);
                        navi.enabled = false;
                        rig.useGravity = false;
                    }                 
                }
            }

            if (hit.collider.tag == "ZombieGrabZone" && !heroBehavior.isGrabbing && !heroBehavior.playerIsOnTop && !isInTop)
            {
                anim.SetBool("HangJump", false);
                fallChoosed = Random.Range(0, fallAnimations.Length);
                anim.SetBool(fallAnimations[fallChoosed], true);
                isOnGrab = true;
                isInTop = true;
            }

            if (hit.collider.tag == "TopGrabber" && !isInTop && !heroBehavior.isGrabbing && heroBehavior.playerIsOnTop)//***
            {               
                anim.SetBool("HangJump", false);
                anim.SetBool("Climb", true);
                isOnGrab = false;
                isInTop = true;
            }

            if (hit.collider.tag == "TopGrabber" && heroBehavior.isGrabbing ||
                hit.collider.tag == "TopGrabber" && !heroBehavior.isGrabbing && !heroBehavior.playerIsOnTop)//***
            {
                anim.SetBool("HangJump", false);
                anim.SetBool("Climb", false);
                anim.SetBool(fallAnimations[fallChoosed], true);
                isOnGrab = false;
            }
        }
    }

    public void RayToFall()
    {
        RaycastHit hit;
        if(Physics.Raycast(rayFoots.transform.position, -rayFoots.transform.up,out hit, 1f))
        {
            if(hit.collider.tag != "TopGrabber")
            {
                
            }
        }
        else
        {
            if (rayFoots.activeInHierarchy && !isFalling)
            {
                anim.SetBool("Fall", true);
                anim.SetBool(fallAnimations[fallChoosed], false);
                anim.applyRootMotion = true;
            }            
        }
    }

    public void GrabToTop()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5 && anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_StandingUp"))
        {
            rayFoots.SetActive(true);
            lookToPlayer = true;
        }

        if (lookToPlayer)
        {
            transform.LookAt(new Vector3(player.transform.position.x, this.transform.position.y, player.transform.position.z));
            //anim.SetBool("Fall", false);
        }
    }

    public void ZombieAttack()
    {
        if (!isOnGrab && lifeEnemy > 0) 
        {
            attacking = true;
            navi.enabled = false;
           
            anim.SetBool("Attack", true);
            attackChoosed = Random.Range(0, attacksTypes.Length);
            anim.SetFloat("Attack_Blend", attacksTypes[attackChoosed]);
            transform.LookAt(new Vector3(player.transform.position.x, this.transform.position.y, player.transform.position.z));

            if (run)
            {
                anim.SetBool("Run", false);
            }
            if (walk)
            {
                anim.SetBool("Walk", false);
            }
            if (walk2)
            {
                anim.SetBool("Walk2", false);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "DeadStumble" && lifeEnemy >= 50)
        { 
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Run"))
            {
                int fallStumble = Random.Range(0, 4);

                if (fallStumble == 3)
                {
                    anim.SetBool("Stumble", true);
                    anim.SetBool("StandUp", true);
                }
            }
        }

        if (other.gameObject.tag == "DeadStumble" && lifeEnemy < 50)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Run"))
            {
                int fallStumble = Random.Range(0, 4);

                if (fallStumble == 3)
                {
                    anim.SetBool("Stumble", true);
                    anim.SetBool("StandUp", false);
                }
            }
        }

        if (other.gameObject.tag == player.tag && attackFinished && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Climb_Up") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_StandingUp") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Jump") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Hang_Wall") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Climbing_Wall") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Wall_Release") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Wall_Release_2") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Jump_Of_Top") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Fall"))
        {
            attackFinished = false;
            ZombieAttack();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            anim.SetBool("HangJump", false);
        }

        if (collision.gameObject.tag == "Ground" && heroBehavior.isGrabbing && jump)
        {
             navi.enabled = false;           
        }

        if (collision.gameObject.tag == "Ground" && !heroBehavior.isGrabbing && lifeEnemy > 0 ||
            collision.gameObject.tag == "Ground" && heroBehavior.isGrabbing && lifeEnemy > 0)
        {
            rayDistance = 0.6f;
            hanged = false;
            isOnGrab = false;
            jump = false;           
            isFalling = false;
            isInTop = false;            
            normalPosition = false;
            navi.enabled = true;
            rayFoots.SetActive(false);
            anim.SetBool("Fall", false);
            anim.SetBool(fallAnimations[fallChoosed], false);
        }

        if(collision.gameObject.tag == player.tag && attackFinished && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Climb_Up") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_StandingUp") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Jump") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Hang_Wall") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Climbing_Wall") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Wall_Release") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Wall_Release_2") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Jump_Of_Top") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Zombie_Fall"))
        {
            attackFinished = false;
            ZombieAttack();
        }
    }

    private void OnAnimatorMove()
    {
         if (!isFalling)
         {
             anim.ApplyBuiltinRootMotion();
         }

         if (!jump)
         {
             if(walk || walk2)
             {
                 navi.velocity = anim.deltaPosition / Time.deltaTime;
             }
             else
             {
                 navi.velocity = anim.deltaPosition * Time.deltaTime;
             }
         }
    }

    public void DeactivateRootMotion()// Quando o Zumbi se soltar da parede ele deve cair 
    {
        isFalling = true;
        anim.applyRootMotion = false;
        rig.useGravity = true;
        capsCol.isTrigger = false;
        anim.SetBool(fallAnimations[fallChoosed], false);
    }

    public void FallingOnTop()
    {
        rig.useGravity = true;
        lookToPlayer = false;
        capsCol.isTrigger = false;
        isFalling = true;
        anim.applyRootMotion = false;
        anim.SetBool("Climb", false);
    }

    public void TurnTrigger()
    {
        capsCol.isTrigger = true;
    }

    private void OnParticleCollision(GameObject other)
    {
        if(other.tag == "Smoke")
        {
            ID smokeID = other.gameObject.GetComponent<ID>();
            lifeEnemy -= 1;

            if(lifeEnemy <= 0)
            {
                RifleShoot();                
            }

            if (!breathPoison && lifeEnemy > 0)
            {
                if (missionScript.throwGrenadeMission == true && GameObject.FindWithTag(smokeID.objID).GetComponent<HeroBehavior>().isInMission)
                {
                    missionScript.grenadesEnemyCount++;
                }

                breathPoison = true;
            }
        }
    }
}
