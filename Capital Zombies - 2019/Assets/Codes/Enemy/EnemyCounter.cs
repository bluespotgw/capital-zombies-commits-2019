﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCounter : MonoBehaviour
{
    public int totalEnemies = 0;
    public Text counterText;
    GameObject[] zombies;

    void Start()
    {
        /*zombies = GameObject.FindGameObjectsWithTag("ZombiePrefab");
        totalEnemies = zombies.Length;*/
    }

    void Update()
    {
        counterText.text = totalEnemies.ToString();
    }
}
