﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BigZ : MonoBehaviour
{
    public GameObject playerToFollow;
    public float speed = 0;
    public float distanceToWalk = 0;
    public float distanceToAttack = 0;
    public float lifeBigZ = 400;
    int attackChooosed = 0;
    NavMeshAgent nav;
    Animator anim;
    MissionScript missionScript;
    bool isAttack = false;
    bool firstAttack = false;
    bool isDead = false;
    bool dest = false;
    bool acessMissionScript;
    float dis = 0;
    float goDownTime = 3;
    float goDestroy = 3;

    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAttack && !isDead)
        {
            if (dis < distanceToWalk && dis > distanceToAttack)
            {
                anim.SetBool("Walk", true);
                nav.speed = 1f;
            }
            else
            {
                anim.SetBool("Walk", false);
                nav.speed = 4f;
            }
        }       

        if (dis < distanceToAttack)
        {
            isAttack = true;
        }
        else
        {
            isAttack = false;
            firstAttack = false;
            attackChooosed = 0;
            anim.SetInteger("Attack", 0);
        }

        if (isAttack && !isDead)
        {
            if (!firstAttack)
            {
                attackChooosed = Random.Range(1, 3);
                firstAttack = true;
            }

            if(anim.GetCurrentAnimatorStateInfo(0).IsName("Chefao_MarretandoLateral") ||
                anim.GetCurrentAnimatorStateInfo(0).IsName("Chefao_MarretandoFrente"))
            {
                attackChooosed = Random.Range(1, 3);
            }
            
            nav.speed = 0.1f;
            anim.SetInteger("Attack", attackChooosed);
            print("ok");
        }

        if (isDead && !acessMissionScript)
        {
            anim.SetBool("Dead", true);
            nav.enabled = false;
            missionScript.specialECount++;           
            acessMissionScript = true;
        }
        else
        {
            if(playerToFollow != null && !acessMissionScript)
            {
                nav.SetDestination(playerToFollow.transform.position);
                dis = Vector3.Distance(transform.position, playerToFollow.transform.position);
            }                       
        }

        if (lifeBigZ <= 0)
        {
            isDead = true;

            goDownTime -= Time.deltaTime;
            print(goDownTime);

            if (goDownTime <= 0)
            {
                CapsuleCollider col = GetComponent<CapsuleCollider>();
                col.enabled = false;

                goDestroy -= Time.deltaTime;

                if (goDestroy <= 0)
                {
                    Destroy(this.gameObject);
                }
            }  
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.tag == "Smoke")
        {
            lifeBigZ -= 0.25f;
        }
    }
}
