﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieIK : MonoBehaviour
{
    [Header("HEAD TARGET:")]
    GameObject target;
    public Transform head;

    public float spineWeight = 0;
    public float headWeight = 0;

    Animator anim;
    StandardZombie stanZombieScript;

    void Start()
    {
        anim = GetComponent<Animator>();
        stanZombieScript = GetComponent<StandardZombie>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (stanZombieScript.headRotateToPlayer)
        {
            target = stanZombieScript.player;
            anim.SetBoneLocalRotation(HumanBodyBones.Head, new Quaternion(head.transform.localRotation.x, head.transform.localRotation.y, target.transform.position.y, headWeight));
            anim.SetBoneLocalRotation(HumanBodyBones.Spine, new Quaternion(head.transform.localRotation.x, head.transform.localRotation.y, target.transform.position.y, spineWeight));
        }       
    }
}
