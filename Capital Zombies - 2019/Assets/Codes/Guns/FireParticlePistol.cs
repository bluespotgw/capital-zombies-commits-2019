﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireParticlePistol : MonoBehaviour
{
    public GameObject fireParticlePistol;
    public ParticleSystem trailParticle;
    public GameObject bulletParticle;
    public GameObject[] bulletsSpawn;
    public GameObject heroObj;
    public HeroBehavior heroBehavior;
    Animator heroAnim;
    
    private void Start()
    {
        heroAnim = heroObj.GetComponent<Animator>();
    }

    private void Update()
    {
        //Previne de sair da animaçao de tiro com a particula ativa
        if (heroBehavior.isShooting == true && !heroAnim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Reloading_Gun"))
        {
            if (heroBehavior.pistols[heroBehavior.pistolSelected].gameObject.activeInHierarchy)
            {
                fireParticlePistol.SetActive(true);
                trailParticle.gameObject.SetActive(true);
                var main = trailParticle.main;
                main.simulationSpeed = 0.5f;
            }
        }
        else
        {
            fireParticlePistol.SetActive(false);
            trailParticle.gameObject.SetActive(false);
        }
    }

    public void ActiveParticle()
    {
        if (heroBehavior.glockAmmo > 0 && heroBehavior.pistols[0].activeSelf)
        {
            heroBehavior.glockAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[0].transform.position, bulletsSpawn[0].transform.rotation);
        }
        else if(heroBehavior.coltAmmo > 0 && heroBehavior.pistols[1].activeSelf)
        {
            heroBehavior.coltAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[1].transform.position, bulletsSpawn[1].transform.rotation);
        }
        else if(heroBehavior.p38Ammo > 0 && heroBehavior.pistols[2].activeSelf)
        {
            heroBehavior.p38Ammo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[2].transform.position, bulletsSpawn[2].transform.rotation);
        }
        else if (heroBehavior.coltAnaAmmo > 0 && heroBehavior.pistols[3].activeSelf)
        {
            heroBehavior.coltAnaAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[3].transform.position, bulletsSpawn[3].transform.rotation);
        }
        else if(heroBehavior.socomAmmo > 0 && heroBehavior.pistols[4].activeSelf)
        {
            heroBehavior.socomAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[4].transform.position, bulletsSpawn[4].transform.rotation);
        }
    }

    public void DeactivateParticle()
    {
        fireParticlePistol.SetActive(false);
        trailParticle.gameObject.SetActive(false);
    }
}
