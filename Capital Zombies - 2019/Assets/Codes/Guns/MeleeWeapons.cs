﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapons : MonoBehaviour
{
    public float damageValue = 0;
    public string playerTag = "";
    public int meleeID = 0;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "DamageBox")
        {
            DamageBox dmgScript = other.gameObject.GetComponent<DamageBox>();            
            dmgScript.playerTag = transform.root.GetChild(1).tag;
            dmgScript.gunType = "Melee";
            dmgScript.gunID = meleeID;
            dmgScript.TakeDamage(damageValue);
        }

        if(other.tag == "SpecialEnemyBigZ")
        {
            BigZ specialBigZ = other.GetComponent<BigZ>();
            specialBigZ.lifeBigZ -= damageValue;
        }

        if (other.tag == "ZombieStan")
        {
            HitboxStandardZombie zombieHitbox = other.GetComponent<HitboxStandardZombie>();

            zombieHitbox.damageValue = damageValue;

            zombieHitbox.whoShooting = this.gameObject;
            zombieHitbox.ReceiveDamage();
        }
    }
}
