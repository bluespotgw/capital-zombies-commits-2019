﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayGun : MonoBehaviour
{
    public GameObject decals;
    //[Header("GUN RAYCASTS:")]
    public GameObject raycastAK;
    //public GameObject raycastM4;

    void Start()
    {
        
    }

    void Update()
    {
        //Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        Debug.DrawRay(raycastAK.transform.position, transform.TransformDirection(-Vector3.right) * 10, Color.green);
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(raycastAK.transform.position, transform.TransformDirection(-Vector3.right) * 10, out hit))
            {
                //Debug.Log(hit.collider.name);
                Instantiate(decals, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
            }
        }        
    }   
}
