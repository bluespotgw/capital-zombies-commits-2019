﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireParticle : MonoBehaviour
{
    public GameObject fireParticleRifle;
    public ParticleSystem trailParticle; 
    public GameObject heroObj;
    public GameObject bulletParticle;
    public GameObject[] bulletsSpawn;
    public HeroBehavior heroBehavior;
    Animator heroAnim;
    
    private void Start()
    {
        heroAnim = heroObj.GetComponent<Animator>();
    }

    private void Update()
    {
        //Previne de sair da animaçao de tiro com a particula ativa. Serve apenas para os rifles de assalto
        if (heroBehavior.isShooting == true && !heroAnim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Reloading_Gun") && 
            heroBehavior.GunSelected == 1)
        {
            if (!heroBehavior.rifles[4].activeSelf && !heroBehavior.rifles[5].activeSelf && !heroBehavior.rifles[6].activeSelf)
            {
                fireParticleRifle.SetActive(true);
                trailParticle.gameObject.SetActive(true);
                var main = trailParticle.main;
                main.simulationSpeed = 1;
            }
        }
        else
        {
            fireParticleRifle.SetActive(false);
            trailParticle.gameObject.SetActive(false);
        }
    }

    public void ActiveParticle()
    {
        //Se alguma arma de mira estiver ativa a particula é ativa apenas uma vez
        if (heroBehavior.rifles[4].activeSelf || heroBehavior.rifles[5].activeSelf || heroBehavior.rifles[6].activeSelf)
        {
            fireParticleRifle.SetActive(true);          
        }

        if (heroBehavior.akAmmo > 0 && heroBehavior.rifles[0].activeSelf)
        {
            heroBehavior.akAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[0].transform.position, bulletsSpawn[0].transform.rotation);
        }
        else if (heroBehavior.m4Ammo > 0 && heroBehavior.rifles[1].activeSelf)
        {
            heroBehavior.m4Ammo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[1].transform.position, bulletsSpawn[1].transform.rotation);
        }
        else if(heroBehavior.stonerAmmo > 0 && heroBehavior.rifles[2].activeSelf)
        {
            heroBehavior.stonerAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[2].transform.position, bulletsSpawn[2].transform.rotation);
        }
        else if(heroBehavior.g28Ammo > 0 && heroBehavior.rifles[3].activeSelf)
        {
            heroBehavior.g28Ammo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[3].transform.position, bulletsSpawn[3].transform.rotation);
        }
        else if(heroBehavior.awpAmmo > 0 && heroBehavior.rifles[4].activeSelf)
        {
            heroBehavior.awpAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[4].transform.position, bulletsSpawn[4].transform.rotation);

        }
        else if(heroBehavior.scoutAmmo > 0 && heroBehavior.rifles[5].activeSelf)
        {
            heroBehavior.scoutAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[5].transform.position, bulletsSpawn[5].transform.rotation);
        }
        else if(heroBehavior.shotgunAmmo > 0 && heroBehavior.rifles[6].activeSelf)
        {
            heroBehavior.shotgunAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[6].transform.position, bulletsSpawn[6].transform.rotation);
        }
        else if(heroBehavior.mp5Ammo > 0 && heroBehavior.rifles[7].activeSelf)
        {
            heroBehavior.mp5Ammo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[7].transform.position, bulletsSpawn[7].transform.rotation);
        }
        else if(heroBehavior.mp40Ammo > 0 && heroBehavior.rifles[8].activeSelf)
        {
            heroBehavior.mp40Ammo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[8].transform.position, bulletsSpawn[8].transform.rotation);
        }
        else if(heroBehavior.bizonAmmo > 0 && heroBehavior.rifles[9].activeSelf)
        {
            heroBehavior.bizonAmmo -= 1f;
            heroBehavior.RayToGuns();
            Instantiate(bulletParticle, bulletsSpawn[9].transform.position, bulletsSpawn[9].transform.rotation);
        }
    }

    //Serve para as armas de mira desativar a particula
    public void DeactivateParticle()
    {
        fireParticleRifle.SetActive(false);
        trailParticle.gameObject.SetActive(false);
    }
}
