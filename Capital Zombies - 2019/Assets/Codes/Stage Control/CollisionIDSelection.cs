﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionIDSelection : MonoBehaviour
{
    public int itemID = 0;
    public string gunName = "";
    public int gunIDToSave = 0;
    public SelectionStage selectionDirector;
    public string playerTag = "";

    private void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        selectionDirector.upItem = true;

        if (other.gameObject.tag == "Player1")
        {
            selectionDirector.P1IsInGun = true;
            selectionDirector.idActualGunP1 = itemID;
            selectionDirector.receiveGunNameP1 = gunName;
        }

        if (other.gameObject.tag == "Player2")
        {
            selectionDirector.P2IsInGun = true;
            selectionDirector.idActualGunP2 = itemID;
            selectionDirector.receiveGunNameP2 = gunName;
        }

        if (other.gameObject.tag == "Player3")
        {
            selectionDirector.P3IsInGun = true;
            selectionDirector.idActualGunP3 = itemID;
            selectionDirector.receiveGunNameP3 = gunName;
        }

        if (other.gameObject.tag == "Player4")
        {
            selectionDirector.P4IsInGun = true;
            selectionDirector.idActualGunP4 = itemID;
            selectionDirector.receiveGunNameP4 = gunName;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        selectionDirector.upItem = false;

        if (other.gameObject.tag == "Player1")
        {
            selectionDirector.P1IsInGun = false;
            selectionDirector.idOldGunP1 = selectionDirector.idActualGunP1;
            selectionDirector.receiveGunNameP1 = "";
        }

        if (other.gameObject.tag == "Player2")
        {
            selectionDirector.P2IsInGun = false;
            selectionDirector.idOldGunP2 = selectionDirector.idActualGunP2;
            selectionDirector.receiveGunNameP2 = "";
        }

        if (other.gameObject.tag == "Player3")
        {
            selectionDirector.P3IsInGun = false;
            selectionDirector.idOldGunP3 = selectionDirector.idActualGunP3;
            selectionDirector.receiveGunNameP3 = "";
        }

        if (other.gameObject.tag == "Player4")
        {
            selectionDirector.P4IsInGun = false;
            selectionDirector.idOldGunP4 = selectionDirector.idActualGunP4;
            selectionDirector.receiveGunNameP4 = "";
        }
    }
}
