﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionStage : MonoBehaviour
{
    [Header("ITENS DE CENA:")]
    public GameObject[] allGuns; // Todas as armas do jogo 3D
    public float itemYPos; // Posição que o item deve subir na cena
    public int idActualGunP1 = 0; // Arma que está atualmente selecionada pelo colisor
    public int idOldGunP1 = 0; // Ultima arma visitada pelo jogador
    public int idActualGunP2 = 0;
    public int idOldGunP2 = 0;
    public int idActualGunP3 = 0;
    public int idOldGunP3 = 0;
    public int idActualGunP4 = 0;
    public int idOldGunP4 = 0;
    public string receiveGunNameP1 = ""; // Tipo de arma passada pelo colisor da arma
    public string receiveGunNameP2 = "";
    public string receiveGunNameP3 = "";
    public string receiveGunNameP4 = "";
    public Animator[] gunsAnimP1; // Animators da interface das armas já selecionadas.Serve pra piscar as armas
    public Animator[] gunsAnimP2;
    public Animator[] gunsAnimP3;
    public Animator[] gunsAnimP4;
    Vector3[] itemOldPos; // Posição atual do item
    Vector3[] itemNewPos; // Posição atual do item
    
    [Header("PLAYERS SELECIONADOS:")]
    public GameObject[] playersChoice; // Players selecionados pelos jogadores
    public HeroData[] heroesScriptableObjects;
    public bool P1IsInGun = false;
    public bool P2IsInGun = false;
    public bool P3IsInGun = false;
    public bool P4IsInGun = false;

    [Header("CONTROLE DA ANIMAÇÃO DO ITEM SUBIR:")]
    public bool upItem = false; // Controla se a arma 3D sobe ou desce quando colidir com ela

    [Header("ICONES DAS ARMAS:")]
    public Sprite[] gunIcons; // Icones das armas em SpriteSheet

    [Header("BG STATS DE ARMAS DOS PLAYERS:")]
    public GameObject[] bgStatsPlayers;// Fundo vermelho dos stats das armas
    public Image[] gunIconStats;// Icone da arma dentro do fundo

    [Header("TEXTOS DE CADA BG STATS:")]
    public Text[] GunStatsText;// Texto de informações de cada fundo

    [Header("UI DOS PLAYERS:")]
    public GameObject[] uiPlayers;

    [Header("ARMAS SELECIONADAS DOS PLAYERS:")] // Icones de armas selecionadas de cada player
    public Image[] P1Guns;
    public Image[] P2Guns;
    public Image[] P3Guns;
    public Image[] P4Guns;

    [Header("TEXTO STATUS:")] // Textos presentes no status 
    public string maxAmmo = "";
    public string power = "";
    public string shootRate = "";
    public string precision = "";
    public string mobility = "";
    public string resistance = "";

    [Header("MUNIÇÃO ARMAS:")]// Textos presentes de munição no status
    public string akMaxAmmo = "30<color=orange>/30</color>";
    public string m4MaxAmmo = "25<color=orange>/25</color>";
    public string glockMaxAmmo = "20<color=orange>/20</color>";
    public string coltMaxAmmo = "20<color=orange>/20</color>";
    public string shieldMaxRes = "-----";
    public string grenadeMaxAmmo = "5<color=orange>/5</color>";
    public string smokeMaxAmmo = "3<color=orange>/3</color>";

    [Header("BARRA DAS ARMAS:")]
    public Image[] powerBar;
    public Image[] shootRBar;
    public Image[] precisionBar;
    public GameObject[] precisionBG;// Oculta a barra da arma
    public GameObject[] shootRBG;

    [Header("BARRA DE STATUS DAS ARMAS:")]
    public float akPower = 0;
    public float m4Power = 0;
    public float glockPower = 0;
    public float coltPower = 0;
    public float katanaPower = 0;
    public float shieldMobility = 0;
    public float shieldResistance = 0;
    public float hePower = 0;
    public float smokePower = 0;

    public float akShootRate = 0;
    public float m4ShootRate = 0;
    public float glockShootRate = 0;
    public float coltShootRate = 0;
    public float katanaShootRate = 0;
    public float heShootRate = 0;
    public float smokeShootRate = 0;

    public float akPrecision = 0;
    public float m4Precision = 0;
    public float glockPrecision = 0;
    public float coltPrecision = 0;
    public float katanaPrecision = 0;

    Transform gunCanvasP1;//Canvas global de cada arma
    Transform gunCanvasP2;
    Transform gunCanvasP3;
    Transform gunCanvasP4;

    [Header("CHECKS:")]
    public GameObject[] playerChecks;
    HeroBehavior heroScript;
    public int confirmedPlayers = 0;//Jogadores ativos no total
    public int portalConfirmedPlayers = 0;//Jogadores que entraram no cenário

    [Header("ITEM SAVE SCRIPT:")]
    public GlobalGameSelections itemScript;

    [Header("SPAWNERS:")]
    public GameObject[] allCharSpawners;

    [Header("SELECTION PLAYER CANVAS:")]
    public GameObject charSelecScreen;

    [Header("CAMERA MAIN PARA ANIMAÇÃO:")]
    public GameObject mainCam;//camera do portal
    public GameObject camTarget;//alvo da camera no portal
    public float speedCam = 0;
    public Transform positionToLook;
    Animator charCameraAnim; 
    bool goCam = false;

    [Header("BOTÃO SELEÇÃO JOYSTICK:")]
    public string[] joyButton;
    public CursorScript[] cursorButtonSelection;

    [Header("LOADING AND WORLD CANVAS:")]
    public GameObject loadCanvas;
    public GameObject gunSelectionCanvas;

    GameObject P1Exists;
    GameObject P2Exists;
    GameObject P3Exists;
    GameObject P4Exists;

    void Start()
    {
        //Identificando posições de cada arma na cena
        itemOldPos = new Vector3[allGuns.Length];
        itemNewPos = new Vector3[allGuns.Length];

        for (int i = 0; i < allGuns.Length; i++)
        {
            itemOldPos[i] = allGuns[i].transform.localPosition;

            itemNewPos[i] = allGuns[i].transform.localPosition;
            itemNewPos[i].y += itemYPos;
        }

        //Coletando o Animator de cada icone de arma selecionada 
        for(int i = 0;i < P1Guns.Length; i++)
        {
            gunsAnimP1[i] = P1Guns[i].GetComponent<Animator>();
            gunsAnimP2[i] = P2Guns[i].GetComponent<Animator>();
            gunsAnimP3[i] = P3Guns[i].GetComponent<Animator>();
            gunsAnimP4[i] = P4Guns[i].GetComponent<Animator>();
        }

        /*itemScript.heroName = new string[confirmedPlayers];
        itemScript.controllerOrder = new string[confirmedPlayers];
        itemScript.controllerType = new string[confirmedPlayers];
        itemScript.rifleGun = new int[confirmedPlayers];
        itemScript.pistolGun = new int[confirmedPlayers];
        itemScript.meleeGun = new int[confirmedPlayers];
        itemScript.grenadeSelected = new int[confirmedPlayers];*/
    }

    void Update()
    {
        if (P1Exists != null)
        {
            if (gunsAnimP1[0].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP1[0].GetCurrentAnimatorStateInfo(0).IsName("FlashRifle"))
            {
                gunsAnimP1[0].SetBool("flashRifle", false);
            }
            if (gunsAnimP1[1].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP1[1].GetCurrentAnimatorStateInfo(0).IsName("FlashPistol"))
            {
                gunsAnimP1[1].SetBool("flashPistol", false);
            }
            if (gunsAnimP1[2].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP1[2].GetCurrentAnimatorStateInfo(0).IsName("FlashMelee"))
            {
                gunsAnimP1[2].SetBool("flashMelee", false);
            }
            if (gunsAnimP1[3].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP1[3].GetCurrentAnimatorStateInfo(0).IsName("FlashGrenade"))
            {
                gunsAnimP1[3].SetBool("flashGrenade", false);
            }
        }

        if (P2Exists != null)
        {
            if (gunsAnimP2[0].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP2[0].GetCurrentAnimatorStateInfo(0).IsName("FlashRifle"))
            {
                gunsAnimP2[0].SetBool("flashRifle", false);
            }
            if (gunsAnimP2[1].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP2[1].GetCurrentAnimatorStateInfo(0).IsName("FlashPistol"))
            {
                gunsAnimP2[1].SetBool("flashPistol", false);
            }
            if (gunsAnimP2[2].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP2[2].GetCurrentAnimatorStateInfo(0).IsName("FlashMelee"))
            {
                gunsAnimP2[2].SetBool("flashMelee", false);
            }
            if (gunsAnimP2[3].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP2[3].GetCurrentAnimatorStateInfo(0).IsName("FlashGrenade"))
            {
                gunsAnimP2[3].SetBool("flashGrenade", false);
            }

        }

        if (P3Exists != null)
        {
            if (gunsAnimP3[0].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP3[0].GetCurrentAnimatorStateInfo(0).IsName("FlashRifle"))
            {
                gunsAnimP3[0].SetBool("flashRifle", false);
            }
            if (gunsAnimP3[1].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP3[1].GetCurrentAnimatorStateInfo(0).IsName("FlashPistol"))
            {
                gunsAnimP3[1].SetBool("flashPistol", false);
            }
            if (gunsAnimP3[2].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP3[2].GetCurrentAnimatorStateInfo(0).IsName("FlashMelee"))
            {
                gunsAnimP3[2].SetBool("flashMelee", false);
            }
            if (gunsAnimP3[3].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP3[3].GetCurrentAnimatorStateInfo(0).IsName("FlashGrenade"))
            {
                gunsAnimP3[3].SetBool("flashGrenade", false);
            }
        }

        if (P4Exists != null)
        {
            if (gunsAnimP4[0].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP4[0].GetCurrentAnimatorStateInfo(0).IsName("FlashRifle"))
            {
                gunsAnimP4[0].SetBool("flashRifle", false);
            }
            if (gunsAnimP4[1].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP4[1].GetCurrentAnimatorStateInfo(0).IsName("FlashPistol"))
            {
                gunsAnimP4[1].SetBool("flashPistol", false);
            }
            if (gunsAnimP4[2].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP4[2].GetCurrentAnimatorStateInfo(0).IsName("FlashMelee"))
            {
                gunsAnimP4[2].SetBool("flashMelee", false);
            }
            if (gunsAnimP4[3].GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && gunsAnimP4[3].GetCurrentAnimatorStateInfo(0).IsName("FlashGrenade"))
            {
                gunsAnimP4[3].SetBool("flashGrenade", false);
            }
        }

        if (upItem)
        {
            if (P1IsInGun)
            {
                allGuns[idActualGunP1].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP1].transform.localPosition, itemNewPos[idActualGunP1], 0.1f);
                gunCanvasP1 = allGuns[idActualGunP1].gameObject.transform.GetChild(0);
                gunCanvasP1.gameObject.SetActive(true);
                bgStatsPlayers[0].SetActive(true);
                gunIconStats[0].sprite = gunIcons[idActualGunP1];

                //Texto de status de cada arma
                if (idActualGunP1 == 0)
                {
                    GunStatsText[0].text = maxAmmo + "\n" + akMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[0].fillAmount = akPower / 100;
                    shootRBar[0].fillAmount = akShootRate / 100;
                    precisionBar[0].fillAmount = akPrecision / 100;

                    precisionBG[0].SetActive(true);
                    shootRBG[0].SetActive(true);
                }
                else if (idActualGunP1 == 1)
                {
                    GunStatsText[0].text = maxAmmo + "\n" + m4MaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[0].fillAmount = m4Power / 100;
                    shootRBar[0].fillAmount = m4ShootRate / 100;
                    precisionBar[0].fillAmount = m4Precision / 100;

                    precisionBG[0].SetActive(true);
                    shootRBG[0].SetActive(true);
                }
                else if (idActualGunP1 == 2)
                {
                    GunStatsText[0].text = maxAmmo + "\n" + glockMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[0].fillAmount = glockPower / 100;
                    shootRBar[0].fillAmount = glockShootRate / 100;
                    precisionBar[0].fillAmount = glockPrecision / 100;

                    precisionBG[0].SetActive(true);
                    shootRBG[0].SetActive(true);
                }
                else if (idActualGunP1 == 3)
                {
                    GunStatsText[0].text = maxAmmo + "\n" + coltMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[0].fillAmount = coltPower / 100;
                    shootRBar[0].fillAmount = coltShootRate / 100;
                    precisionBar[0].fillAmount = coltPrecision / 100;

                    precisionBG[0].SetActive(true);
                    shootRBG[0].SetActive(true);
                }
                else if (idActualGunP1 == 4)
                {
                    GunStatsText[0].text = maxAmmo + "\nN/A\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[0].fillAmount = katanaPower / 100;
                    shootRBar[0].fillAmount = katanaShootRate / 100;
                    precisionBar[0].fillAmount = katanaPrecision / 100;

                    precisionBG[0].SetActive(true);
                    shootRBG[0].SetActive(true);
                }
                else if (idActualGunP1 == 5)
                {
                    GunStatsText[0].text = maxAmmo + "\nN/A\n<color=orange>" + resistance + "</color>\n" + shieldMaxRes + "\n<color=orange>" + mobility + "</color>\n----";

                    powerBar[0].fillAmount = shieldResistance / 100;
                    shootRBar[0].fillAmount = shieldMobility / 100;

                    precisionBG[0].SetActive(false);
                    shootRBG[0].SetActive(true);
                }
                else if (idActualGunP1 == 6)
                {
                    GunStatsText[0].text = maxAmmo + "\n" + grenadeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[0].fillAmount = hePower / 100;
                    shootRBar[0].fillAmount = heShootRate / 100;

                    precisionBG[0].SetActive(false);
                    shootRBG[0].SetActive(true);
                }
                else if (idActualGunP1 == 7)
                {
                    GunStatsText[0].text = maxAmmo + "\n" + smokeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[0].fillAmount = smokePower / 100;
                    shootRBar[0].fillAmount = smokeShootRate / 100;

                    precisionBG[0].SetActive(false);
                    shootRBG[0].SetActive(true);
                }

                //Arma antes vista volta pra mesa
                if (idOldGunP1 != idActualGunP1)
                {
                    allGuns[idOldGunP1].transform.localPosition = Vector3.Lerp(allGuns[idOldGunP1].transform.localPosition, itemOldPos[idOldGunP1], 0.1f);
                }
            }

            if (P2IsInGun)
            {
                allGuns[idActualGunP2].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP2].transform.localPosition, itemNewPos[idActualGunP2], 0.1f);
                gunCanvasP2 = allGuns[idActualGunP2].gameObject.transform.GetChild(0);
                gunCanvasP2.gameObject.SetActive(true);
                bgStatsPlayers[1].SetActive(true);
                gunIconStats[1].sprite = gunIcons[idActualGunP2];

                //Texto de status de cada arma
                if (idActualGunP2 == 0)
                {
                    GunStatsText[1].text = maxAmmo + "\n" + akMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[1].fillAmount = akPower / 100;
                    shootRBar[1].fillAmount = akShootRate / 100;
                    precisionBar[1].fillAmount = akPrecision / 100;

                    precisionBG[1].SetActive(true);
                    shootRBG[1].SetActive(true);
                }
                else if (idActualGunP2 == 1)
                {
                    GunStatsText[1].text = maxAmmo + "\n" + m4MaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[1].fillAmount = m4Power / 100;
                    shootRBar[1].fillAmount = m4ShootRate / 100;
                    precisionBar[1].fillAmount = m4Precision / 100;

                    precisionBG[1].SetActive(true);
                    shootRBG[1].SetActive(true);
                }
                else if (idActualGunP2 == 2)
                {
                    GunStatsText[1].text = maxAmmo + "\n" + glockMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[1].fillAmount = glockPower / 100;
                    shootRBar[1].fillAmount = glockShootRate / 100;
                    precisionBar[1].fillAmount = glockPrecision / 100;

                    precisionBG[1].SetActive(true);
                    shootRBG[1].SetActive(true);
                }
                else if (idActualGunP2 == 3)
                {
                    GunStatsText[1].text = maxAmmo + "\n" + coltMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[1].fillAmount = coltPower / 100;
                    shootRBar[1].fillAmount = coltShootRate / 100;
                    precisionBar[1].fillAmount = coltPrecision / 100;

                    precisionBG[1].SetActive(true);
                    shootRBG[1].SetActive(true);
                }
                else if (idActualGunP2 == 4)
                {
                    GunStatsText[1].text = maxAmmo + "\nN/A\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[1].fillAmount = katanaPower / 100;
                    shootRBar[1].fillAmount = katanaShootRate / 100;
                    precisionBar[1].fillAmount = katanaPrecision / 100;

                    precisionBG[1].SetActive(true);
                    shootRBG[1].SetActive(true);
                }
                else if (idActualGunP2 == 5)
                {
                    GunStatsText[1].text = maxAmmo + "\nN/A\n<color=orange>" + resistance + "</color>\n" + shieldMaxRes + "\n<color=orange>" + mobility + "</color>\n----";

                    powerBar[1].fillAmount = shieldResistance / 100;
                    shootRBar[1].fillAmount = shieldMobility / 100;

                    precisionBG[1].SetActive(false);
                    shootRBG[1].SetActive(true);
                }
                else if (idActualGunP2 == 6)
                {
                    GunStatsText[1].text = maxAmmo + "\n" + grenadeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[1].fillAmount = hePower / 100;
                    shootRBar[1].fillAmount = heShootRate / 100;

                    precisionBG[1].SetActive(false);
                    shootRBG[1].SetActive(true);
                }
                else if (idActualGunP2 == 7)
                {
                    GunStatsText[1].text = maxAmmo + "\n" + smokeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[1].fillAmount = smokePower / 100;
                    shootRBar[1].fillAmount = smokeShootRate / 100;

                    precisionBG[1].SetActive(false);
                    shootRBG[1].SetActive(true);
                }

                //Arma antes vista volta pra mesa
                if (idOldGunP2 != idActualGunP2)
                {
                    allGuns[idOldGunP2].transform.localPosition = Vector3.Lerp(allGuns[idOldGunP2].transform.localPosition, itemOldPos[idOldGunP2], 0.1f);
                }
            }

            if (P3IsInGun)
            {
                allGuns[idActualGunP3].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP3].transform.localPosition, itemNewPos[idActualGunP3], 0.1f);
                gunCanvasP3 = allGuns[idActualGunP3].gameObject.transform.GetChild(0);
                gunCanvasP3.gameObject.SetActive(true);
                bgStatsPlayers[2].SetActive(true);
                gunIconStats[2].sprite = gunIcons[idActualGunP3];

                //Texto de status de cada arma
                if (idActualGunP3 == 0)
                {
                    GunStatsText[2].text = maxAmmo + "\n" + akMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[2].fillAmount = akPower / 100;
                    shootRBar[2].fillAmount = akShootRate / 100;
                    precisionBar[2].fillAmount = akPrecision / 100;

                    precisionBG[2].SetActive(true);
                    shootRBG[2].SetActive(true);
                }
                else if (idActualGunP3 == 1)
                {
                    GunStatsText[2].text = maxAmmo + "\n" + m4MaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[2].fillAmount = m4Power / 100;
                    shootRBar[2].fillAmount = m4ShootRate / 100;
                    precisionBar[2].fillAmount = m4Precision / 100;

                    precisionBG[2].SetActive(true);
                    shootRBG[2].SetActive(true);
                }
                else if (idActualGunP3 == 2)
                {
                    GunStatsText[2].text = maxAmmo + "\n" + glockMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[2].fillAmount = glockPower / 100;
                    shootRBar[2].fillAmount = glockShootRate / 100;
                    precisionBar[2].fillAmount = glockPrecision / 100;

                    precisionBG[2].SetActive(true);
                    shootRBG[2].SetActive(true);
                }
                else if (idActualGunP3 == 3)
                {
                    GunStatsText[2].text = maxAmmo + "\n" + coltMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[2].fillAmount = coltPower / 100;
                    shootRBar[2].fillAmount = coltShootRate / 100;
                    precisionBar[2].fillAmount = coltPrecision / 100;

                    precisionBG[2].SetActive(true);
                    shootRBG[2].SetActive(true);
                }
                else if (idActualGunP3 == 4)
                {
                    GunStatsText[2].text = maxAmmo + "\nN/A\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[2].fillAmount = katanaPower / 100;
                    shootRBar[2].fillAmount = katanaShootRate / 100;
                    precisionBar[2].fillAmount = katanaPrecision / 100;

                    precisionBG[2].SetActive(true);
                    shootRBG[2].SetActive(true);
                }
                else if (idActualGunP3 == 5)
                {
                    GunStatsText[2].text = maxAmmo + "\nN/A\n<color=orange>" + resistance + "</color>\n" + shieldMaxRes + "\n<color=orange>" + mobility + "</color>\n----";

                    powerBar[2].fillAmount = shieldResistance / 100;
                    shootRBar[2].fillAmount = shieldMobility / 100;

                    precisionBG[2].SetActive(false);
                    shootRBG[2].SetActive(true);
                }
                else if (idActualGunP3 == 6)
                {
                    GunStatsText[2].text = maxAmmo + "\n" + grenadeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[2].fillAmount = hePower / 100;
                    shootRBar[2].fillAmount = heShootRate / 100;

                    precisionBG[2].SetActive(false);
                    shootRBG[2].SetActive(true);
                }
                else if (idActualGunP3 == 7)
                {
                    GunStatsText[2].text = maxAmmo + "\n" + smokeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[2].fillAmount = smokePower / 100;
                    shootRBar[2].fillAmount = smokeShootRate / 100;

                    precisionBG[2].SetActive(false);
                    shootRBG[2].SetActive(true);
                }

                //Arma antes vista volta pra mesa
                if (idOldGunP3 != idActualGunP3)
                {
                    allGuns[idOldGunP3].transform.localPosition = Vector3.Lerp(allGuns[idOldGunP3].transform.localPosition, itemOldPos[idOldGunP3], 0.1f);
                }
            }

            if (P4IsInGun)
            {
                allGuns[idActualGunP4].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP4].transform.localPosition, itemNewPos[idActualGunP4], 0.1f);
                gunCanvasP4 = allGuns[idActualGunP4].gameObject.transform.GetChild(0);
                gunCanvasP4.gameObject.SetActive(true);
                bgStatsPlayers[3].SetActive(true);
                gunIconStats[3].sprite = gunIcons[idActualGunP4];

                //Texto de status de cada arma
                if (idActualGunP4 == 0)
                {
                    GunStatsText[3].text = maxAmmo + "\n" + akMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[3].fillAmount = akPower / 100;
                    shootRBar[3].fillAmount = akShootRate / 100;
                    precisionBar[3].fillAmount = akPrecision / 100;

                    precisionBG[3].SetActive(true);
                    shootRBG[3].SetActive(true);
                }
                else if (idActualGunP4 == 1)
                {
                    GunStatsText[3].text = maxAmmo + "\n" + m4MaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[3].fillAmount = m4Power / 100;
                    shootRBar[3].fillAmount = m4ShootRate / 100;
                    precisionBar[3].fillAmount = m4Precision / 100;

                    precisionBG[3].SetActive(true);
                    shootRBG[3].SetActive(true);
                }
                else if (idActualGunP4 == 2)
                {
                    GunStatsText[3].text = maxAmmo + "\n" + glockMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[3].fillAmount = glockPower / 100;
                    shootRBar[3].fillAmount = glockShootRate / 100;
                    precisionBar[3].fillAmount = glockPrecision / 100;

                    precisionBG[3].SetActive(true);
                    shootRBG[3].SetActive(true);
                }
                else if (idActualGunP4 == 3)
                {
                    GunStatsText[3].text = maxAmmo + "\n" + coltMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[3].fillAmount = coltPower / 100;
                    shootRBar[3].fillAmount = coltShootRate / 100;
                    precisionBar[3].fillAmount = coltPrecision / 100;

                    precisionBG[3].SetActive(true);
                    shootRBG[3].SetActive(true);
                }
                else if (idActualGunP4 == 4)
                {
                    GunStatsText[3].text = maxAmmo + "\nN/A\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[3].fillAmount = katanaPower / 100;
                    shootRBar[3].fillAmount = katanaShootRate / 100;
                    precisionBar[3].fillAmount = katanaPrecision / 100;

                    precisionBG[3].SetActive(true);
                    shootRBG[3].SetActive(true);
                }
                else if (idActualGunP4 == 5)
                {
                    GunStatsText[3].text = maxAmmo + "\nN/A\n<color=orange>" + resistance + "</color>\n" + shieldMaxRes + "\n<color=orange>" + mobility + "</color>\n----";

                    powerBar[3].fillAmount = shieldResistance / 100;
                    shootRBar[3].fillAmount = shieldMobility / 100;

                    precisionBG[3].SetActive(false);
                    shootRBG[3].SetActive(true);
                }
                else if (idActualGunP4 == 6)
                {
                    GunStatsText[3].text = maxAmmo + "\n" + grenadeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[3].fillAmount = hePower / 100;
                    shootRBar[3].fillAmount = heShootRate / 100;

                    precisionBG[3].SetActive(false);
                    shootRBG[3].SetActive(true);
                }
                else if (idActualGunP4 == 7)
                {
                    GunStatsText[3].text = maxAmmo + "\n" + smokeMaxAmmo + "\n<color=orange>" + power + "</color>\n----\n<color=orange>" +
                    shootRate + "</color>\n----\n<color=orange>" + precision + "</color>\n----";

                    powerBar[3].fillAmount = smokePower / 100;
                    shootRBar[3].fillAmount = smokeShootRate / 100;

                    precisionBG[3].SetActive(false);
                    shootRBG[3].SetActive(true);
                }

                //Arma antes vista volta pra mesa
                if (idOldGunP4 != idActualGunP4)
                {
                    allGuns[idOldGunP4].transform.localPosition = Vector3.Lerp(allGuns[idOldGunP4].transform.localPosition, itemOldPos[idOldGunP4], 0.1f);
                }
            }

            if (!itemScript.multiplayerSelected && Input.GetKeyDown("x") ||
                !itemScript.multiplayerSelected && Input.GetKeyDown("joystick 1 button " + joyButton[0]) ||
                itemScript.multiplayerSelected && Input.GetKeyDown("joystick 1 button " + joyButton[0]))
            {
                if (P1IsInGun)
                {
                    if (receiveGunNameP1 == "AK")
                    {
                        itemScript.rifleGun[0] = 0;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "M4")
                    {
                        itemScript.rifleGun[0] = 1;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "Stoner")
                    {
                        itemScript.rifleGun[0] = 2;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "G28")
                    {
                        itemScript.rifleGun[0] = 3;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "Awp")
                    {
                        itemScript.rifleGun[0] = 4;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "Scout")
                    {
                        itemScript.rifleGun[0] = 5;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "Shotgun")
                    {
                        itemScript.rifleGun[0] = 6;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "MP5")
                    {
                        itemScript.rifleGun[0] = 7;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "MP40")
                    {
                        itemScript.rifleGun[0] = 8;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP1 == "Bizon")
                    {
                        itemScript.rifleGun[0] = 9;
                        P1Guns[0].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[0].SetBool("flashRifle", true);
                    }

                    else if (receiveGunNameP1 == "Glock")
                    {
                        itemScript.pistolGun[0] = 0;
                        P1Guns[1].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP1 == "Colt")
                    {
                        itemScript.pistolGun[0] = 1;
                        P1Guns[1].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP1 == "P38")
                    {
                        itemScript.pistolGun[0] = 2;
                        P1Guns[1].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP1 == "ColtAna")
                    {
                        itemScript.pistolGun[0] = 3;
                        P1Guns[1].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP1 == "Socom")
                    {
                        itemScript.pistolGun[0] = 4;
                        P1Guns[1].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[1].SetBool("flashPistol", true);
                    }

                    else if (receiveGunNameP1 == "Axe")
                    {
                        itemScript.meleeGun[0] = 0;
                        P1Guns[2].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP1 == "Katana")
                    {
                        itemScript.meleeGun[0] = 1;
                        P1Guns[2].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP1 == "Knife")
                    {
                        itemScript.meleeGun[0] = 2;
                        P1Guns[2].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP1 == "Chainsaw")
                    {
                        itemScript.meleeGun[0] = 3;
                        P1Guns[2].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP1 == "Shield")
                    {
                        itemScript.meleeGun[0] = 4;
                        P1Guns[2].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[2].SetBool("flashMelee", true);
                    }

                    else if (receiveGunNameP1 == "Smoke")
                    {
                        itemScript.grenadeSelected[0] = 0;
                        P1Guns[3].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[3].SetBool("flashGrenade", true);
                    }
                    else if (receiveGunNameP1 == "HE")
                    {
                        itemScript.grenadeSelected[0] = 1;
                        P1Guns[3].sprite = gunIcons[idActualGunP1];
                        gunsAnimP1[3].SetBool("flashGrenade", true);
                    }
                }

            }

            if (itemScript.multiplayerSelected && Input.GetKeyDown("joystick 2 button " + joyButton[1]))
            {
                if (P2IsInGun)
                {
                    if (receiveGunNameP2 == "AK")
                    {
                        itemScript.rifleGun[1] = 0;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "M4")
                    {
                        itemScript.rifleGun[1] = 1;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "Stoner")
                    {
                        itemScript.rifleGun[1] = 2;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "G28")
                    {
                        itemScript.rifleGun[1] = 3;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "Awp")
                    {
                        itemScript.rifleGun[1] = 4;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "Scout")
                    {
                        itemScript.rifleGun[1] = 5;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "Shotgun")
                    {
                        itemScript.rifleGun[1] = 6;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "MP5")
                    {
                        itemScript.rifleGun[1] = 7;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "MP40")
                    {
                        itemScript.rifleGun[1] = 8;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP2 == "Bizon")
                    {
                        itemScript.rifleGun[1] = 9;
                        P2Guns[0].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[0].SetBool("flashRifle", true);
                    }

                    else if (receiveGunNameP2 == "Glock")
                    {
                        itemScript.pistolGun[1] = 0;
                        P2Guns[1].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP2 == "Colt")
                    {
                        itemScript.pistolGun[1] = 1;
                        P2Guns[1].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP2 == "P38")
                    {
                        itemScript.pistolGun[1] = 2;
                        P2Guns[1].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP2 == "ColtAna")
                    {
                        itemScript.pistolGun[1] = 3;
                        P2Guns[1].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP2 == "Socom")
                    {
                        itemScript.pistolGun[1] = 4;
                        P2Guns[1].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[1].SetBool("flashPistol", true);
                    }

                    else if (receiveGunNameP2 == "Axe")
                    {
                        itemScript.meleeGun[1] = 0;
                        P2Guns[2].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP2 == "Katana")
                    {
                        itemScript.meleeGun[1] = 1;
                        P2Guns[2].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP2 == "Knife")
                    {
                        itemScript.meleeGun[1] = 2;
                        P2Guns[2].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP2 == "Chainsaw")
                    {
                        itemScript.meleeGun[1] = 3;
                        P2Guns[2].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP2 == "Shield")
                    {
                        itemScript.meleeGun[1] = 4;
                        P2Guns[2].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[2].SetBool("flashMelee", true);
                    }

                    else if (receiveGunNameP2 == "Smoke")
                    {
                        itemScript.grenadeSelected[1] = 0;
                        P2Guns[3].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[3].SetBool("flashGrenade", true);
                    }
                    else if (receiveGunNameP2 == "HE")
                    {
                        itemScript.grenadeSelected[1] = 1;
                        P2Guns[3].sprite = gunIcons[idActualGunP2];
                        gunsAnimP2[3].SetBool("flashGrenade", true);
                    }
                }
            }

            if (itemScript.multiplayerSelected && Input.GetKeyDown("joystick 3 button " + joyButton[2]))
            {
                if (P3IsInGun)
                {
                    if (receiveGunNameP3 == "AK")
                    {
                        itemScript.rifleGun[2] = 0;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "M4")
                    {
                        itemScript.rifleGun[2] = 1;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "Stoner")
                    {
                        itemScript.rifleGun[2] = 2;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "G28")
                    {
                        itemScript.rifleGun[2] = 3;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "Awp")
                    {
                        itemScript.rifleGun[2] = 4;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "Scout")
                    {
                        itemScript.rifleGun[2] = 5;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "Shotgun")
                    {
                        itemScript.rifleGun[2] = 6;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "MP5")
                    {
                        itemScript.rifleGun[2] = 7;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "MP40")
                    {
                        itemScript.rifleGun[2] = 8;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP3 == "Bizon")
                    {
                        itemScript.rifleGun[2] = 9;
                        P3Guns[0].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[0].SetBool("flashRifle", true);
                    }

                    else if (receiveGunNameP3 == "Glock")
                    {
                        itemScript.pistolGun[2] = 0;
                        P3Guns[1].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP3 == "Colt")
                    {
                        itemScript.pistolGun[2] = 1;
                        P3Guns[1].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP3 == "P38")
                    {
                        itemScript.pistolGun[2] = 2;
                        P3Guns[1].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP3 == "ColtAna")
                    {
                        itemScript.pistolGun[2] = 3;
                        P3Guns[1].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP3 == "Socom")
                    {
                        itemScript.pistolGun[2] = 4;
                        P3Guns[1].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[1].SetBool("flashPistol", true);
                    }

                    else if (receiveGunNameP3 == "Axe")
                    {
                        itemScript.meleeGun[2] = 0;
                        P3Guns[2].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP3 == "Katana")
                    {
                        itemScript.meleeGun[2] = 1;
                        P3Guns[2].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP3 == "Knife")
                    {
                        itemScript.meleeGun[2] = 2;
                        P3Guns[2].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP3 == "Chainsaw")
                    {
                        itemScript.meleeGun[2] = 3;
                        P3Guns[2].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP3 == "Shield")
                    {
                        itemScript.meleeGun[2] = 4;
                        P3Guns[2].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[2].SetBool("flashMelee", true);
                    }

                    else if (receiveGunNameP3 == "Smoke")
                    {
                        itemScript.grenadeSelected[2] = 0;
                        P3Guns[3].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[3].SetBool("flashGrenade", true);
                    }
                    else if (receiveGunNameP3 == "HE")
                    {
                        itemScript.grenadeSelected[2] = 1;
                        P3Guns[3].sprite = gunIcons[idActualGunP3];
                        gunsAnimP3[3].SetBool("flashGrenade", true);
                    }
                }
            }

            if (itemScript.multiplayerSelected && Input.GetKeyDown("joystick 4 button " + joyButton[3]))
            {
                if (P4IsInGun)
                {
                    if (receiveGunNameP4 == "AK")
                    {
                        itemScript.rifleGun[3] = 0;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "M4")
                    {
                        itemScript.rifleGun[3] = 1;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "Stoner")
                    {
                        itemScript.rifleGun[3] = 2;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "G28")
                    {
                        itemScript.rifleGun[3] = 3;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "Awp")
                    {
                        itemScript.rifleGun[3] = 4;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "Scout")
                    {
                        itemScript.rifleGun[3] = 5;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "Shotgun")
                    {
                        itemScript.rifleGun[3] = 6;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "MP5")
                    {
                        itemScript.rifleGun[3] = 7;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "MP40")
                    {
                        itemScript.rifleGun[3] = 8;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }
                    else if (receiveGunNameP4 == "Bizon")
                    {
                        itemScript.rifleGun[3] = 9;
                        P4Guns[0].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[0].SetBool("flashRifle", true);
                    }

                    else if (receiveGunNameP4 == "Glock")
                    {
                        itemScript.pistolGun[3] = 0;
                        P4Guns[1].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP4 == "Colt")
                    {
                        itemScript.pistolGun[3] = 1;
                        P4Guns[1].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP4 == "P38")
                    {
                        itemScript.pistolGun[3] = 2;
                        P4Guns[1].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP4 == "ColtAna")
                    {
                        itemScript.pistolGun[3] = 3;
                        P4Guns[1].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[1].SetBool("flashPistol", true);
                    }
                    else if (receiveGunNameP4 == "Socom")
                    {
                        itemScript.pistolGun[3] = 4;
                        P4Guns[1].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[1].SetBool("flashPistol", true);
                    }

                    else if (receiveGunNameP4 == "Axe")
                    {
                        itemScript.meleeGun[3] = 0;
                        P4Guns[2].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP4 == "Katana")
                    {
                        itemScript.meleeGun[3] = 1;
                        P4Guns[2].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP4 == "Knife")
                    {
                        itemScript.meleeGun[3] = 2;
                        P4Guns[2].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP4 == "Chainsaw")
                    {
                        itemScript.meleeGun[3] = 3;
                        P4Guns[2].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[2].SetBool("flashMelee", true);
                    }
                    else if (receiveGunNameP4 == "Shield")
                    {
                        itemScript.meleeGun[3] = 4;
                        P4Guns[2].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[2].SetBool("flashMelee", true);
                    }

                    else if (receiveGunNameP4 == "Smoke")
                    {
                        itemScript.grenadeSelected[3] = 0;
                        P4Guns[3].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[3].SetBool("flashGrenade", true);
                    }
                    else if (receiveGunNameP4 == "HE")
                    {
                        itemScript.grenadeSelected[3] = 1;
                        P4Guns[3].sprite = gunIcons[idActualGunP4];
                        gunsAnimP4[3].SetBool("flashGrenade", true);
                    }
                }
            }
        }
        else
        {
            if (!P1IsInGun)
            {
                allGuns[idActualGunP1].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP1].transform.localPosition, itemOldPos[idActualGunP1], 0.1f);
                gunCanvasP1 = allGuns[idActualGunP1].gameObject.transform.GetChild(0);
                gunCanvasP1.gameObject.SetActive(false);
                bgStatsPlayers[0].SetActive(false);
            }

            if (!P2IsInGun)
            {
                allGuns[idActualGunP2].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP2].transform.localPosition, itemOldPos[idActualGunP2], 0.1f);
                gunCanvasP2 = allGuns[idActualGunP2].gameObject.transform.GetChild(0);
                gunCanvasP2.gameObject.SetActive(false);
                bgStatsPlayers[1].SetActive(false);
            }

            if (!P3IsInGun)
            {
                allGuns[idActualGunP3].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP3].transform.localPosition, itemOldPos[idActualGunP3], 0.1f);
                gunCanvasP3 = allGuns[idActualGunP3].gameObject.transform.GetChild(0);
                gunCanvasP3.gameObject.SetActive(false);
                bgStatsPlayers[2].SetActive(false);
            }

            if (!P4IsInGun)
            {
                allGuns[idActualGunP4].transform.localPosition = Vector3.Lerp(allGuns[idActualGunP4].transform.localPosition, itemOldPos[idActualGunP4], 0.1f);
                gunCanvasP4 = allGuns[idActualGunP4].gameObject.transform.GetChild(0);
                gunCanvasP4.gameObject.SetActive(false);
                bgStatsPlayers[3].SetActive(false);
            }
        }

        if (itemScript.canSpawn)
        {
            for(int i = 0; i < itemScript.controllerType.Length; i++)
            {
                if(itemScript.controllerType[i] != "")
                {
                    if (itemScript.heroName[i] == "Jack")
                    {
                        GameObject charInsta = (GameObject)Instantiate(playersChoice[0], allCharSpawners[i].transform.position, allCharSpawners[i].transform.rotation);                        
                        Transform charCam = charInsta.transform.GetChild(0);
                        Transform heroObj = charInsta.transform.GetChild(1);
                        Transform canvasObj = charInsta.transform.GetChild(2);

                        heroObj.gameObject.tag = "Player" + (i + 1).ToString();
                        charCam.gameObject.SetActive(false);
                        canvasObj.gameObject.SetActive(false);
                        heroScript = heroObj.GetComponent<HeroBehavior>();
                        heroScript.heroData = heroesScriptableObjects[0];
                        heroScript.joystickOrderID = itemScript.controllerOrder[i];
                        heroScript.gameControlToPlay = itemScript.controllerType[i];
                        heroScript.isInSelectionZone = true;
                        joyButton[i] = cursorButtonSelection[i].joystickSelectionButton;
                    }
                    else if (itemScript.heroName[i] == "Julie")
                    {
                        GameObject charInsta = (GameObject)Instantiate(playersChoice[1], allCharSpawners[i].transform.position, allCharSpawners[i].transform.rotation);
                        Transform charCam = charInsta.transform.GetChild(0);
                        Transform heroObj = charInsta.transform.GetChild(1);
                        Transform canvasObj = charInsta.transform.GetChild(2);

                        heroObj.gameObject.tag = "Player" + (i + 1).ToString();
                        charCam.gameObject.SetActive(false);
                        canvasObj.gameObject.SetActive(false);
                        heroScript = heroObj.GetComponent<HeroBehavior>();
                        heroScript.heroData = heroesScriptableObjects[1];
                        heroScript.joystickOrderID = itemScript.controllerOrder[i];
                        heroScript.gameControlToPlay = itemScript.controllerType[i];
                        heroScript.isInSelectionZone = true;
                        joyButton[i] = cursorButtonSelection[i].joystickSelectionButton;
                    }
                    else if (itemScript.heroName[i] == "Jimmy")
                    {
                        GameObject charInsta = (GameObject)Instantiate(playersChoice[2], allCharSpawners[i].transform.position, allCharSpawners[i].transform.rotation);
                        Transform charCam = charInsta.transform.GetChild(0);
                        Transform heroObj = charInsta.transform.GetChild(1);
                        Transform canvasObj = charInsta.transform.GetChild(2);

                        heroObj.gameObject.tag = "Player" + (i + 1).ToString();
                        charCam.gameObject.SetActive(false);
                        canvasObj.gameObject.SetActive(false);
                        heroScript = heroObj.GetComponent<HeroBehavior>();
                        heroScript.heroData = heroesScriptableObjects[2];
                        heroScript.joystickOrderID = itemScript.controllerOrder[i];
                        heroScript.gameControlToPlay = itemScript.controllerType[i];
                        heroScript.isInSelectionZone = true;
                        joyButton[i] = cursorButtonSelection[i].joystickSelectionButton;
                    }
                    else if (itemScript.heroName[i] == "Josh")
                    {
                        GameObject charInsta = (GameObject)Instantiate(playersChoice[3], allCharSpawners[i].transform.position, allCharSpawners[i].transform.rotation);
                        Transform charCam = charInsta.transform.GetChild(0);
                        Transform heroObj = charInsta.transform.GetChild(1);
                        Transform canvasObj = charInsta.transform.GetChild(2);

                        heroObj.gameObject.tag = "Player" + (i + 1).ToString();
                        charCam.gameObject.SetActive(false);
                        canvasObj.gameObject.SetActive(false);
                        heroScript = heroObj.GetComponent<HeroBehavior>();
                        heroScript.heroData = heroesScriptableObjects[3];
                        heroScript.joystickOrderID = itemScript.controllerOrder[i];
                        heroScript.gameControlToPlay = itemScript.controllerType[i];
                        heroScript.isInSelectionZone = true;
                        joyButton[i] = cursorButtonSelection[i].joystickSelectionButton;
                    }
                }
            }

            P1Exists = GameObject.FindGameObjectWithTag("Player1");
            P2Exists = GameObject.FindGameObjectWithTag("Player2");
            P3Exists = GameObject.FindGameObjectWithTag("Player3");
            P4Exists = GameObject.FindGameObjectWithTag("Player4");

            if (P1Exists != null)
            {
                confirmedPlayers += 1;
                uiPlayers[0].SetActive(true);
            }
            if (P2Exists != null)
            {
                confirmedPlayers += 1;
                uiPlayers[1].SetActive(true);
            }
            if (P3Exists != null)
            {
                confirmedPlayers += 1;
                uiPlayers[2].SetActive(true);
            }
            if (P4Exists != null)
            {
                confirmedPlayers += 1;
                uiPlayers[3].SetActive(true);
            }

            portalConfirmedPlayers = confirmedPlayers;
            charSelecScreen.SetActive(false);
            goCam = true;

            itemScript.canSpawn = false;
        }

        if (goCam)
        {
            mainCam.transform.rotation = Quaternion.Slerp(mainCam.transform.rotation, positionToLook.transform.rotation, Time.deltaTime);
        }

        if(portalConfirmedPlayers <= 0)
        {
            goCam = false;

            Quaternion looking = Quaternion.LookRotation(camTarget.transform.position - mainCam.transform.position);
            mainCam.transform.rotation = Quaternion.Slerp(mainCam.transform.rotation,looking,Time.deltaTime);
            float dis = Vector3.Distance(mainCam.transform.position, camTarget.transform.position);

            if(dis > 2)
            {
                mainCam.transform.Translate(transform.forward * speedCam);
            }

            loadCanvas.SetActive(true);
            gunSelectionCanvas.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player1")
        {
            portalConfirmedPlayers -= 1;
            playerChecks[0].SetActive(true);
            Destroy(other.transform.parent.gameObject);          
        }
        if (other.tag == "Player2")
        {
            portalConfirmedPlayers -= 1;
            playerChecks[1].SetActive(true);
            Destroy(other.transform.parent.gameObject);
        }
        if (other.tag == "Player3")
        {
            portalConfirmedPlayers -= 1;
            playerChecks[2].SetActive(true);
            Destroy(other.transform.parent.gameObject);
        }
        if (other.tag == "Player4")
        {
            portalConfirmedPlayers -= 1;
            playerChecks[3].SetActive(true);
            Destroy(other.transform.parent.gameObject);
        }
    }
}
