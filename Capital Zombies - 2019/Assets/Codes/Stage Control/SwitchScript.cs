﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchScript : MonoBehaviour
{
    public string switchType = "";
    public bool onOff = false;
    public GlobalGameSelections itemScript;
    public Text switchText;
    Image colorSwitch;

    private void Start()
    {
        colorSwitch = GetComponent<Image>();

        if (onOff)
        {
            switchText.text = "ON";
            colorSwitch.color = Color.green;

            if(switchType == "Random")
            {
                itemScript.randomStage = "On";
            }
            else
            {
                itemScript.teamAttack = "On";
            }
           
        }
        else
        {
            switchText.text = "OFF";
            colorSwitch.color = Color.red;

            if (switchType == "Random")
            {
                itemScript.randomStage = "Off";
            }
            else
            {
                itemScript.teamAttack = "Off";
            }
        }
    }

    public void changeMode()
    {
        onOff = !onOff;

        if (onOff)
        {
            switchText.text = "ON";
            colorSwitch.color = Color.green;

            if (switchType == "Random")
            {
                itemScript.randomStage = "On";
            }
            else
            {
                itemScript.teamAttack = "On";
            }
        }
        else
        {
            switchText.text = "OFF";
            colorSwitch.color = Color.red;

            if (switchType == "Random")
            {
                itemScript.randomStage = "Off";
            }
            else
            {
                itemScript.teamAttack = "Off";
            }
        }
    }
}
