﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharSelection : MonoBehaviour
{
    [Header("TODAS AS CAIXAS DE SELEÇÃO DE PERSONAGENS:")]
    public GameObject[] playersBoxSelection;

    [Header("TEMPO PARA CHECAR CONTROLES CONECTADOS:")]
    public float checkTimer = 0;
    float timer = 0;

    [Header("ICONES DE CADA CONTROLE:")]
    public GameObject[] allSelectors;
    CursorScript[] scriptCursor;

    [Header("ICONES DE CADA CONTROLE:")]
    public Sprite controllerIconXbox;
    public Sprite controllerIconPlaystation;
    public Sprite controllerIconKeyboard;
    public Sprite controllerIconGeneric;
    public Image[] playerIconController;

    [Header("NOME CONTROLES:")]
    public Text[] controllerName;

    [Header("VELOCIDADE DOS CURSORES:")]
    public float cursorSpeed = 0;

    [Header("CONTROLES CONECTADOS:")]
    public string[] controles;

    [Header("SFX CAIXAS DE SELECAO:")]
    public GameObject[] allSFXBoxes;
    AudioSource[] p1SFX;
    AudioSource[] p2SFX;
    AudioSource[] p3SFX;
    AudioSource[] p4SFX;
    bool playSFXP1 = false;
    bool playSFXP2 = false;
    bool playSFXP3 = false;
    bool playSFXP4 = false;

    [Header("MODO DE JOGO:")]
    public bool multiplayer = false;

    [Header("ITEM SCRIPT:")]
    public GlobalGameSelections itemScript;

    [Header("MARCADOR CONFIRMED:")]
    public int confirmedPlayers = 0;
    public GameObject confirmLabel;
    DG.Tweening.DOTweenAnimation labelTween;
    public int actualPlayers = 0;

    int lastIndex = 0;

    string[] splittedP1;
    string[] splittedP2;
    string[] splittedP3;
    string[] splittedP4;
    string joyNameP1 = "";
    string joyNameP2 = "";
    string joyNameP3 = "";
    string joyNameP4 = "";

    bool axisP1InUse = false;
    bool axisP2InUse = false;
    bool axisP3InUse = false;
    bool axisP4InUse = false;

    void Start()
    {
        timer = checkTimer;
        Cursor.visible = false;

        labelTween = confirmLabel.GetComponent<DG.Tweening.DOTweenAnimation>();

        for (int i = 0; i < itemScript.heroName.Length; i++)
        {
            itemScript.heroName[i] = "";
        }

        /*for (int i = 0; i < itemScript.controllerOrder.Length; i++)
        {
            itemScript.controllerOrder[i] = "";
        }*/

        for (int i = 0; i < itemScript.controllerType.Length; i++)
        {
            itemScript.controllerType[i] = "";
        }

        for (int i = 0; i < itemScript.rifleGun.Length; i++)
        {
            itemScript.rifleGun[i] = 0;
        }

        for (int i = 0; i < itemScript.pistolGun.Length; i++)
        {
            itemScript.pistolGun[i] = 0;
        }

        for (int i = 0; i < itemScript.meleeGun.Length; i++)
        {
            itemScript.meleeGun[i] = 0;
        }

        for (int i = 0; i < itemScript.grenadeSelected.Length; i++)
        {
            itemScript.grenadeSelected[i] = 0;
        }

        if (multiplayer)
        {
            itemScript.multiplayerSelected = true;
        }
        else
        {
            itemScript.multiplayerSelected = false;
        }

        p1SFX = allSFXBoxes[0].GetComponents<AudioSource>();
        p2SFX = allSFXBoxes[1].GetComponents<AudioSource>();
        p3SFX = allSFXBoxes[2].GetComponents<AudioSource>();
        p4SFX = allSFXBoxes[3].GetComponents<AudioSource>();

        scriptCursor = new CursorScript[allSelectors.Length];

        for (int i = 0; i < allSelectors.Length; i++)
        {
            scriptCursor[i] = allSelectors[i].GetComponent<CursorScript>();
        }
    }

    void Update()
    {
        if (multiplayer)
        {
            controles = new string[Input.GetJoystickNames().Length];
            for (int i = 0; i < Input.GetJoystickNames().Length; i++)
            {
                controles[i] = Input.GetJoystickNames()[i];
            }
        
            //Compara quais joysticks estao conectados para mover os cursores
            if (Input.GetJoystickNames().Length > 0)
            {
                if(Input.GetJoystickNames()[0] != "")
                {
                    float joy1Hor = Input.GetAxis("LeftHorizontalJoystick");
                    float joy1Vert = Input.GetAxis("LeftVerticalJoystick");

                    if (joy1Hor != 0 || joy1Vert != 0)
                    {
                        Vector2 pos = allSelectors[0].transform.localPosition;
                        pos.x += joy1Hor * cursorSpeed;
                        pos.y += joy1Vert * cursorSpeed;
                        allSelectors[0].transform.localPosition = pos;
                    }
                }
            }

            if (Input.GetJoystickNames().Length > 1)
            {
                if(Input.GetJoystickNames()[1] != "")
                {
                    float joy2Hor = Input.GetAxis("LeftHorizontalJoystickP2");
                    float joy2Vert = Input.GetAxis("LeftVerticalJoystickP2");

                    if (joy2Hor != 0 || joy2Vert != 0)
                    {
                        Vector2 pos = allSelectors[1].transform.localPosition;
                        pos.x += joy2Hor * cursorSpeed;
                        pos.y += joy2Vert * cursorSpeed;
                        allSelectors[1].transform.localPosition = pos;
                    }
                }
            }

            if (Input.GetJoystickNames().Length > 2)
            {
                if(Input.GetJoystickNames()[2] != "")
                {
                    float joy3Hor = Input.GetAxis("LeftHorizontalJoystickP3");
                    float joy3Vert = Input.GetAxis("LeftVerticalJoystickP3");

                    if (joy3Hor != 0 || joy3Vert != 0)
                    {
                        Vector2 pos = allSelectors[2].transform.localPosition;
                        pos.x += joy3Hor * cursorSpeed;
                        pos.y += joy3Vert * cursorSpeed;
                        allSelectors[2].transform.localPosition = pos;
                    }
                }
            }

            if (Input.GetJoystickNames().Length > 3)
            {
                if (Input.GetJoystickNames()[3] != "")
                {
                    float joy4Hor = Input.GetAxis("LeftHorizontalJoystickP4");
                    float joy4Vert = Input.GetAxis("LeftVerticalJoystickP4");

                    if (joy4Hor != 0 || joy4Vert != 0)
                    {
                        Vector2 pos = allSelectors[3].transform.localPosition;
                        pos.x += joy4Hor * cursorSpeed;
                        pos.y += joy4Vert * cursorSpeed;
                        allSelectors[3].transform.localPosition = pos;
                    }
                }
            }
        }
        else
        {
            playersBoxSelection[0].SetActive(true);
            allSelectors[0].SetActive(true);

            if (Input.GetJoystickNames().Length > 0)
            {
                if (Input.GetJoystickNames()[0] != "")
                {
                    actualPlayers = 1;

                    float joy1Hor = Input.GetAxis("LeftHorizontalJoystick");
                    float joy1Vert = Input.GetAxis("LeftVerticalJoystick");

                    if (joy1Hor != 0 || joy1Vert != 0)
                    {
                        Vector2 pos = allSelectors[0].transform.localPosition;
                        pos.x += joy1Hor * cursorSpeed;
                        pos.y += joy1Vert * cursorSpeed;
                        allSelectors[0].transform.localPosition = pos;
                    }

                    splittedP1 = Input.GetJoystickNames()[0].Split(' ', '(');

                    if (!playSFXP1)
                    {
                        playSFXP1 = true;
                        p1SFX[0].Play();
                    }

                    for (int i = 0; i < splittedP1.Length; i++)
                    {
                        if (splittedP1[i] == "xbox" || splittedP1[i] == "XBOX" || splittedP1[i] == "Xbox")
                        {
                            playerIconController[0].sprite = controllerIconXbox;
                            controllerName[0].text = "Xbox";
                            scriptCursor[0].joystickSelectionButton = "0";
                            scriptCursor[0].joystickReselectionButton = "1";
                            scriptCursor[0].joystickStartButton = "7";
                            break;
                        }
                        else if (splittedP1[i] == "Wireless")
                        {
                            playerIconController[0].sprite = controllerIconPlaystation;
                            controllerName[0].text = "Playstation";
                            scriptCursor[0].joystickSelectionButton = "2";
                            scriptCursor[0].joystickReselectionButton = "1";
                            scriptCursor[0].joystickStartButton = "9";
                            break;
                        }
                        else
                        {
                            playerIconController[0].sprite = controllerIconGeneric;
                            controllerName[0].text = "Generic";
                            scriptCursor[0].joystickSelectionButton = "1";
                            scriptCursor[0].joystickReselectionButton = "2";
                            scriptCursor[0].joystickStartButton = "9";
                        }
                    }

                    itemScript.controllerType[0] = controllerName[0].text;
                }
                else
                {
                    actualPlayers = 1;

                    Vector3 mousePos = Input.mousePosition;
                    mousePos.z = 1;
                    allSelectors[0].transform.position = Camera.main.ScreenToWorldPoint(mousePos);

                    playersBoxSelection[0].SetActive(true);
                    playersBoxSelection[1].SetActive(false);
                    playersBoxSelection[2].SetActive(false);
                    playersBoxSelection[3].SetActive(false);

                    allSelectors[0].SetActive(true);
                    allSelectors[1].SetActive(false);
                    allSelectors[2].SetActive(false);
                    allSelectors[3].SetActive(false);

                    if (!playSFXP1)
                    {
                        playSFXP1 = true;
                        p1SFX[0].Play();
                    }

                    playerIconController[0].sprite = controllerIconKeyboard;
                    controllerName[0].text = "Keyboard";
                    itemScript.controllerType[0] = controllerName[0].text;
                }
            }
            else
            {
                actualPlayers = 1;

                Vector3 mousePos = Input.mousePosition;
                mousePos.z = 1;
                allSelectors[0].transform.position = Camera.main.ScreenToWorldPoint(mousePos);

                playersBoxSelection[0].SetActive(true);
                playersBoxSelection[1].SetActive(false);
                playersBoxSelection[2].SetActive(false);
                playersBoxSelection[3].SetActive(false);

                allSelectors[0].SetActive(true);
                allSelectors[1].SetActive(false);
                allSelectors[2].SetActive(false);
                allSelectors[3].SetActive(false);

                if (!playSFXP1)
                {
                    playSFXP1 = true;
                    p1SFX[0].Play();
                }

                playerIconController[0].sprite = controllerIconKeyboard;
                controllerName[0].text = "Keyboard";
                itemScript.controllerType[0] = controllerName[0].text;
            }   
        }

        //------------------------------------------ Multiplayer Controllers --------------------------------
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            if (multiplayer)
            {
                if (Input.GetJoystickNames().Length > 0)
                {
                    if (Input.GetJoystickNames()[0] != null && Input.GetJoystickNames()[0] != "")
                    {
                       
                        playersBoxSelection[0].SetActive(true);
                        allSelectors[0].SetActive(true);

                        splittedP1 = Input.GetJoystickNames()[0].Split(' ', '(');

                        if (!playSFXP1)
                        {
                            playSFXP1 = true;
                            p1SFX[0].Play();
                            actualPlayers++;
                        }

                        for (int i = 0; i < splittedP1.Length; i++)
                        {
                            if (splittedP1[i] == "xbox" || splittedP1[i] == "XBOX" || splittedP1[i] == "Xbox")
                            {
                                playerIconController[0].sprite = controllerIconXbox;
                                controllerName[0].text = "Xbox";
                                scriptCursor[0].joystickSelectionButton = "0";
                                scriptCursor[0].joystickReselectionButton = "1";
                                scriptCursor[0].joystickStartButton = "7";
                                break;
                            }
                            else if (splittedP1[i] == "Wireless")
                            {
                                playerIconController[0].sprite = controllerIconPlaystation;
                                controllerName[0].text = "Playstation";
                                scriptCursor[0].joystickSelectionButton = "2";
                                scriptCursor[0].joystickReselectionButton = "1";
                                scriptCursor[0].joystickStartButton = "9";
                                break;
                            }
                            else
                            {
                                playerIconController[0].sprite = controllerIconGeneric;
                                controllerName[0].text = "Generic";
                                scriptCursor[0].joystickSelectionButton = "1";
                                scriptCursor[0].joystickReselectionButton = "2";
                                scriptCursor[0].joystickStartButton = "9";
                            }
                        }

                        itemScript.controllerType[0] = controllerName[0].text;
                    }
                    else
                    {                        
                        playersBoxSelection[0].SetActive(false);
                        allSelectors[0].SetActive(false);

                        if (playSFXP1)
                        {
                            playSFXP1 = false;
                            p1SFX[1].Play();
                            actualPlayers--;
                        }

                        itemScript.controllerType[0] = "";
                    }
                }

                if (Input.GetJoystickNames().Length > 1)
                {
                    if (Input.GetJoystickNames()[1] != null && Input.GetJoystickNames()[1] != "")
                    {
                        playersBoxSelection[1].SetActive(true);
                        allSelectors[1].SetActive(true);

                        splittedP2 = Input.GetJoystickNames()[1].Split(' ', '(');

                        if (!playSFXP2)
                        {
                            playSFXP2 = true;
                            p2SFX[0].Play();
                            actualPlayers++;
                        }

                        for (int i = 0; i < splittedP2.Length; i++)
                        {
                            if (splittedP2[i] == "xbox" || splittedP2[i] == "XBOX" || splittedP2[i] == "Xbox")
                            {
                                playerIconController[1].sprite = controllerIconXbox;
                                controllerName[1].text = "Xbox";
                                scriptCursor[1].joystickSelectionButton = "0";
                                scriptCursor[1].joystickReselectionButton = "1";
                                scriptCursor[1].joystickStartButton = "7";
                                break;
                            }
                            else if (splittedP2[i] == "Wireless")
                            {
                                playerIconController[1].sprite = controllerIconPlaystation;
                                controllerName[1].text = "Playstation";
                                scriptCursor[1].joystickSelectionButton = "2";
                                scriptCursor[1].joystickReselectionButton = "1";
                                scriptCursor[1].joystickStartButton = "9";
                                break;
                            }
                            else
                            {
                                playerIconController[1].sprite = controllerIconGeneric;
                                controllerName[1].text = "Generic";
                                scriptCursor[1].joystickSelectionButton = "1";
                                scriptCursor[1].joystickReselectionButton = "2";
                                scriptCursor[1].joystickStartButton = "9";

                            }
                        }

                        itemScript.controllerType[1] = controllerName[1].text;
                    }
                    else
                    {                       
                        playersBoxSelection[1].SetActive(false);
                        allSelectors[1].SetActive(false);

                        if (playSFXP2)
                        {
                            playSFXP2 = false;
                            p2SFX[1].Play();
                            actualPlayers--;
                        }

                        itemScript.controllerType[1] = "";
                    }
                }

                if (Input.GetJoystickNames().Length > 2)
                {
                    if (Input.GetJoystickNames()[2] != null && Input.GetJoystickNames()[2] != "")
                    {                        
                        playersBoxSelection[2].SetActive(true);
                        allSelectors[2].SetActive(true);

                        splittedP3 = Input.GetJoystickNames()[2].Split(' ', '(');

                        if (!playSFXP3)
                        {
                            playSFXP3 = true;
                            p3SFX[0].Play();
                            actualPlayers++;
                        }

                        for (int i = 0; i < splittedP3.Length; i++)
                        {
                            if (splittedP3[i] == "xbox" || splittedP3[i] == "XBOX" || splittedP3[i] == "Xbox")
                            {
                                playerIconController[2].sprite = controllerIconXbox;
                                controllerName[2].text = "Xbox";
                                scriptCursor[2].joystickSelectionButton = "0";
                                scriptCursor[2].joystickReselectionButton = "1";
                                scriptCursor[2].joystickStartButton = "7";
                                break;
                            }
                            else if (splittedP3[i] == "Wireless")
                            {
                                playerIconController[2].sprite = controllerIconPlaystation;
                                controllerName[2].text = "Playstation";
                                scriptCursor[2].joystickSelectionButton = "2";
                                scriptCursor[2].joystickReselectionButton = "1";
                                scriptCursor[2].joystickStartButton = "9";
                                break;
                            }
                            else
                            {
                                playerIconController[2].sprite = controllerIconGeneric;
                                controllerName[2].text = "Generic";
                                scriptCursor[2].joystickSelectionButton = "1";
                                scriptCursor[2].joystickReselectionButton = "2";
                                scriptCursor[2].joystickStartButton = "9";
                            }
                        }

                        itemScript.controllerType[2] = controllerName[2].text;
                    }
                    else
                    {                        
                        playersBoxSelection[2].SetActive(false);
                        allSelectors[2].SetActive(false);

                        if (playSFXP3)
                        {
                            playSFXP3 = false;
                            p3SFX[1].Play();
                            actualPlayers--;
                        }

                        itemScript.controllerType[2] = "";
                    }
                }

                if (Input.GetJoystickNames().Length > 3)
                {
                    if (Input.GetJoystickNames()[3] != null && Input.GetJoystickNames()[3] != "")
                    {                        
                        playersBoxSelection[3].SetActive(true);
                        allSelectors[3].SetActive(true);

                        splittedP4 = Input.GetJoystickNames()[3].Split(' ', '(');

                        if (!playSFXP4)
                        {
                            playSFXP4 = true;
                            p4SFX[0].Play();
                            actualPlayers++;
                        }

                        for (int i = 0; i < splittedP4.Length; i++)
                        {
                            if (splittedP4[i] == "xbox" || splittedP4[i] == "XBOX" || splittedP4[i] == "Xbox")
                            {
                                playerIconController[3].sprite = controllerIconXbox;
                                controllerName[3].text = "Xbox";
                                scriptCursor[3].joystickSelectionButton = "0";
                                scriptCursor[3].joystickReselectionButton = "1";
                                scriptCursor[3].joystickStartButton = "7";
                                break;
                            }
                            else if (splittedP4[i] == "Wireless")
                            {
                                playerIconController[3].sprite = controllerIconPlaystation;
                                controllerName[3].text = "Playstation";
                                scriptCursor[3].joystickSelectionButton = "2";
                                scriptCursor[3].joystickReselectionButton = "1";
                                scriptCursor[3].joystickStartButton = "9";
                                break;
                            }
                            else
                            {
                                playerIconController[3].sprite = controllerIconGeneric;
                                controllerName[3].text = "Generic";
                                scriptCursor[3].joystickSelectionButton = "1";
                                scriptCursor[3].joystickReselectionButton = "2";
                                scriptCursor[3].joystickStartButton = "9";
                            }
                        }

                        itemScript.controllerType[3] = controllerName[3].text;
                    }
                    else
                    {
                        playersBoxSelection[3].SetActive(false);
                        allSelectors[3].SetActive(false);

                        if (playSFXP4)
                        {
                            playSFXP4 = false;
                            p4SFX[1].Play();
                            actualPlayers--;
                        }

                        itemScript.controllerType[3] = "";
                    }
                }
            }
            //-------------------------------------------------------
           
            timer = checkTimer;
        }

        if(confirmedPlayers == actualPlayers && confirmedPlayers > 0)
        {
            labelTween.DOPlay();

            if (multiplayer)
            {
                if (Input.GetKeyDown("joystick 1 button " + scriptCursor[0].joystickStartButton) ||
                Input.GetKeyDown("joystick 2 button " + scriptCursor[1].joystickStartButton) ||
                Input.GetKeyDown("joystick 3 button " + scriptCursor[2].joystickStartButton) ||
                Input.GetKeyDown("joystick 4 button " + scriptCursor[3].joystickStartButton))
                {
                    itemScript.canSpawn = true;                    
                }
            }
            else
            {
                if (Input.GetKeyDown("joystick 1 button " + scriptCursor[0].joystickStartButton) ||
                    Input.GetKeyDown(KeyCode.Return))
                {
                    itemScript.canSpawn = true;
                }
            }
            
        }
        else
        {
            labelTween.DORewind();
        }

        if (itemScript.canSpawn)
        {
            
        }
    }

}
