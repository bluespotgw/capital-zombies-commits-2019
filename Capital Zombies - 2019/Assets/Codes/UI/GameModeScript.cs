﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameModeScript : MonoBehaviour
{
    [Header("TODA A UI DE OPÇÕES:")]
    public int cursorCount = 0;
    public GameObject charSelectObject;
    public GameObject lifeBoxUI;
    public GameObject timeBoxUI;
    public GameObject randomStageUI;
    public GameObject teamAttackUI;
    public Image[] controlArrow;
    public Text gameModeTextUI;
    public Text descriptionModeTextUI;
    public int gameModeID = 0;

    public GlobalGameSelections itemScript;

    DG.Tweening.DOTweenAnimation[] gameSelectorTween;
    CharSelection selectCharScript;

    private void Start()
    {
        gameSelectorTween = this.gameObject.GetComponents<DG.Tweening.DOTweenAnimation>();
        selectCharScript = charSelectObject.GetComponent<CharSelection>();
        teamAttackUI.SetActive(false);     

        if (selectCharScript.multiplayer)
        {
            gameModeTextUI.text = "Versus Mode";
            descriptionModeTextUI.text = "Fight Against Everyone!";
            itemScript.modeSelection = "Versus";
            //lifeBoxUI.SetActive(true);
            timeBoxUI.SetActive(true);
            randomStageUI.SetActive(true);
        }
        else if (!selectCharScript.multiplayer)
        {
            gameModeTextUI.text = "History Mode";
            descriptionModeTextUI.text = "Play inside the history of CZ!";
            itemScript.modeSelection = "History";
            lifeBoxUI.SetActive(false);
            timeBoxUI.SetActive(false);
            randomStageUI.SetActive(false);
        }
    }

    private void Update()
    {
        if(cursorCount > 0)
        {
            gameSelectorTween[0].DORestartById("1");
        }

    }

    public void changeMode()
    {
        gameModeID++;

        if(gameModeID > 2)
        {
            gameModeID = 0;
        }

        if (gameModeID == 0)
        {
            gameModeTextUI.text = "Versus Mode";
            descriptionModeTextUI.text = "Fight Against Everyone!";
            itemScript.modeSelection = "Versus";
        }

        if (gameModeID == 1)
        {
            gameModeTextUI.text = "Team Mode";
            descriptionModeTextUI.text = "Join with a friend to fight!";
            teamAttackUI.SetActive(true);
            itemScript.modeSelection = "Team";
        }

        if (gameModeID == 2)
        {
            gameModeTextUI.text = "Survivor Mode";
            descriptionModeTextUI.text = "It's humans vs zombies! Who will win?";
            teamAttackUI.SetActive(false);
            itemScript.modeSelection = "Survivor";
        }
    }

}
