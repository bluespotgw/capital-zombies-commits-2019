﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CursorScript : MonoBehaviour
{
    [Header("BORDAS DE SELEÇÃO:")]    
    bool isInPos = false;

    //Detector do Icone dos Personagens
    GraphicRaycaster gr;
    PointerEventData pEvent = new PointerEventData(null);

    //Mudar de cor as bordas dos personagens 
    FaceBorderActivation faceScript;
    Color cursorColor;

    [Header("JOYSTICK REFERENTE AO SELETOR:")]//Quem controla os botões de seleçao é o script CharSelection
    public string joystickCursor;
    public string joystickSelectionButton;
    public string joystickReselectionButton;
    public string joystickStartButton;
    bool toReselection = true;

    [Header("CAIXAS DE SELEÇÃO DO PERSONAGEM:")]
    public GameObject playerSelector;
    public Image charImage;
    public Sprite[] faceCharSprites;

    AudioSource[] sfx;

    [Header("ARQUIVOS DE AUDIO DE VOZES:")]
    public AudioSource[] charAudioSource;
    public AudioClip[] jimmyTalks;
    public AudioClip[] julieTalks;
    public AudioClip[] jackTalks;
    public AudioClip[] joshTalks;

    [Header("ID DO CHAR ESCOLHIDO:")]
    string playerSelected = "";

    [Header("GAME SELECTOR:")]
    public GameObject gameSelector;//Objeto GameMode
    GameModeScript gameModeBoxScript;//Script das opções
    CharSelection selectCharScript;//Script da cena principal de seleção - ta no Canvas
    DG.Tweening.DOTweenAnimation[] gameSelectorTween;//As animações tween do GameMode
    public GameObject charSelectObject;//Objeto Canvas
    bool isOverGameMode = false;//Controle do cursor em cima do GameMode
    public bool cursorP1 = false; //Ativo so no cursor 1
    public bool cursorP2 = false;
    public bool cursorP3 = false;
    public bool cursorP4 = false;

    [Header("ITEM SCRIPTABLE OBJECT:")]
    public GlobalGameSelections itemScript;

    [Header("TEAM E RANDOM BUTTONS:")]
    SwitchScript[] switchCode;
    public GameObject[] switches;
    
    void Start()
    {
        gr = GetComponentInParent<GraphicRaycaster>();
        cursorColor = GetComponent<Image>().color;
        sfx = GetComponents<AudioSource>();

        gameSelectorTween = gameSelector.gameObject.GetComponents<DG.Tweening.DOTweenAnimation>();
        selectCharScript = charSelectObject.GetComponent<CharSelection>();
        gameModeBoxScript = gameSelector.GetComponent<GameModeScript>();

        switchCode = new SwitchScript[switches.Length];

        for (int i = 0; i < switches.Length; i++)
        {
            switchCode[i] = switches[i].GetComponent<SwitchScript>();
        }
    }

    private void Update()
    {
        pEvent.position = Camera.main.WorldToScreenPoint(transform.position);
        List<RaycastResult> results = new List<RaycastResult>();
        gr.Raycast(pEvent, results);

        if (results.Count > 0)
        {
            //Seleçao de personagens-----------
            if (results[1].gameObject.name == "Jimmy Mask" || results[1].gameObject.name == "Julie Mask" || 
                results[1].gameObject.name == "Jack Mask" || results[1].gameObject.name == "Josh Mask")
            {
                if (toReselection)
                {
                    if (!isInPos)
                    {
                        faceScript = results[1].gameObject.GetComponent<FaceBorderActivation>();
                        faceScript.cursorCount++;
                        sfx[2].Play();
                        isInPos = true;
                    }

                    if (faceScript.cursorCount > 1)
                    {
                        faceScript.borderFace.color = new Color(0, 0, 0, faceScript.borderFace.color.a);
                    }
                    else
                    {
                        faceScript.borderFace.color = new Color(cursorColor.r, cursorColor.g, cursorColor.b, faceScript.borderFace.color.a);
                    }
                }
                
                if(Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton) && toReselection ||
                   Input.GetMouseButton(0) && toReselection && cursorP1)
                {
                    toReselection = false;
                    faceScript.cursorCount--;
                    selectCharScript.confirmedPlayers++;

                    DG.Tweening.DOTweenAnimation tweenScript = results[1].gameObject.GetComponent<DG.Tweening.DOTweenAnimation>();
                    tweenScript.DORestart();

                    DG.Tweening.DOTweenAnimation[] tweenBoxScript = playerSelector.gameObject.GetComponents<DG.Tweening.DOTweenAnimation>();
                    tweenBoxScript[0].DORestartById("1");

                    sfx[0].Play();

                    if(results[1].gameObject.name == "Jimmy Mask")
                    {
                        playerSelected = "Jimmy";
                        charImage.gameObject.SetActive(true);
                        charImage.sprite = faceCharSprites[0];
                        charAudioSource[0].clip = jimmyTalks[Random.Range(0, jimmyTalks.Length)];
                        charAudioSource[0].Play();

                        if (cursorP1)
                        {
                            itemScript.heroName[0] = "Jimmy";
                        }
                        else if (cursorP2)
                        {
                            itemScript.heroName[1] = "Jimmy";
                        }
                        else if (cursorP3)
                        {
                            itemScript.heroName[2] = "Jimmy";
                        }
                        else if (cursorP4)
                        {
                            itemScript.heroName[3] = "Jimmy";
                        }
                    }
                    else if (results[1].gameObject.name == "Julie Mask")
                    {
                        playerSelected = "Julie";
                        charImage.gameObject.SetActive(true);
                        charImage.sprite = faceCharSprites[1];
                        charAudioSource[1].clip = julieTalks[Random.Range(0, julieTalks.Length)];
                        charAudioSource[1].Play();

                        if (cursorP1)
                        {
                            itemScript.heroName[0] = "Julie";
                        }
                        else if (cursorP2)
                        {
                            itemScript.heroName[1] = "Julie";
                        }
                        else if (cursorP3)
                        {
                            itemScript.heroName[2] = "Julie";
                        }
                        else if (cursorP4)
                        {
                            itemScript.heroName[3] = "Julie";
                        }
                    }
                    else if (results[1].gameObject.name == "Jack Mask")
                    {
                        playerSelected = "Jack";
                        charImage.gameObject.SetActive(true);
                        charImage.sprite = faceCharSprites[2];
                        charAudioSource[2].clip = jackTalks[Random.Range(0, jackTalks.Length)];
                        charAudioSource[2].Play();

                        if (cursorP1)
                        {
                            itemScript.heroName[0] = "Jack";
                        }
                        else if (cursorP2)
                        {
                            itemScript.heroName[1] = "Jack";
                        }
                        else if (cursorP3)
                        {
                            itemScript.heroName[2] = "Jack";
                        }
                        else if (cursorP4)
                        {
                            itemScript.heroName[3] = "Jack";
                        }
                    }
                    else if (results[1].gameObject.name == "Josh Mask")
                    {
                        playerSelected = "Josh";
                        charImage.gameObject.SetActive(true);
                        charImage.sprite = faceCharSprites[3];
                        charAudioSource[3].clip = joshTalks[Random.Range(0, joshTalks.Length)];
                        charAudioSource[3].Play();

                        if (cursorP1)
                        {
                            itemScript.heroName[0] = "Josh";
                        }
                        else if (cursorP2)
                        {
                            itemScript.heroName[1] = "Josh";
                        }
                        else if (cursorP3)
                        {
                            itemScript.heroName[2] = "Josh";
                        }
                        else if (cursorP4)
                        {
                            itemScript.heroName[3] = "Josh";
                        }
                    }
                }
            }
            else
            {
                if (isInPos && toReselection)
                {
                    faceScript.cursorCount--;                    
                    isInPos = false;
                }
            }

            //Game Selection Box-----------
            if (results[1].gameObject.name == "Game Mode")
            {              
                if (!isOverGameMode)
                {
                    gameModeBoxScript.cursorCount++;
                    isOverGameMode = true;
                    sfx[2].Play();
                }

                //Search script significa se o cursor 1 está ativado ou seja, é single player
                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton))
                {
                    sfx[0].Play();

                    gameModeBoxScript.changeMode();
                    gameSelectorTween[1].DORestart();
                }
            }
            if (results[1].gameObject.name != "Game Mode" && isOverGameMode)
            {
                isOverGameMode = false;
                gameModeBoxScript.cursorCount--;
            }

            //Setas de configuraçoes------------------**********************
            if (results[1].gameObject.name == "Add_Life")
            {
                Color cor = this.GetComponent<Image>().color;
                cor.a = 0;
                this.GetComponent<Image>().color = cor;

                GameObject textPlayer = this.transform.GetChild(0).gameObject;
                textPlayer.SetActive(false);

                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton))
                {
                    sfx[2].Play();
                    itemScript.totalLife++;
                }
            }

            if (results[1].gameObject.name == "Remove_Life")
            {
                Color cor = this.GetComponent<Image>().color;
                cor.a = 0;
                this.GetComponent<Image>().color = cor;

                GameObject textPlayer = this.transform.GetChild(0).gameObject;
                textPlayer.SetActive(false);

                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton))
                {
                    sfx[2].Play();
                    itemScript.totalLife--;
                }
            }

            if (results[1].gameObject.name == "Add_Time")
            {
                Color cor = this.GetComponent<Image>().color;
                cor.a = 0;
                this.GetComponent<Image>().color = cor;

                GameObject textPlayer = this.transform.GetChild(0).gameObject;
                textPlayer.SetActive(false);

                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton))
                {
                    sfx[2].Play();
                    itemScript.totalTime++;
                }
            }

            if (results[1].gameObject.name == "Remove_Time")
            {
                Color cor = this.GetComponent<Image>().color;
                cor.a = 0;
                this.GetComponent<Image>().color = cor;

                GameObject textPlayer = this.transform.GetChild(0).gameObject;
                textPlayer.SetActive(false);

                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton))
                {
                    sfx[2].Play();

                    if(itemScript.totalTime > 1)
                    {
                        itemScript.totalTime--;
                    }                    
                }
            }

            if (results[1].gameObject.name != "Add_Time" && results[1].gameObject.name != "Remove_Life" &&
                results[1].gameObject.name != "Add_Life" && results[1].gameObject.name != "Remove_Time")
            {
                Color cor = this.GetComponent<Image>().color;
                cor.a = 1;
                this.GetComponent<Image>().color = cor;

                GameObject textPlayer = this.transform.GetChild(0).gameObject;
                textPlayer.SetActive(true);
            }

            if (results[1].gameObject.name == "Switch Random")
            {
                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton))
                {
                    sfx[2].Play();
                    switchCode[0].changeMode();
                }
            }

            if (results[1].gameObject.name == "Switch Team")
            {
                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton))
                {
                    sfx[2].Play();
                    switchCode[1].changeMode();
                }
            }

            if (results[1].gameObject.name == "Back Menu Arrow")
            {
                if (selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton) ||
                    !selectCharScript.multiplayer && Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickSelectionButton) ||
                    Input.GetMouseButtonDown(0) && !selectCharScript.multiplayer && cursorP1)
                {
                    print("Volta pro menu");
                }
            }
        }

        if (Input.GetKeyDown("joystick " + joystickCursor + " button " + joystickReselectionButton) && !toReselection ||
            Input.GetMouseButton(1) && !toReselection && cursorP1)
        {
            toReselection = true;
            isInPos = false;
            charImage.gameObject.SetActive(false);
            sfx[1].Play();
            selectCharScript.confirmedPlayers--;

            if (playerSelected == "Jimmy")
            {
                charAudioSource[0].Stop();
            }
            else if (playerSelected == "Julie")
            {
                charAudioSource[1].Stop();
            }
            else if (playerSelected == "Jack")
            {
                charAudioSource[2].Stop();
            }
            else if (playerSelected == "Josh")
            {
                charAudioSource[3].Stop();
            }

        }
    }
}
