﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoTalk : MonoBehaviour
{
    [Header("INITAL TALKS:")]
    public string[] initialTalks;
    public Text talkBox;
    public float charSpeed = 0;
    public float lettervel = 0;

    [Header("CANVAS TALK:")]
    public GameObject canvasTalk;

    [Header("DOT TWEEN:")]
    DG.Tweening.DOTweenAnimation tween;
    DG.Tweening.DOTweenAnimation fadeImage;
    public GameObject panelImage;

    [Header("PANEL AND IMAGES:")]
    public Image panel;
    public Sprite[] imagesPanel;

    //Player Components
    GameObject player;
    bool itsOver = false;

    bool canProceed = true;
    int actualTalk = 0;

    void Start()
    {       
        talkBox.text = "";
        StartCoroutine(autoTalk());
        tween = GetComponent<DG.Tweening.DOTweenAnimation>();
        fadeImage = panelImage.GetComponent<DG.Tweening.DOTweenAnimation>();
        lettervel = charSpeed;      
    }

    void Update()
    {
        if(player == null)
        {
           player = GameObject.FindWithTag("Player1");
        }

        if (!itsOver)
        {
            player.GetComponent<HeroBehavior>().enabled = false;
            player.transform.parent.GetChild(2).gameObject.SetActive(false);
        }

        if (initialTalks[actualTalk] == "Imagem")
        {
            fadeImage.DOPlayAllById("3");
            panel.sprite = imagesPanel[actualTalk];
            talkBox.text = "";

            if (Input.GetKeyDown("p"))
            {
                fadeImage.DOPlayAllById("4");
            }
        }

        if (Input.GetKeyDown("p") && canProceed)
        {            
            if(actualTalk < initialTalks.Length-1)
            {
                talkBox.text = "";
                actualTalk++;
                StartCoroutine(autoTalk());
            }
            else
            {
                tween.DOPlayAllById("1");
            }
        }

        if (!canProceed)
        {
            if (Input.GetKey("p"))
            {
                lettervel = charSpeed / 2;
            }
            else
            {
                lettervel = charSpeed;
            }
        }
    }

    IEnumerator autoTalk()
    {
        //yield return new WaitForSeconds(0.5f);

        foreach(char letter in initialTalks[actualTalk].ToCharArray())
        {
            talkBox.text += letter;
            canProceed = false;
            yield return new WaitForSeconds(lettervel);
        }

        canProceed = true;
    }

    public void CanProceed()
    {
        player.GetComponent<HeroBehavior>().enabled = true;
        player.transform.parent.GetChild(2).gameObject.SetActive(true);
        Destroy(this.gameObject);
    }
}
