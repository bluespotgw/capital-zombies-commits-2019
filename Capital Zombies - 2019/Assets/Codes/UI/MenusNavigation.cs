﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class MenusNavigation : MonoBehaviour
{
    /*[Header("MOUSE EVENTS:")]
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    public EventSystem m_EventSystem;*/

    [Header("MODES:")]
    public bool pauseMenu;
    public bool mainMenu;
    public bool gameoverMenu;
    public bool mapSelectionMenu;

    [Header("LOAD CANVAS:")]
    public GameObject loadingCanvas;

    [Header("OPTIONS TEXTS:")]
    public Text[] options;

    [Header("PLAYER WHO INVOKE:")]
    public HeroBehavior playerScript;
    public HeroBehavior[] allPlayersToPause;

    [Header("MAP ICONS:")]
    public GameObject[] mapIcons;
    public Sprite[] mapPieces;
    public string[] regionsName;
    public Image DFMapIndicator;
    public Image stageImageSelector;
    public Text regionTextName;
    public Text mapTextName;

    public GameObject selector;//Piscador
    public GlobalGameSelections itemScript;//Necessario apenas pro single ja que o player é destruido e nao da pra pegar os valores
    public int selectorPos = 0;//Posiçao do seletor

    float vertAxis = 0;//Pro eixo de cada controle
    float horAxis = 0;
    string joyname = "";//Nome de cada controle
    bool plusOption = false;//Para que o seletor passe por cada opção

    void Start()
    {
        /*m_Raycaster = GetComponentInParent<GraphicRaycaster>();
        m_EventSystem = GetComponent<EventSystem>();*/

        if (pauseMenu)
        {
            for (int i = 0; i < 4; i++)
            {
                GameObject player = GameObject.FindWithTag("Player" + (i + 1));

                if (player != null)
                {
                    allPlayersToPause[i] = player.GetComponent<HeroBehavior>();
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {        
        
        /*if (playerScript.joystickActivated)
        {
            selector.transform.position = options[selectorPos].transform.position;

            if (playerScript.verticalLeftJoy < 0 && selectorPos < options.Length - 1)
            {
                selectorPos++;
            }
            if (playerScript.verticalLeftJoy > 0 && selectorPos > 0)
            {
                selectorPos--;
            }
        }
        else if (playerScript.keyboardActivated)
        {
            m_PointerEventData = new PointerEventData(m_EventSystem);
            m_PointerEventData.position = Input.mousePosition;

            List<RaycastResult> results = new List<RaycastResult>();

            m_Raycaster.Raycast(m_PointerEventData, results);

            foreach (RaycastResult result in results)
            {
                selector.transform.position = result.gameObject.transform.position;
            }
        }*/

        if (pauseMenu)
        {
            PauseGameFunc(); 
        }

        if (gameoverMenu)
        {
            GameOverFunc(); 
        }

        if (mapSelectionMenu)
        {
            ChooseStage();
        }
    }

    void PauseGameFunc()
    {
        Time.timeScale = 0;

        if (playerScript.tag == "Player1")
        {
            if (playerScript.itemScript.controllerType[0] == "Keyboard")
            {
                vertAxis = Input.GetAxisRaw("VerticalKeyboard");
            }
            else
            {
                vertAxis = Input.GetAxisRaw(playerScript.heroData.leftJoystickVerticalAxis);
                joyname = "joystick 1 button ";
            }

        }
        else if (playerScript.tag == "Player2")
        {
            vertAxis = Input.GetAxisRaw(playerScript.heroData.leftJoystickVerticalAxisP2);

            if (playerScript.gameControlToPlay != "Keyboard")
            {
                joyname = "joystick 2 button ";
            }
        }
        else if (playerScript.tag == "Player3")
        {
            vertAxis = Input.GetAxisRaw(playerScript.heroData.leftJoystickVerticalAxisP3);

            if (playerScript.gameControlToPlay != "Keyboard")
            {
                joyname = "joystick 3 button ";
            }
        }
        else if (playerScript.tag == "Player4")
        {
            vertAxis = Input.GetAxisRaw(playerScript.heroData.leftJoystickVerticalAxisP4);

            if (playerScript.gameControlToPlay != "Keyboard")
            {
                joyname = "joystick 4 button ";
            }
        }
        //------------------
        if (vertAxis < 0 && selectorPos < options.Length - 1 && !plusOption)
        {
            selectorPos++;
            plusOption = true;
        }
        if (vertAxis > 0 && selectorPos > 0 && !plusOption)
        {
            selectorPos--;
            plusOption = true;
        }
        if (vertAxis == 0)
        {
            plusOption = false;
        }
        //------------------
        selector.transform.position = options[selectorPos].transform.position;
        //------------------
        foreach (HeroBehavior hb in allPlayersToPause)
        {
            if (hb != null)
            {
                hb.enabled = false;
            }
        }
        //------------------
        if (Input.GetKeyDown(playerScript.jumpAndReleaseGrabButton) && selectorPos == 0 && playerScript.gameControlToPlay == "Keyboard" ||
            Input.GetKeyDown(joyname + playerScript.jumpAndReleaseGrabButton) && selectorPos == 0 && playerScript.gameControlToPlay != "Keyboard")
        {
            Time.timeScale = 1;

            foreach (HeroBehavior hb in allPlayersToPause)
            {
                if (hb != null)
                {
                    hb.enabled = true;
                }
            }

            pauseMenu = false;
            this.gameObject.SetActive(false);
        }

        /*m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();

        m_Raycaster.Raycast(m_PointerEventData, results);

        foreach (RaycastResult result in results)
        {
            if (Input.GetMouseButton(0) && result.gameObject.name == "Continue")
            {
                print("here");
                Time.timeScale = 1;

                foreach (HeroBehavior hb in allPlayersToPause)
                {
                    if (hb != null)
                    {
                        hb.enabled = true;
                    }
                }

                pauseMenu = false;
                this.gameObject.SetActive(false);
            }
        }*/
    }

    void GameOverFunc()
    {
        Time.timeScale = 0.5f;

        if (itemScript.controllerType[0] == "Keyboard")
        {
            vertAxis = Input.GetAxisRaw("VerticalKeyboard");
        }
        else
        {
            vertAxis = Input.GetAxisRaw("LeftVerticalJoystick");
            joyname = "joystick 1 button ";
        }
        //------------------
        if (vertAxis < 0 && selectorPos < options.Length - 1 && !plusOption)
        {
            selectorPos++;
            plusOption = true;
        }
        if (vertAxis > 0 && selectorPos > 0 && !plusOption)
        {
            selectorPos--;
            plusOption = true;
        }
        if (vertAxis == 0)
        {
            plusOption = false;
        }
        //------------------
        selector.transform.position = options[selectorPos].transform.position;
        //------------------
        if (Input.GetKeyDown(KeyCode.Space) && selectorPos == 1 && itemScript.controllerType[0] == "Keyboard" ||
            Input.GetKeyDown("joystick 1 button 0") && selectorPos == 1 && itemScript.controllerType[0] != "Keyboard")
        {
            Time.timeScale = 1;
            loadingCanvas.SetActive(true);
            gameoverMenu = false;
        }
    }

    void ChooseStage()
    {
        if (itemScript.controllerType[0] == "Keyboard")
        {
            horAxis = Input.GetAxisRaw("HorizontalKeyboard");
        }
        else
        {
            horAxis = Input.GetAxisRaw("LeftHorizontalJoystick");
            joyname = "joystick 1 button ";
        }
        //------------------
        if (horAxis > 0 && selectorPos < mapIcons.Length - 1 && !plusOption)
        {
            selectorPos++;
            plusOption = true;
        }
        if (horAxis < 0 && selectorPos > 0 && !plusOption)
        {
            selectorPos--;
            plusOption = true;
        }
        if (horAxis == 0)
        {
            plusOption = false;
        }
        //------------------
        selector.transform.position = mapIcons[selectorPos].transform.GetChild(0).position;
        DFMapIndicator.sprite = mapPieces[selectorPos];
        stageImageSelector.sprite = mapIcons[selectorPos].transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite;
        mapTextName.text = mapIcons[selectorPos].name;
        regionTextName.text = "Region: <color=red>" + regionsName[selectorPos] + "</color>";
        //------------------
        if (Input.GetKeyDown(KeyCode.Space) && selectorPos == 1 && itemScript.controllerType[0] == "Keyboard" ||
            Input.GetKeyDown("joystick 1 button 0") && selectorPos == 1 && itemScript.controllerType[0] != "Keyboard")
        {

        }
    }
}
