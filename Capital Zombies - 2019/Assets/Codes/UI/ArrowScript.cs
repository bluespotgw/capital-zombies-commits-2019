﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ArrowScript : MonoBehaviour
{ //Detector do Icone dos Personagens
    GraphicRaycaster gr;
    PointerEventData pEvent = new PointerEventData(null);
    GameModeScript gameModeBoxScript;//Script das opções
    DG.Tweening.DOTweenAnimation arrowTween;
    Image colorArrow;

    [Header("OBJETOS:")]
    public GameObject gameSelector;//Objeto GameMode
    public GameObject canvas;

    [Header("ITEM SCRIPTABLE OBJECT:")]
    public GlobalGameSelections itemScript;

    [Header("TEXTOS DE VIDA E TEMPO:")]
    public Text lifeCounter;
    public Text timeCounter;

    void Start()
    {
        gameModeBoxScript = gameSelector.GetComponent<GameModeScript>();
        gr = canvas.GetComponent<GraphicRaycaster>();

        arrowTween = GetComponent<DG.Tweening.DOTweenAnimation>();
        colorArrow = this.GetComponent<Image>();
    }


    // Update is called once per frame
    void Update()
    {
        lifeCounter.text = itemScript.totalLife.ToString();
        timeCounter.text = itemScript.totalTime.ToString() + ":00";

        pEvent.position = Camera.main.WorldToScreenPoint(transform.position);
        List<RaycastResult> results = new List<RaycastResult>();
        gr.Raycast(pEvent, results);

        if(results.Count > 0)
        {
            if (results[0].gameObject.name == "ArrowDetector")
            {
                colorArrow.color = Color.green;
                arrowTween.DOPlay();
            }
            else
            {
                colorArrow.color = Color.white;
                arrowTween.DORewind();
            }
        }        
    }

    public void lifeValues()
    {
        /*if(arrowType == "add")
        {
            lifePoints++;
        }
        else
        {
            lifePoints--;
        }*/
    }

    public void timeValues()
    {
        /*if(arrowType == "add")
        {
            timeValue++;
        }
        else
        {
            timeValue--;
        }*/
    }
}
