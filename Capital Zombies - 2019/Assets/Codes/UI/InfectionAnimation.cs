﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfectionAnimation : MonoBehaviour
{
    public Image charUIElement;
    public Text statusText;
    public float animTime = 0;
    public string whoIsTheChar = "";
    float timerUI = 0;
    public bool infected = false;
    public bool humanAgain = false;
    DG.Tweening.DOTweenAnimation tweenScript;

    [Header("CHARS IMAGES:")]
    public Sprite[] jackPoses;
    public Sprite[] juliePoses;
    public Sprite[] jimmyPoses;
    public Sprite[] joshPoses;

    private void Start()
    {
        timerUI = animTime;
        tweenScript = GetComponent<DG.Tweening.DOTweenAnimation>();
    }

    private void Update()
    {
        if (infected)
        {
            IsInfected();
        }

        if (humanAgain)
        {
            BackToNormal();
        }
    }

    public void IsInfected()
    {
        tweenScript.DOPlayById("1");
        statusText.text = "INFECTED";

        if (whoIsTheChar == "Jack")
        {
            if(timerUI > 0)
            {
                timerUI -= Time.deltaTime;
                charUIElement.sprite = jackPoses[0];
            }
            else
            {
                charUIElement.sprite = jackPoses[1];
                timerUI = 0;
                infected = false;
            }           
        }
        if (whoIsTheChar == "Julie")
        {
            if (timerUI > 0)
            {
                timerUI -= Time.deltaTime;
                charUIElement.sprite = juliePoses[0];
            }
            else
            {
                charUIElement.sprite = juliePoses[1];
                timerUI = 0;
                infected = false;
            }
        }
        if (whoIsTheChar == "Jimmy")
        {
            if (timerUI > 0)
            {
                timerUI -= Time.deltaTime;
                charUIElement.sprite = jimmyPoses[0];
            }
            else
            {
                charUIElement.sprite = jimmyPoses[1];
                timerUI = 0;
                infected = false;
            }
        }
        if (whoIsTheChar == "Josh")
        {
            if (timerUI > 0)
            {
                timerUI -= Time.deltaTime;
                charUIElement.sprite = joshPoses[0];
            }
            else
            {
                charUIElement.sprite = joshPoses[1];
                timerUI = 0;
                infected = false;
            }
        }
    }

    public void BackToNormal()
    {
        tweenScript.DOPlayById("1");
        statusText.text = "CURED";
        timerUI = animTime;
        infected = false;

        if (whoIsTheChar == "Jack")
        {
            charUIElement.sprite = jackPoses[2];
        }

        humanAgain = false;
    }
}
