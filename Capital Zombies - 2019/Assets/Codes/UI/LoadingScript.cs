﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{
    [Header("UI ELEMENTS TO APPEAR:")]
    public Image bgBlack;
    public GameObject[] loadingObjectsUI;
    bool hasColor = false;

    [Header("IMAGES AND TEXTS TO MASK:")]
    public Sprite[] imagesAlert;
    public Sprite[] imagesGeneral;
    public Sprite[] imagesTips;

    public Image[] maskBGAndLabel;
    public Image mask;

    public Text labelText;
    public Text descrText;

    public string[] descriptionTextAlert;
    public string[] descriptionTextGeneral;
    public string[] descriptionTextTips;

    [Header("TWEENS:")]
    DG.Tweening.DOTweenAnimation[] tweens;

    public string cena;

    void Start()
    {
        tweens = new DG.Tweening.DOTweenAnimation[4];
        tweens[0] = loadingObjectsUI[1].GetComponent<DG.Tweening.DOTweenAnimation>();
        tweens[1] = loadingObjectsUI[2].GetComponent<DG.Tweening.DOTweenAnimation>();
        tweens[2] = loadingObjectsUI[3].GetComponent<DG.Tweening.DOTweenAnimation>();
        tweens[3] = bgBlack.gameObject.GetComponent<DG.Tweening.DOTweenAnimation>();

        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(bgBlack.color.a >= 1 && !hasColor)
        {
            foreach(GameObject obj in loadingObjectsUI)
            {
                obj.SetActive(true);
            }

            int pickRandomSubject = Random.Range(0, 3);

            if(pickRandomSubject == 0)
            {
                labelText.text = "Alerts";
                for(int i = 0; i < maskBGAndLabel.Length; i++)
                {
                    maskBGAndLabel[i].color = Color.red;
                }

                int pickImage = Random.Range(0, imagesAlert.Length);
                mask.sprite = imagesAlert[pickImage];
                descrText.text = descriptionTextAlert[pickImage];
            }
            else if (pickRandomSubject == 1)
            {
                labelText.text = "General";
                for (int i = 0; i < maskBGAndLabel.Length; i++)
                {
                    maskBGAndLabel[i].color = Color.blue;
                }

                int pickImage = Random.Range(0, imagesGeneral.Length);
                mask.sprite = imagesGeneral[pickImage];
                descrText.text = descriptionTextGeneral[pickImage];
            }
            else if (pickRandomSubject == 2)
            {
                labelText.text = "Tips";
                for (int i = 0; i < maskBGAndLabel.Length; i++)
                {
                    maskBGAndLabel[i].color = Color.green;
                }

                int pickImage = Random.Range(0, imagesTips.Length);
                mask.sprite = imagesTips[pickImage];
                descrText.text = descriptionTextTips[pickImage];
            }

            
            hasColor = true;
        }

        /*if (Input.GetKeyDown("r"))
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }*/

    }

    public void destoyThis()
    {
        Destroy(this.gameObject);
    }

    public void LoadScene()
    {
        StartCoroutine(PreLoad());
    }

    IEnumerator PreLoad()
    {
        yield return null;
        AsyncOperation asOperation = SceneManager.LoadSceneAsync(cena);
        asOperation.allowSceneActivation = false;
        Debug.Log("Progress - " + asOperation.progress);

        while (!asOperation.isDone)
        {
            if (asOperation.progress >= 0.9f)
            {
                asOperation.allowSceneActivation = true;

                yield return new WaitForSeconds(3);

                loadingObjectsUI[0].SetActive(false);

                foreach (DG.Tweening.DOTweenAnimation tweenings in tweens)
                {
                    tweenings.DOPlayById("1");
                }
            }

            yield return null;
        }  
    }
}
