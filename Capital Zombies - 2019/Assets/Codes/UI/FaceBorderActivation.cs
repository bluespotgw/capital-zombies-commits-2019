﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaceBorderActivation : MonoBehaviour
{
    [Header("FACE DO PERSONAGEM:")]
    public Image allPlayersFace;
    public GameObject status;
    public int cursorCount = 0;

    //public bool activedFace = false;
    public Image borderFace;

    void Start()
    {
        allPlayersFace.alphaHitTestMinimumThreshold = 0.5f;
    }

    void Update()
    {
        if (cursorCount > 0)
        {
            borderFace.gameObject.SetActive(true);
            status.SetActive(true);
        }
        else
        {
            borderFace.gameObject.SetActive(false);
            status.SetActive(false);
        }
    }
}
