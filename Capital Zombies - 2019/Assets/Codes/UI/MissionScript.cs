﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MissionScript : MonoBehaviour
{
    [Header("MISSION UI:")]
    public GameObject[] missionUI;
    public Image resultIcon;
    public Sprite[] correctWrongIcons;
    public Text missionDescrText;
    public Text counterText;

    [Header("MISSION TYPES VALUES:")]
    public string[] damageMissionsByTimer;
    public string[] damageMissionsByCounter;

    public string[] defenseMissionsByTimer;
    public string[] defenseMissionsByCounter;

    public string[] speedMissionsByTimer;
    public string[] speedMissionsByCounter;

    [Header("CHOOSED PLAYER:")]
    public GameObject[] playerTagID;

    [Header("RESCUE THE CITZEN OBJS:")]
    public GameObject objectiveZone;
    public GameObject[] allSpawnpoints;
    public GameObject citzenOBJ;
    public bool rescueMission = false;

    //DONT RECEIVE DAMAGE OBJS
    float playerLife = 0;
    bool checkLife = false;

    [Header("KILL ONLY PISTOL OBJS:")]
    public int pistolKills = 0;
    public bool pistolMission = false;

    [Header("GET WALLETS OBJS:")]
    public int walletsCount = 0;
    public bool getWalletMission = false;

    [Header("FPM OBJS:")]
    public bool fpmMission = false;

    [Header("DONT JUMP OBJS:")]
    public bool jumpMission = false;

    [Header("GRENADES OBJS:")]
    public bool throwGrenadeMission = false;
    public int grenadesEnemyCount = 0;

    [Header("SPECIAL ZOMBIE OBJS:")]
    public bool specialZombieMission = false;
    public int specialCount = 0;
    public GameObject specialZombie;
    public GameObject[] allSpawners;

    [Header("JUMP/KILL MISSION OBJS:")]
    public bool jumpAndKillMission = false;

    [Header("DONT KILL ZOMBIES OBJS:")]
    public bool dontKillZombiesMission = false;

    [Header("COLLECT AMMO WALLETS OBJS:")]
    public bool getAmmoWallet = false;
    public int ammoWalletsCount = 0;

    [Header("DEFEAT SPECIAL ENEMY OBJS:")]
    public bool defeatSpecialEnemyMission = false;
    public GameObject specialEnemy;
    public int specialECount = 0;
    GameObject specialExist;
    BigZ bigzScript;
    //usa os spawn do zumbi especial

    [Header("WAVE MISSION:")]
    public GameObject wave;
    public Material particleWaveColor;
    public Light particleLight;

    [Header("ACTIVE POWER:")]
    public GameObject speedTrail;
    public bool damagePower = false;
    public bool defensePower = false;
    public bool speedPower = false;
    public float damagePowerTime = 0;
    public float defensePowerTime = 0;
    public float speedPowerTime = 0;
    float timerDm = 0;
    float timerDf = 0;
    float timerSp = 0;

    [Space(20)]

    public float timeToMission = 0;
    public int howManyPlayers = 0;
    int missionSelectedTimer = 0;
    int missionSelectedCounter = 0;

    public bool missionFailed = false;
    public bool missionCompleted = false;

    float timer = 0;

    int randomMission = 0;
    int isTimerOrCounter = 0;
    int limitItens = 0;

    float timerOfMissionInUI = 120;
    int counterOfMissionInUI = 0;
    bool canCount = true;

    HeroBehavior playerChoosed;
    public GlobalGameSelections itemScript;

    DG.Tweening.DOTweenAnimation tweens;
    Color uiColor;

    void Start()
    {
        timer = Random.Range(15,timeToMission);

        foreach (GameObject obj in missionUI)
        {
            obj.SetActive(false);
        }

        if (itemScript.multiplayerSelected)
        {
            this.GetComponent<CanvasScaler>().referenceResolution = new Vector2(2500, 600);
        }

        uiColor = resultIcon.GetComponent<Image>().color;

        timerDm = damagePowerTime;
        timerDf = defensePowerTime;
        timerSp = speedPowerTime;
    }

    void Update()
    {
        /*if (Input.GetKeyDown("r"))
        {
            foreach (GameObject obj in missionUI)
            {
                obj.SetActive(false);
            }

            timerOfMissionInUI = 5;
            timer = Random.Range(1, timeToMission);
            canCount = true;
            resultIcon.gameObject.SetActive(false);
            missionCompleted = false;
            missionFailed = false;
        }*/

        if(timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            if (canCount)
            {               
                foreach (GameObject obj in missionUI)
                {
                    obj.SetActive(true);
                }

                randomMission = Random.Range(1, 4);
                //randomMission = 1;
                isTimerOrCounter = Random.Range(1, 3);
                //isTimerOrCounter = 2;

                //print(randomMission);
                //print(isTimerOrCounter);

                GetComponent<Canvas>().enabled = true;

                for (int i = 0; i < 4; i++)
                {
                    playerTagID[i] = GameObject.FindWithTag("Player" + (i + 1));

                    if (playerTagID[i] != null)
                    {
                        howManyPlayers++;
                    }
                }

                //----------------------------------------
                playerChoosed = playerTagID[Random.Range(0, howManyPlayers)].GetComponent<HeroBehavior>();              

                if (playerChoosed == null)
                {
                    missionFailed = true;
                }

                this.GetComponent<Canvas>().worldCamera = playerChoosed.transform.parent.GetChild(0).GetChild(0).GetComponent<Camera>();
                this.GetComponent<Canvas>().planeDistance = 1;

                playerChoosed.isInMission = true;
                //-------------------------------------------

                if (randomMission == 1)//Damage Missions
                {
                    for (int i = 0; i < missionUI.Length; i++)
                    {
                        missionUI[i].gameObject.GetComponent<Image>().color = Color.red;
                    }
                    
                    if (isTimerOrCounter == 1)//Time missions
                    {
                        missionSelectedTimer = Random.Range(0, damageMissionsByTimer.Length);
                        //missionSelectedTimer = 0;
                        missionDescrText.text = damageMissionsByTimer[missionSelectedTimer];
                        
                        if (missionSelectedTimer == 0)//Misison citzen
                        {
                            //instancia o citzen aqui
                            int randZone = Random.Range(0, allSpawnpoints.Length);
                            GameObject newClone = Instantiate(citzenOBJ, allSpawnpoints[randZone].transform.position, allSpawnpoints[randZone].transform.rotation);
                            objectiveZone.SetActive(true);

                            newClone.GetComponent<Citzen>().playerToFollow = playerChoosed.transform;
                            newClone.GetComponent<Citzen>().startQuest = true;

                            rescueMission = true;
                            
                        }
                        else if (missionSelectedTimer == 1)//Mission Life = Life
                        {
                            checkLife = true;
                            playerLife = playerChoosed.life;
                        }
                    }
                    else if (isTimerOrCounter == 2)//Counter Mission
                    {
                        missionSelectedCounter = Random.Range(0, damageMissionsByCounter.Length);
                        //missionSelectedCounter = 0;
                        missionDescrText.text = damageMissionsByCounter[missionSelectedCounter];

                        if(missionSelectedCounter == 0)//Mission Kill pistol
                        {
                            pistolMission = true;
                            limitItens = 5;
                        }
                        else if(missionSelectedCounter == 1)//Mission Wallet
                        {
                            getWalletMission = true;
                            limitItens = 2;
                        }
                    }
                }
                else if (randomMission == 2)
                {
                    for (int i = 0; i < missionUI.Length; i++)
                    {
                        missionUI[i].gameObject.GetComponent<Image>().color = Color.blue;
                    }

                    if (isTimerOrCounter == 1)
                    {
                        missionSelectedTimer = Random.Range(0, defenseMissionsByTimer.Length);
                        //missionSelectedTimer = 1;
                        missionDescrText.text = defenseMissionsByTimer[missionSelectedTimer];

                        if(missionSelectedTimer == 0)//Mission FPM
                        {
                            fpmMission = true;
                        }
                        else if(missionSelectedTimer == 1)//Mission Jump
                        {
                            jumpMission = true;
                        }
                    }
                    else if (isTimerOrCounter == 2)
                    {
                        missionSelectedCounter = Random.Range(0, defenseMissionsByCounter.Length);
                        //missionSelectedCounter = 1;
                        missionDescrText.text = defenseMissionsByCounter[missionSelectedCounter];

                        if (missionSelectedCounter == 0)//Mission Grenades
                        {
                            throwGrenadeMission = true;
                            limitItens = 7;
                        }
                        else if (missionSelectedCounter == 1)//Mission Special Zombie
                        {
                            specialZombieMission = true;
                            limitItens = 1;
                            int randomSpawner = Random.Range(0, allSpawners.Length);
                            GameObject zombie = Instantiate(specialZombie, allSpawners[0].transform.position, allSpawners[0].transform.rotation);
                            StandardZombie zScript = zombie.GetComponent<StandardZombie>();
                            zScript.specialZombie = true;
                            zScript.lifeEnemy = 500;
                            zScript.blueArrow.SetActive(true);
                        }
                    }
                }
                else if (randomMission == 3)
                {
                    for (int i = 0; i < missionUI.Length; i++)
                    {
                        missionUI[i].gameObject.GetComponent<Image>().color = new Color32(81,138,4,255);
                    }

                    if (isTimerOrCounter == 1)
                    {
                        missionSelectedTimer = Random.Range(0, speedMissionsByTimer.Length);
                        //missionSelectedTimer = 1;
                        missionDescrText.text = speedMissionsByTimer[missionSelectedTimer];

                        if (missionSelectedTimer == 0)//Mission Jump and Kill
                        {
                            jumpAndKillMission = true;
                        }
                        else if (missionSelectedTimer == 1)//Mission dont kill zombies
                        {
                            dontKillZombiesMission = true;
                        }
                        
                    }
                    else if (isTimerOrCounter == 2)
                    {
                        missionSelectedCounter = Random.Range(0, speedMissionsByCounter.Length);
                        //missionSelectedCounter = 0;
                        missionDescrText.text = speedMissionsByCounter[missionSelectedCounter];

                        if(missionSelectedCounter == 0)//Mission ammo wallets
                        {
                            getAmmoWallet = true;
                            limitItens = 2;
                        }
                        else if (missionSelectedCounter == 1 && !defeatSpecialEnemyMission)// Mission special BigZ
                        {
                            limitItens = 1;

                            int randomSpawner = Random.Range(0, allSpawners.Length);
                            GameObject specialE = Instantiate(specialEnemy, allSpawners[0].transform.position, allSpawners[0].transform.rotation);
                            bigzScript = specialE.GetComponent<BigZ>();

                            defeatSpecialEnemyMission = true;
                        }
                    }
                }

                canCount = false;
            }

            if (isTimerOrCounter == 1)
            {
                if (timerOfMissionInUI > 1 && !missionCompleted && !missionFailed)
                {
                    timerOfMissionInUI -= Time.deltaTime;
                    int min = Mathf.FloorToInt(timerOfMissionInUI / 60);
                    int sec = Mathf.FloorToInt(timerOfMissionInUI % 60);
                    counterText.text = min.ToString("00") + ":" + sec.ToString("00");
                }
                else
                {
                    timerOfMissionInUI = 0;
                    resultIcon.gameObject.SetActive(true);
                    resultIcon.sprite = correctWrongIcons[1];
                    resultIcon.color = new Color(1, 0, 0, resultIcon.color.a);

                    //-----------
                    if (!missionCompleted && !fpmMission && !checkLife && !jumpMission && !jumpAndKillMission &&
                        !dontKillZombiesMission)
                    {
                        missionFailed = true;
                    }
                    else
                    {
                        //missionFailed = false;
                    }
                    //-----------
                    if (checkLife)
                    {
                        missionCompleted = true;
                        damagePower = true;
                        checkLife = false;
                    }

                    if (fpmMission && !missionFailed)
                    {
                        missionCompleted = true;
                        defensePower = true;
                        fpmMission = false;
                    }

                    if (jumpMission && !missionFailed)//DontJump
                    {
                        missionCompleted = true;
                        defensePower = true;
                        jumpMission = false;
                    }

                    if (jumpAndKillMission)
                    {
                        jumpAndKillMission = false;
                    }

                    if (dontKillZombiesMission && !missionFailed)
                    {
                        missionCompleted = true;
                        speedPower = true;
                        dontKillZombiesMission = false;
                    }

                    if (rescueMission)
                    {
                        objectiveZone.SetActive(false);
                        GameObject citzen = GameObject.FindWithTag("Citzen");

                        if (citzen != null)
                        {
                            citzen.GetComponent<Citzen>().life = 0;
                            print("found");
                        }

                        rescueMission = false;
                    }
                }

                if (missionCompleted)
                {
                    resultIcon.gameObject.SetActive(true);
                    resultIcon.sprite = correctWrongIcons[0];
                    resultIcon.color = new Color(0, 1, 0, resultIcon.color.a);

                    if (missionSelectedTimer == 0)//Resgate do cidadao
                    {
                        objectiveZone.SetActive(false);
                    }
                }

                if (missionFailed)
                {
                    resultIcon.gameObject.SetActive(true);
                    resultIcon.sprite = correctWrongIcons[1];
                    resultIcon.color = new Color(1, 0, 0, resultIcon.color.a);
                }
            }
            else if (isTimerOrCounter == 2)
            {
                counterText.text = counterOfMissionInUI.ToString() + "<color=orange>/" + limitItens + "</color>";

                if (missionFailed)
                {
                    resultIcon.gameObject.SetActive(true);
                    resultIcon.sprite = correctWrongIcons[1];
                    resultIcon.color = new Color(1, 0, 0, resultIcon.color.a);
                }

                if (missionCompleted)
                {
                    resultIcon.gameObject.SetActive(true);
                    resultIcon.sprite = correctWrongIcons[0];
                    resultIcon.color = new Color(0, 1, 0, resultIcon.color.a);
                }
            }
        }

        if (checkLife)
        {
            float oldLife = playerChoosed.life;

            if(oldLife != playerLife)
            {
                missionFailed = true;
                checkLife = false;
            }
        }

        if (pistolMission)
        {
            counterOfMissionInUI = pistolKills;

            if (pistolKills >= 5)
            {
                missionCompleted = true;
                damagePower = true;
                pistolKills = 0;
                pistolMission = false;
            }
        }

        if (getWalletMission)
        {
            counterOfMissionInUI = walletsCount;

            if(walletsCount >= limitItens)
            {
                missionCompleted = true;
                damagePower = true;
                walletsCount = 0;
                getWalletMission = false;
            }
        }

        if (throwGrenadeMission)
        {
            counterOfMissionInUI = grenadesEnemyCount;

            if(grenadesEnemyCount >= limitItens)
            {
                missionCompleted = true;
                defensePower = true;
                grenadesEnemyCount = 0;
                throwGrenadeMission = false;
            }
        }

        if (specialZombieMission)
        {
            counterOfMissionInUI = specialCount;

            if(specialCount >= limitItens)
            {
                missionCompleted = true;
                defensePower = true;
                defensePower = true;
                specialCount = 0;
                specialZombieMission = false;
            }
        }

        if (getAmmoWallet)
        {
            counterOfMissionInUI = ammoWalletsCount;

            if(ammoWalletsCount >= limitItens)
            {
                missionCompleted = true;
                speedPower = true;
                ammoWalletsCount = 0;
                getAmmoWallet = false;
            }
        }

        if (defeatSpecialEnemyMission)
        {
            counterOfMissionInUI = specialECount;

            if (playerChoosed != null)
            {
                bigzScript.playerToFollow = playerChoosed.gameObject;
            } 
            
            if(specialECount >= 1)
            {
                missionCompleted = true;
                speedPower = true;
                specialECount = 0;
                defeatSpecialEnemyMission = false;
            }
        }

        if (damagePower)
        {
            wave = playerChoosed.wave;
            particleLight = playerChoosed.particleLight;
            particleWaveColor = playerChoosed.particleWaveColor;

            playerChoosed.damageMultiplier = 100;

            timerDm -= Time.deltaTime;
            wave.SetActive(true);
            particleWaveColor.color = Color.red;
            particleLight.color = Color.red;

            playerChoosed.faceUIAnimation.SetBool("Flip", true);
            playerChoosed.faceUIScript.actualStatus = "Damage Up";           

            if (timerDm <= 0)
            {
                wave.SetActive(false);
                playerChoosed.damageMultiplier = 1;
                timerDm = damagePowerTime;

                playerChoosed.faceUIAnimation.SetBool("Flip", false);
                playerChoosed.faceUIScript.actualStatus = "";
                playerChoosed.headIconsUI.gameObject.SetActive(false);

                damagePower = false;
            }
        }

        if (defensePower)
        {
            wave = playerChoosed.wave;
            particleLight = playerChoosed.particleLight;
            particleWaveColor = playerChoosed.particleWaveColor;

            playerChoosed.life = 100;

            timerDf -= Time.deltaTime;
            wave.SetActive(true);
            particleWaveColor.color = Color.blue;
            particleLight.color = Color.blue;

            playerChoosed.faceUIAnimation.SetBool("Flip", true);
            playerChoosed.faceUIScript.actualStatus = "Defense Up";
            playerChoosed.faceUIScript.imageIconHead.SetActive(false);

            if (timerDf <= 0)
            {
                wave.SetActive(false);
                timerDf = damagePowerTime;

                playerChoosed.faceUIAnimation.SetBool("Flip", false);
                playerChoosed.faceUIScript.actualStatus = "";
                playerChoosed.headIconsUI.gameObject.SetActive(false);

                defensePower = false;
            }
        }

        if (speedPower)
        {
            wave = playerChoosed.wave;
            particleLight = playerChoosed.particleLight;
            particleWaveColor = playerChoosed.particleWaveColor;
            speedTrail = playerChoosed.speedTrail;
            speedTrail.SetActive(true);

            playerChoosed.anim.speed = 1.35f;

            timerSp -= Time.deltaTime;
            wave.SetActive(true);
            particleWaveColor.color = Color.green;
            particleLight.color = Color.green;

            playerChoosed.faceUIAnimation.SetBool("Flip", true);
            playerChoosed.faceUIScript.actualStatus = "Speed Up";

            if (timerSp <= 0)
            {
                wave.SetActive(false);
                speedTrail.SetActive(false);
                playerChoosed.anim.speed = 1;
                timerSp = damagePowerTime;

                playerChoosed.faceUIAnimation.SetBool("Flip", false);
                playerChoosed.faceUIScript.actualStatus = "";
                playerChoosed.headIconsUI.gameObject.SetActive(false);

                speedPower = false;
            }
        }
    }

    public void MissionComplete()
    {
        foreach (GameObject obj in missionUI)
        {
            obj.SetActive(false);
        }

        howManyPlayers = 0;
        timerOfMissionInUI = 120;
        timer = Random.Range(1, timeToMission);
        canCount = true;
        resultIcon.gameObject.SetActive(false);
        missionCompleted = false;
        missionFailed = false;
        playerChoosed.isInMission = false;

        //Caso morra a missao reseta
        
        pistolKills = 0;
        pistolMission = false;

        walletsCount = 0;
        getWalletMission = false;

        grenadesEnemyCount = 0;
        throwGrenadeMission = false;

        specialZombieMission = false;

        getAmmoWallet = false;

        missionSelectedTimer = 0;
        missionSelectedCounter = 0;
        randomMission = 0;
        isTimerOrCounter = 0;
    }
}
