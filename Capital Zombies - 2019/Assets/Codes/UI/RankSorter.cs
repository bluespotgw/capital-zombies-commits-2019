﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class RankSorter : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("RANK ELEMENTS:")]
    public Text[] killInfo;
    public Text[] deathInfo;
    public Text[] totalInfo;
    public Text[] playerID;
    public Text[] rankNumber;
    public Image[] playersIMG;
    public Sprite[] playersPics;

    public GlobalGameSelections itemScript;
    GameDirector directorScript; 
    GameObject directorOBJ;

    int loopControl = 0;
    public string pos1Name = "";
    public string pos2Name = "";
    public string pos3Name = "";
    public string pos4Name = "";

    public bool hasShowed = false;

    int loopCount = 0;

    void Start()
    {
        directorOBJ = GameObject.Find("Director");
        directorScript = directorOBJ.GetComponent<GameDirector>();
    }

    void Update()
    {
        if (this.gameObject.activeInHierarchy && !hasShowed || this.gameObject.activeInHierarchy && directorScript.notMoreTied)
        {
            directorScript.RankUpdate();
            CheckRank();            
        }
        else
        {
            //hasShowed = false;
        }

        if (loopCount >= 6)
        {
            hasShowed = true;
        }
    }

    void CheckRank()
    {
        for(int i = 0; i < directorScript.rank.Length; i++)
        {
            //print(loopControl);
            loopCount++;

            if (directorScript.killsP[i] - directorScript.deathsP[i] == directorScript.rank[0] && loopControl == 0)
            {
                if (directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[0])
                {
                    playerID[0].text = "Player 1x";
                    pos1Name = "Player 1";

                    if(itemScript.heroName[0] != "")
                    {
                        string playerSelected = itemScript.heroName[0] + " Load Image";
                        playersIMG[0].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                    }
                    else
                    {
                        playersIMG[0].sprite = playersPics[4];
                    }                    
                }
                else if (directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[0])
                {
                    playerID[0].text = "Player 2x";
                    pos1Name = "Player 2";

                    if(itemScript.heroName[1] != "")
                    {
                        string playerSelected = itemScript.heroName[1] + " Load Image";
                        playersIMG[0].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                    }
                    else
                    {
                        playersIMG[0].sprite = playersPics[4];
                    }
                    
                }
                else if (directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[0])
                {
                    playerID[0].text = "Player 3x";
                    pos1Name = "Player 3";

                    if (itemScript.heroName[2] != "")
                    {
                        string playerSelected = itemScript.heroName[2] + " Load Image";
                        playersIMG[0].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                    }
                    else
                    {
                        playersIMG[0].sprite = playersPics[4];
                    }
                }
                else if (directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[0])
                {
                    playerID[0].text = "Player 4x";
                    pos1Name = "Player 4";

                    if (itemScript.heroName[3] != "")
                    {
                        string playerSelected = itemScript.heroName[3] + " Load Image";
                        playersIMG[0].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                    }
                    else
                    {
                        playersIMG[0].sprite = playersPics[4];
                    }                        
                }

                rankNumber[0].text = directorScript.playersOrderRank[0].ToString() + "°";
                killInfo[0].text = directorScript.killsP[i].ToString();
                deathInfo[0].text = directorScript.deathsP[i].ToString();
                totalInfo[0].text = directorScript.rank[0].ToString();
                
                loopControl = 1;
            }
            else if (directorScript.killsP[i] - directorScript.deathsP[i] == directorScript.rank[1] && loopControl == 1)
            {
                if (directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[1])
                {
                    if (pos1Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 1y";
                        pos2Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }
                            
                    }
                    else if (pos1Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 2y";
                        pos2Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }
                            
                    }
                    else if (pos1Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 3y";
                        pos2Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }
                            
                    }
                    else if (pos1Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 4y";
                        pos2Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }   
                    }
                }
                else if (directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[1])
                {
                    if (pos1Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 1y";
                        pos2Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 2y";
                        pos2Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }                            
                    }
                    else if (pos1Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 3y";
                        pos2Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }                            
                    }
                    else if (pos1Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 4y";
                        pos2Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }                            
                    }
                }
                else if (directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[1])
                {
                    if (pos1Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 1y";
                        pos2Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 2y";
                        pos2Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }
                            
                    }
                    else if (pos1Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 3y";
                        pos2Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        } 
                    }
                    else if (pos1Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 4y";
                        pos2Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }
                    }
                }
                else if (directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[1])
                {
                    if (pos1Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 1y";
                        pos2Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 2y";
                        pos2Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }                            
                    }
                    else if (pos1Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 3y";
                        pos2Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[1])
                    {
                        playerID[1].text = "Player 4y";
                        pos2Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[1].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[1].sprite = playersPics[4];
                        }    
                    }
                }

                rankNumber[1].text = directorScript.playersOrderRank[1].ToString() + "°";
                killInfo[1].text = directorScript.killsP[i].ToString();
                deathInfo[1].text = directorScript.deathsP[i].ToString();
                totalInfo[1].text = directorScript.rank[1].ToString();
                loopControl = 2;
            }
            else if(directorScript.killsP[i] - directorScript.deathsP[i] == directorScript.rank[2] && loopControl == 2)
            {
                if (directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[2])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 1z";
                        pos3Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }                           
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 2z";
                        pos3Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 3z";
                        pos3Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 4z";
                        pos3Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                }
                else if (directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[2])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 1z";
                        pos3Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 2z";
                        pos3Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 3z";
                        pos3Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 4z";
                        pos3Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                }
                else if (directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[2])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 1z";
                        pos3Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 2z";
                        pos3Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 3z";
                        pos3Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 4z";
                        pos3Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                }
                else if (directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[2])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 1z";
                        pos3Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }    
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 2z";
                        pos3Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 3z";
                        pos3Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[2])
                    {
                        playerID[2].text = "Player 4z";
                        pos3Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[2].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[2].sprite = playersPics[4]; 
                        }   
                    }
                }

                rankNumber[2].text = directorScript.playersOrderRank[2].ToString() + "°";
                killInfo[2].text = directorScript.killsP[i].ToString();
                deathInfo[2].text = directorScript.deathsP[i].ToString();
                totalInfo[2].text = directorScript.rank[2].ToString();
                loopControl = 3;
            }
            else if(directorScript.killsP[i] - directorScript.deathsP[i] == directorScript.rank[3] && loopControl == 3)
            {
                
                if (directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[3])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && pos3Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 1a";
                        pos4Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && pos3Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 2a";
                        pos4Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && pos3Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 3a";
                        pos4Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }   
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && pos3Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 4a";
                        pos4Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }   
                    }
                }
                else if (directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[3])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && pos3Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 1a";
                        pos4Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }  
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && pos3Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 2a";
                        pos4Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && pos3Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 3a";
                        pos4Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && pos3Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 4a";
                        pos4Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                }
                else if (directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[3])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && pos3Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 1a";
                        pos4Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && pos3Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 2a";
                        pos4Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && pos3Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 3a";
                        pos4Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && pos3Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 4a";
                        pos4Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                }
                else if (directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[3])
                {
                    if (pos1Name != "Player 1" && pos2Name != "Player 1" && pos3Name != "Player 1" && directorScript.killsP[0] - directorScript.deathsP[0] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 1a";
                        pos4Name = "Player 1";

                        if (itemScript.heroName[0] != "")
                        {
                            string playerSelected = itemScript.heroName[0] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 2" && pos2Name != "Player 2" && pos3Name != "Player 2" && directorScript.killsP[1] - directorScript.deathsP[1] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 2a";
                        pos4Name = "Player 2";

                        if (itemScript.heroName[1] != "")
                        {
                            string playerSelected = itemScript.heroName[1] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 3" && pos2Name != "Player 3" && pos3Name != "Player 3" && directorScript.killsP[2] - directorScript.deathsP[2] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 3a";
                        pos4Name = "Player 3";

                        if (itemScript.heroName[2] != "")
                        {
                            string playerSelected = itemScript.heroName[2] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                    else if (pos1Name != "Player 4" && pos2Name != "Player 4" && pos3Name != "Player 4" && directorScript.killsP[3] - directorScript.deathsP[3] == directorScript.rank[3])
                    {
                        playerID[3].text = "Player 4a";
                        pos4Name = "Player 4";

                        if (itemScript.heroName[3] != "")
                        {
                            string playerSelected = itemScript.heroName[3] + " Load Image";
                            playersIMG[3].sprite = playersPics[Array.FindIndex(playersPics, s => s.name == playerSelected)];
                        }
                        else
                        {
                            playersIMG[3].sprite = playersPics[4];
                        }
                    }
                }

                rankNumber[3].text = directorScript.playersOrderRank[3].ToString() + "°";
                killInfo[3].text = directorScript.killsP[i].ToString();
                deathInfo[3].text = directorScript.deathsP[i].ToString();
                totalInfo[3].text = directorScript.rank[3].ToString();
                loopControl = 0;                
            }
        }
    }
}
