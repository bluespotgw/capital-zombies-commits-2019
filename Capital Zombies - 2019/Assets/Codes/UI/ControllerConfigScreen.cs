﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ControllerConfigScreen : MonoBehaviour
{
    [Header("CONTROLLER TYPE ICONS")]
    public InputField[] selectionBoxes;

    [Header("ALL HERO DATAS:")]
    public HeroData[] playersKeys;

    [Header("TESTER CHAR:")]
    public HeroBehavior testerHeroKey;
    public HeroBehavior testerHeroJoy;

    [Header("P1 COMMANDS")]
    public string[] p1Commands;//O que vai pro codigo do char
    public string[] p1InputNames;//Fica apenas na tela de selecao
    bool showP1Inputs = false;

    [Header("P2 COMMANDS")]
    public string[] p2Commands;
    public string[] p2InputNames;
    bool showP2Inputs = false;

    [Header("P3 COMMANDS")]
    public string[] p3Commands;
    public string[] p3InputNames;
    bool showP3Inputs = false;

    [Header("P4 COMMANDS")]
    public string[] p4Commands;
    public string[] p4InputNames;
    bool showP4Inputs = false;

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    public Image[] joyIcons;
    public DG.Tweening.DOTweenAnimation[] selectionPlayerCircles;
    public GameObject keyboardUIElement;

    bool p1Turn = true;
    bool p2Turn = false;
    bool p3Turn = false;
    bool p4Turn = false;
    bool chooseInput = false;
    string actualInput = "";
    int inputOrder = 0;

    string[] keyboardKeys = new string[] {
     "backspace",
     "delete",
     "tab",
     "clear",
     "return",
     "pause",
     "escape",
     "space",
     "up",
     "down",
     "right",
     "left",
     "insert",
     "home",
     "end",
     "page up",
     "page down",
     "f1",
     "f2",
     "f3",
     "f4",
     "f5",
     "f6",
     "f7",
     "f8",
     "f9",
     "f10",
     "f11",
     "f12",
     "f13",
     "f14",
     "f15",
     "0",
     "1",
     "2",
     "3",
     "4",
     "5",
     "6",
     "7",
     "8",
     "9",
     "!",
     "\"",
     "#",
     "$",
     "&",
     "'",
     "(",
     ")",
     "*",
     "+",
     ",",
     "-",
     ".",
     "/",
     ":",
     ";",
     "<",
     "=",
     ">",
     "?",
     "@",
     "[",
     "\\",
     "]",
     "^",
     "_",
     "`",
     "a",
     "b",
     "c",
     "d",
     "e",
     "f",
     "g",
     "h",
     "i",
     "j",
     "k",
     "l",
     "m",
     "n",
     "o",
     "p",
     "q",
     "r",
     "s",
     "t",
     "u",
     "v",
     "w",
     "x",
     "y",
     "z",
     "numlock",
     "caps lock",
     "scroll lock",
     "right shift",
     "left shift",
     "right ctrl",
     "left ctrl",
     "right alt",
     "left alt"
    };

    public int p1GunIndex = 0;
    public int p2GunIndex = 0;
    public int p3GunIndex = 0;
    public int p4GunIndex = 0;

    void Start()
    {
        m_Raycaster = GetComponentInParent<GraphicRaycaster>();
        m_EventSystem = GetComponent<EventSystem>();
        p1Turn = true;
    }

    // Update is called once per frame
    void Update()
    {
        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();

        m_Raycaster.Raycast(m_PointerEventData, results);

        foreach (RaycastResult result in results)
        {
            if (Input.GetMouseButtonDown(0))
            {
                int indexOfItem = Array.IndexOf(joyIcons, result.gameObject.GetComponent<Image>());

                //----------------------------
                if (result.gameObject.transform.name == "Player Circ 1")
                {
                    selectionPlayerCircles[0].DOPlay();
                    selectionPlayerCircles[1].DORewind();
                    selectionPlayerCircles[2].DORewind();
                    selectionPlayerCircles[3].DORewind();
                    p1Turn = true;
                    p2Turn = false;
                    p3Turn = false;
                    p4Turn = false;

                    if (!showP1Inputs)
                    {
                        if (p1GunIndex == 0)
                        {
                            for (int i = 0; i < 15; i++)
                            {
                                selectionBoxes[i].text = playersKeys[0].p1KeyboardKeys[i];
                            }
                        }
                        else
                        {
                            for (int i = 0; i < 15; i++)
                            {
                                selectionBoxes[i].text = playersKeys[0].p1JoystickKeys[i];
                            }
                        }

                        for (int i = 0; i < 4; i++)
                        {
                            if (joyIcons[i].color != Color.white)
                            {
                                joyIcons[i].color = Color.white;
                            }
                        }

                        joyIcons[p1GunIndex].color = new Color32(255, 132, 0, 255);

                        showP4Inputs = false;
                        showP3Inputs = false;
                        showP2Inputs = false;
                        showP1Inputs = true;
                    }

                    if (indexOfItem == 0)
                    {
                        testerHeroKey.transform.parent.gameObject.SetActive(true);
                        testerHeroJoy.transform.parent.gameObject.SetActive(false);

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p1KeyboardKeys[i];
                        }
                    }
                    else if (indexOfItem == 1)
                    {
                        testerHeroKey.transform.parent.gameObject.SetActive(false);
                        testerHeroJoy.transform.parent.gameObject.SetActive(true);

                        testerHeroJoy.gameControlToPlay = "Xbox";

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p1JoystickKeys[i];
                        }

                    }
                    else if (indexOfItem == 2)
                    {
                        testerHeroKey.transform.parent.gameObject.SetActive(false);
                        testerHeroJoy.transform.parent.gameObject.SetActive(true);

                        testerHeroJoy.gameControlToPlay = "Playstation";

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p1JoystickKeys[i];
                        }
                    }
                    else if (indexOfItem == 3)
                    {
                        testerHeroKey.transform.parent.gameObject.SetActive(false);
                        testerHeroJoy.transform.parent.gameObject.SetActive(true);

                        testerHeroJoy.gameControlToPlay = "Generic";

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p1JoystickKeys[i];
                        }
                    }

                    keyboardUIElement.SetActive(true);
                }
                if (result.gameObject.transform.name == "Player Circ 2")
                {
                    selectionPlayerCircles[0].DORewind();
                    selectionPlayerCircles[1].DOPlay();
                    selectionPlayerCircles[2].DORewind();
                    selectionPlayerCircles[3].DORewind();
                    p1Turn = false;
                    p2Turn = true;
                    p3Turn = false;
                    p4Turn = false;

                    testerHeroKey.transform.parent.gameObject.SetActive(false);
                    testerHeroJoy.transform.parent.gameObject.SetActive(true);

                    if (!showP2Inputs)
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[1].p2keys[i];
                        }

                        for (int i = 0; i < 4; i++)
                        {
                            if (joyIcons[i].color != Color.white)
                            {
                                joyIcons[i].color = Color.white;
                            }
                        }

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p2keys[i];
                        }

                        joyIcons[p2GunIndex].color = new Color32(255, 132, 0, 255);

                        showP4Inputs = false;
                        showP3Inputs = false;
                        showP2Inputs = true;
                        showP1Inputs = false;
                    }

                    if (indexOfItem == 1)
                    {
                        testerHeroJoy.gameControlToPlay = "Xbox";

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p2keys[i];
                        }

                    }
                    else if (indexOfItem == 2)
                    {
                        testerHeroJoy.gameControlToPlay = "Playstation";
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p2keys[i];
                        }
                    }
                    else if (indexOfItem == 3)
                    {
                        testerHeroJoy.gameControlToPlay = "Generic";
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p2keys[i];
                        }
                    }

                    keyboardUIElement.SetActive(false);
                }
                if (result.gameObject.transform.name == "Player Circ 3")
                {
                    selectionPlayerCircles[0].DORewind();
                    selectionPlayerCircles[1].DORewind();
                    selectionPlayerCircles[2].DOPlay();
                    selectionPlayerCircles[3].DORewind();
                    p1Turn = false;
                    p2Turn = false;
                    p3Turn = true;
                    p4Turn = false;

                    testerHeroKey.transform.parent.gameObject.SetActive(false);
                    testerHeroJoy.transform.parent.gameObject.SetActive(true);

                    if (!showP3Inputs)
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[2].p3keys[i];
                        }

                        for (int i = 0; i < 4; i++)
                        {
                            if (joyIcons[i].color != Color.white)
                            {
                                joyIcons[i].color = Color.white;
                            }
                        }

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p3keys[i];
                        }

                        joyIcons[p3GunIndex].color = new Color32(255, 132, 0, 255);

                        showP4Inputs = false;
                        showP3Inputs = true;
                        showP2Inputs = false;
                        showP1Inputs = false;
                    }

                    if (indexOfItem == 1)
                    {
                        testerHeroJoy.gameControlToPlay = "Xbox";

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p3keys[i];
                        }

                    }
                    else if (indexOfItem == 2)
                    {
                        testerHeroJoy.gameControlToPlay = "Playstation";
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p3keys[i];
                        }
                    }
                    else if (indexOfItem == 3)
                    {
                        testerHeroJoy.gameControlToPlay = "Generic";
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p3keys[i];
                        }
                    }

                    keyboardUIElement.SetActive(false);
                }
                if (result.gameObject.transform.name == "Player Circ 4")
                {
                    selectionPlayerCircles[0].DORewind();
                    selectionPlayerCircles[1].DORewind();
                    selectionPlayerCircles[2].DORewind();
                    selectionPlayerCircles[3].DOPlay();
                    p1Turn = false;
                    p2Turn = false;
                    p3Turn = false;
                    p4Turn = true;

                    testerHeroKey.transform.parent.gameObject.SetActive(false);
                    testerHeroJoy.transform.parent.gameObject.SetActive(true);

                    if (!showP4Inputs)
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[3].p4keys[i];
                        }

                        for (int i = 0; i < 4; i++)
                        {
                            if (joyIcons[i].color != Color.white)
                            {
                                joyIcons[i].color = Color.white;
                            }
                        }

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p4keys[i];
                        }

                        joyIcons[p4GunIndex].color = new Color32(255, 132, 0, 255);

                        showP4Inputs = true;
                        showP3Inputs = false;
                        showP2Inputs = false;
                        showP1Inputs = false;
                    }

                    if (indexOfItem == 1)
                    {
                        testerHeroJoy.gameControlToPlay = "Xbox";

                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p4keys[i];
                        }

                    }
                    else if (indexOfItem == 2)
                    {
                        testerHeroJoy.gameControlToPlay = "Playstation";
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p4keys[i];
                        }
                    }
                    else if (indexOfItem == 3)
                    {
                        testerHeroJoy.gameControlToPlay = "Generic";
                        for (int i = 0; i < 15; i++)
                        {
                            selectionBoxes[i].text = playersKeys[0].p4keys[i];
                        }
                    }

                    keyboardUIElement.SetActive(false);
                }

                //-------------------------------

                if (indexOfItem >= 0) {                  
                    if (result.gameObject.name == joyIcons[indexOfItem].gameObject.name)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            if (joyIcons[i].color != Color.white)
                            {
                                joyIcons[i].color = Color.white;
                            }
                        }

                        joyIcons[Array.IndexOf(joyIcons, result.gameObject.GetComponent<Image>())].color = new Color32(255, 132, 0, 255);

                        if (p1Turn)
                        {
                            p1GunIndex = Array.IndexOf(joyIcons, result.gameObject.GetComponent<Image>());
                            
                            if (indexOfItem == 0)
                            {
                                testerHeroKey.transform.parent.gameObject.SetActive(true);
                                testerHeroJoy.transform.parent.gameObject.SetActive(false);

                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p1KeyboardKeys[i];
                                }
                            }
                            else if (indexOfItem == 1)
                            {
                                testerHeroKey.transform.parent.gameObject.SetActive(false);
                                testerHeroJoy.transform.parent.gameObject.SetActive(true);

                                testerHeroJoy.gameControlToPlay = "Xbox";
                                testerHeroJoy.joystickOrderID = "First";
                                testerHeroJoy.controllerOrder = "joystick 1";

                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p1JoystickKeys[i];
                                }
                            }
                            else if (indexOfItem == 2)
                            {
                                testerHeroKey.transform.parent.gameObject.SetActive(false);
                                testerHeroJoy.transform.parent.gameObject.SetActive(true);

                                testerHeroJoy.gameControlToPlay = "Playstation";
                                testerHeroJoy.joystickOrderID = "First";
                                testerHeroJoy.controllerOrder = "joystick 1";

                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p1JoystickKeys[i];
                                }
                            }
                            else if (indexOfItem == 3)
                            {
                                testerHeroKey.transform.parent.gameObject.SetActive(false);
                                testerHeroJoy.transform.parent.gameObject.SetActive(true);

                                testerHeroJoy.gameControlToPlay = "Generic";
                                testerHeroJoy.joystickOrderID = "First";
                                testerHeroJoy.controllerOrder = "joystick 1";

                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p1JoystickKeys[i];
                                }
                            }
                        }
                        if (p2Turn)
                        {
                            testerHeroKey.transform.parent.gameObject.SetActive(false);
                            testerHeroJoy.transform.parent.gameObject.SetActive(true);

                            p2GunIndex = Array.IndexOf(joyIcons, result.gameObject.GetComponent<Image>());
                            testerHeroJoy.joystickOrderID = "Second";
                            testerHeroJoy.controllerOrder = "joystick 2";

                            if (indexOfItem == 1)
                            {
                                testerHeroJoy.gameControlToPlay = "Xbox";

                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p2keys[i];
                                }
                            }
                            else if (indexOfItem == 2)
                            {
                                testerHeroJoy.gameControlToPlay = "Playstation";
                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p2keys[i];
                                }
                            }
                            else if (indexOfItem == 3)
                            {
                                testerHeroJoy.gameControlToPlay = "Generic";
                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p2keys[i];
                                }
                            }
                        }
                        if (p3Turn)
                        {
                            testerHeroKey.transform.parent.gameObject.SetActive(false);
                            testerHeroJoy.transform.parent.gameObject.SetActive(true);

                            p3GunIndex = Array.IndexOf(joyIcons, result.gameObject.GetComponent<Image>());
                            testerHeroJoy.joystickOrderID = "Third";
                            testerHeroJoy.controllerOrder = "joystick 3";

                            if (indexOfItem == 1)
                            {
                                testerHeroJoy.gameControlToPlay = "Xbox";

                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p3keys[i];
                                }
                            }
                            else if (indexOfItem == 2)
                            {
                                testerHeroJoy.gameControlToPlay = "Playstation";
                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p3keys[i];
                                }
                            }
                            else if (indexOfItem == 3)
                            {
                                testerHeroJoy.gameControlToPlay = "Generic";
                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p3keys[i];
                                }
                            }
                        }
                        if (p4Turn)
                        {
                            testerHeroKey.transform.parent.gameObject.SetActive(false);
                            testerHeroJoy.transform.parent.gameObject.SetActive(true);

                            p4GunIndex = Array.IndexOf(joyIcons, result.gameObject.GetComponent<Image>());
                            testerHeroJoy.joystickOrderID = "Fourth";
                            testerHeroJoy.controllerOrder = "joystick 4";

                            if (indexOfItem == 1)
                            {
                                testerHeroJoy.gameControlToPlay = "Xbox";

                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p4keys[i];
                                }
                            }
                            else if (indexOfItem == 2)
                            {
                                testerHeroJoy.gameControlToPlay = "Playstation";
                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p4keys[i];
                                }
                            }
                            else if (indexOfItem == 3)
                            {
                                testerHeroJoy.gameControlToPlay = "Generic";
                                for (int i = 0; i < 15; i++)
                                {
                                    selectionBoxes[i].text = playersKeys[0].p4keys[i];
                                }
                            }
                        }

                        if (result.gameObject.transform.name == "Controller Icon Keyboard")
                        {
                            foreach (InputField InF in selectionBoxes)
                            {
                                //InF.characterLimit = 1;
                            }
                        }
                        else
                        {
                            foreach (InputField InF in selectionBoxes)
                            {
                                //InF.characterLimit = 0;
                            } 
                        }
                    }
                }                
            }               
        }

        if (chooseInput)
        {
            if (p1Turn)
            {
                if (Input.GetAxis("Axis 1") > 0f || Input.GetAxis("Axis 1") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 1";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 1";
                    p1InputNames[inputOrder] = "Joy 1 Axis 1";

                    foreach(HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 2") > 0f || Input.GetAxis("Axis 2") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 2";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 2";
                    p1InputNames[inputOrder] = "Joy 1 Axis 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 3") > 0f || Input.GetAxis("Axis 3") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 3";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 3";
                    p1InputNames[inputOrder] = "Joy 1 Axis 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 4") > 0f || Input.GetAxis("Axis 4") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 4";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 4";
                    p1InputNames[inputOrder] = "Joy 1 Axis 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 5") > 0f || Input.GetAxis("Axis 5") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 5";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 5";
                    p1InputNames[inputOrder] = "Joy 1 Axis 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 6") > 0f || Input.GetAxis("Axis 6") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 6";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 6";
                    p1InputNames[inputOrder] = "Joy 1 Axis 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 7") > 0f || Input.GetAxis("Axis 7") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 7";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 7";
                    p1InputNames[inputOrder] = "Joy 1 Axis 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 8") > 0f || Input.GetAxis("Axis 8") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 8";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 8";
                    p1InputNames[inputOrder] = "Joy 1 Axis 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 9") > 0f || Input.GetAxis("Axis 9") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 9";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 9";
                    p1InputNames[inputOrder] = "Joy 1 Axis 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 10") > 0f || Input.GetAxis("Axis 10") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 10";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 10";
                    p1InputNames[inputOrder] = "Joy 1 Axis 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 11") > 0f || Input.GetAxis("Axis 11") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 11";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 11";
                    p1InputNames[inputOrder] = "Joy 1 Axis 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 12") > 0f || Input.GetAxis("Axis 12") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 12";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 12";
                    p1InputNames[inputOrder] = "Joy 1 Axis 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 13") > 0f || Input.GetAxis("Axis 13") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 13";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 13";
                    p1InputNames[inputOrder] = "Joy 1 Axis 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 14") > 0f || Input.GetAxis("Axis 14") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 14";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 14";
                    p1InputNames[inputOrder] = "Joy 1 Axis 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 15") > 0f || Input.GetAxis("Axis 15") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 15";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 15";
                    p1InputNames[inputOrder] = "Joy 1 Axis 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 16") > 0f || Input.GetAxis("Axis 16") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 16";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 16";
                    p1InputNames[inputOrder] = "Joy 1 Axis 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 17") > 0f || Input.GetAxis("Axis 17") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 17";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 17";
                    p1InputNames[inputOrder] = "Joy 1 Axis 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 18") > 0f || Input.GetAxis("Axis 18") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 18";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 18";
                    p1InputNames[inputOrder] = "Joy 1 Axis 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 19") > 0f || Input.GetAxis("Axis 19") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 19";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 19";
                    p1InputNames[inputOrder] = "Joy 1 Axis 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 19";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 20") > 0f || Input.GetAxis("Axis 20") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 20";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 20";
                    p1InputNames[inputOrder] = "Joy 1 Axis 20";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 20";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 21") > 0f || Input.GetAxis("Axis 21") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 21";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 21";
                    p1InputNames[inputOrder] = "Joy 1 Axis 21";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 21";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 22") > 0f || Input.GetAxis("Axis 22") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 22";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 22";
                    p1InputNames[inputOrder] = "Joy 1 Axis 22";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 22";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 23") > 0f || Input.GetAxis("Axis 23") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 23";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 23";
                    p1InputNames[inputOrder] = "Joy 1 Axis 23";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 23";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 24") > 0f || Input.GetAxis("Axis 24") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 24";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 24";
                    p1InputNames[inputOrder] = "Joy 1 Axis 24";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 24";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 25") > 0f || Input.GetAxis("Axis 25") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 25";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 25";
                    p1InputNames[inputOrder] = "Joy 1 Axis 25";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 25";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 26") > 0f || Input.GetAxis("Axis 26") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 26";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 26";
                    p1InputNames[inputOrder] = "Joy 1 Axis 26";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 26";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 27") > 0f || Input.GetAxis("Axis 27") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 27";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 27";
                    p1InputNames[inputOrder] = "Joy 1 Axis 27";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 27";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("Axis 28") > 0f || Input.GetAxis("Axis 28") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Axis 28";
                    actualInput = "";
                    p1Commands[inputOrder] = "Axis 28";
                    p1InputNames[inputOrder] = "Joy 1 Axis 28";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "Axis 28";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button0))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 0";
                    actualInput = "";
                    p1Commands[inputOrder] = "0";
                    p1InputNames[inputOrder] = "Joy 1 Button 0";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "0";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button1))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 1";
                    actualInput = "";
                    p1Commands[inputOrder] = "1";
                    p1InputNames[inputOrder] = "Joy 1 Button 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button2))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 2";
                    actualInput = "";
                    p1Commands[inputOrder] = "2";
                    p1InputNames[inputOrder] = "Joy 1 Button 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button3))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 3";
                    actualInput = "";
                    p1Commands[inputOrder] = "3";
                    p1InputNames[inputOrder] = "Joy 1 Button 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button4))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 4";
                    actualInput = "";
                    p1Commands[inputOrder] = "4";
                    p1InputNames[inputOrder] = "Joy 1 Button 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button5))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 5";
                    actualInput = "";
                    p1Commands[inputOrder] = "5";
                    p1InputNames[inputOrder] = "Joy 1 Button 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button6))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 6";
                    actualInput = "";
                    p1Commands[inputOrder] = "6";
                    p1InputNames[inputOrder] = "Joy 1 Button 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button7))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 7";
                    actualInput = "";
                    p1Commands[inputOrder] = "7";
                    p1InputNames[inputOrder] = "Joy 1 Button 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button8))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 8";
                    actualInput = "";
                    p1Commands[inputOrder] = "8";
                    p1InputNames[inputOrder] = "Joy 1 Button 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button9))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 9";
                    actualInput = "";
                    p1Commands[inputOrder] = "9";
                    p1InputNames[inputOrder] = "Joy 1 Button 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button10))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 10";
                    actualInput = "";
                    p1Commands[inputOrder] = "10";
                    p1InputNames[inputOrder] = "Joy 1 Button 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button11))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 11";
                    actualInput = "";
                    p1Commands[inputOrder] = "11";
                    p1InputNames[inputOrder] = "Joy 1 Button 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button12))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 12";
                    actualInput = "";
                    p1Commands[inputOrder] = "12";
                    p1InputNames[inputOrder] = "Joy 1 Button 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button13))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 13";
                    actualInput = "";
                    p1Commands[inputOrder] = "13";
                    p1InputNames[inputOrder] = "Joy 1 Button 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button14))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 14";
                    actualInput = "";
                    p1Commands[inputOrder] = "14";
                    p1InputNames[inputOrder] = "Joy 1 Button 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button15))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 15";
                    actualInput = "";
                    p1Commands[inputOrder] = "15";
                    p1InputNames[inputOrder] = "Joy 1 Button 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button16))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 16";
                    actualInput = "";
                    p1Commands[inputOrder] = "16";
                    p1InputNames[inputOrder] = "Joy 1 Button 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button17))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 17";
                    actualInput = "";
                    p1Commands[inputOrder] = "17";
                    p1InputNames[inputOrder] = "Joy 1 Button 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button18))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 18";
                    actualInput = "";
                    p1Commands[inputOrder] = "18";
                    p1InputNames[inputOrder] = "Joy 1 Button 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick1Button19))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy1 Button 19";
                    actualInput = "";
                    p1Commands[inputOrder] = "19";
                    p1InputNames[inputOrder] = "Joy 1 Button 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1JoystickKeys[inputOrder] = "19";
                    }

                    chooseInput = false;
                }
                else if (Input.GetMouseButtonDown(0))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Mouse Button 0";
                    actualInput = "";
                    p1Commands[inputOrder] = "0";
                    p1InputNames[inputOrder] = "Mouse Button 0";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1KeyboardKeys[inputOrder] = "0";
                    }

                    chooseInput = false;
                }
                else if (Input.GetMouseButtonDown(1))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Mouse Button 1";
                    actualInput = "";
                    p1Commands[inputOrder] = "1";
                    p1InputNames[inputOrder] = "Mouse Button 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1KeyboardKeys[inputOrder] = "1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetMouseButtonDown(2))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Mouse Button 2";
                    actualInput = "";
                    p1Commands[inputOrder] = "2";
                    p1InputNames[inputOrder] = "Mouse Button 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1KeyboardKeys[inputOrder] = "2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetMouseButtonDown(3))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Mouse Button 3";
                    actualInput = "";
                    p1Commands[inputOrder] = "3";
                    p1InputNames[inputOrder] = "Mouse Button 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1KeyboardKeys[inputOrder] = "3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetMouseButtonDown(4))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Mouse Button 4";
                    actualInput = "";
                    p1Commands[inputOrder] = "4";
                    p1InputNames[inputOrder] = "Mouse Button 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p1KeyboardKeys[inputOrder] = "4";
                    }

                    chooseInput = false;
                }

                foreach (string key in keyboardKeys)
                {
                    if (Input.GetKeyDown(key))
                    {
                        selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = key;
                        actualInput = "";
                        p1Commands[inputOrder] = key;
                        p1InputNames[inputOrder] = key;

                        foreach (HeroData hd in playersKeys)
                        {
                            hd.p1KeyboardKeys[inputOrder] = key;
                        }

                        chooseInput = false;
                    }
                }
            }
            else if (p2Turn)
            {
                if (Input.GetAxis("2Axis 1") > 0f || Input.GetAxis("2Axis 1") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 X Axis";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 1";
                    p2InputNames[inputOrder] = "Joy 2 Axis 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 2") > 0f || Input.GetAxis("2Axis 2") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Y Axis";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 2";
                    p2InputNames[inputOrder] = "Joy 2 Axis 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 3") > 0f || Input.GetAxis("2Axis 3") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 3";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 3";
                    p2InputNames[inputOrder] = "Joy 2 Axis 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 4") > 0f || Input.GetAxis("2Axis 4") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 4";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 4";
                    p2InputNames[inputOrder] = "Joy 2 Axis 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 5") > 0f || Input.GetAxis("2Axis 5") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 5";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 5";
                    p2InputNames[inputOrder] = "Joy 2 Axis 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 6") > 0f || Input.GetAxis("2Axis 6") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 6";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 6";
                    p2InputNames[inputOrder] = "Joy 2 Axis 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 7") > 0f || Input.GetAxis("2Axis 7") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 7";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 7";
                    p2InputNames[inputOrder] = "Joy 2 Axis 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 8") > 0f || Input.GetAxis("2Axis 8") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 8";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 8";
                    p2InputNames[inputOrder] = "Joy 2 Axis 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 9") > 0f || Input.GetAxis("2Axis 9") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 9";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 9";
                    p2InputNames[inputOrder] = "Joy 2 Axis 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 10") > 0f || Input.GetAxis("2Axis 10") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 10";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 10";
                    p2InputNames[inputOrder] = "Joy 2 Axis 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 11") > 0f || Input.GetAxis("2Axis 11") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 11";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 11";
                    p2InputNames[inputOrder] = "Joy 2 Axis 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 12") > 0f || Input.GetAxis("2Axis 12") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 12";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 12";
                    p2InputNames[inputOrder] = "Joy 2 Axis 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 13") > 0f || Input.GetAxis("2Axis 13") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 13";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 13";
                    p2InputNames[inputOrder] = "Joy 2 Axis 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 14") > 0f || Input.GetAxis("2Axis 14") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 14";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 14";
                    p2InputNames[inputOrder] = "Joy 2 Axis 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 15") > 0f || Input.GetAxis("2Axis 15") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 15";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 15";
                    p2InputNames[inputOrder] = "Joy 2 Axis 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 16") > 0f || Input.GetAxis("2Axis 16") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 16";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 16";
                    p2InputNames[inputOrder] = "Joy 2 Axis 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 17") > 0f || Input.GetAxis("2Axis 17") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 17";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 17";
                    p2InputNames[inputOrder] = "Joy 2 Axis 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 18") > 0f || Input.GetAxis("2Axis 18") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 18";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 18";
                    p2InputNames[inputOrder] = "Joy 2 Axis 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 19") > 0f || Input.GetAxis("2Axis 19") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 19";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 19";
                    p2InputNames[inputOrder] = "Joy 2 Axis 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 19";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 20") > 0f || Input.GetAxis("2Axis 20") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 20";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 20";
                    p2InputNames[inputOrder] = "Joy 2 Axis 20";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 20";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 21") > 0f || Input.GetAxis("2Axis 21") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 21";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 21";
                    p2InputNames[inputOrder] = "Joy 2 Axis 21";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 21";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 22") > 0f || Input.GetAxis("2Axis 22") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 22";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 22";
                    p2InputNames[inputOrder] = "Joy 2 Axis 22";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 22";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 23") > 0f || Input.GetAxis("2Axis 23") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 23";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 23";
                    p2InputNames[inputOrder] = "Joy 2 Axis 23";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 23";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 24") > 0f || Input.GetAxis("2Axis 24") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 24";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 24";
                    p2InputNames[inputOrder] = "Joy 2 Axis 24";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 24";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 25") > 0f || Input.GetAxis("2Axis 25") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 25";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 25";
                    p2InputNames[inputOrder] = "Joy 2 Axis 25";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 25";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 26") > 0f || Input.GetAxis("2Axis 26") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 26";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 26";
                    p2InputNames[inputOrder] = "Joy 2 Axis 26";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 26";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 27") > 0f || Input.GetAxis("2Axis 27") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 27";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 27";
                    p2InputNames[inputOrder] = "Joy 2 Axis 27";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 27";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("2Axis 28") > 0f || Input.GetAxis("2Axis 28") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Axis 28";
                    actualInput = "";
                    p2Commands[inputOrder] = "2Axis 28";
                    p2InputNames[inputOrder] = "Joy 2 Axis 28";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2Axis 28";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button0))//****
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 0";
                    actualInput = "";
                    p2Commands[inputOrder] = "0";
                    p2InputNames[inputOrder] = "Joy 2 Button 0";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "0";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button1))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 1";
                    actualInput = "";
                    p2Commands[inputOrder] = "1";
                    p2InputNames[inputOrder] = "Joy 2 Button 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button2))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 2";
                    actualInput = "";
                    p2Commands[inputOrder] = "2";
                    p2InputNames[inputOrder] = "Joy 2 Button 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button3))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 3";
                    actualInput = "";
                    p2Commands[inputOrder] = "3";
                    p2InputNames[inputOrder] = "Joy 2 Button 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button4))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 4";
                    actualInput = "";
                    p2Commands[inputOrder] = "4";
                    p2InputNames[inputOrder] = "Joy 2 Button 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button5))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 5";
                    actualInput = "";
                    p2Commands[inputOrder] = "5";
                    p2InputNames[inputOrder] = "Joy 2 Button 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button6))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 6";
                    actualInput = "";
                    p2Commands[inputOrder] = "6";
                    p2InputNames[inputOrder] = "Joy 2 Button 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button7))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 7";
                    actualInput = "";
                    p2Commands[inputOrder] = "7";
                    p2InputNames[inputOrder] = "Joy 2 Button 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button8))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 8";
                    actualInput = "";
                    p2Commands[inputOrder] = "8";
                    p2InputNames[inputOrder] = "Joy 2 Button 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button9))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 9";
                    actualInput = "";
                    p2Commands[inputOrder] = "9";
                    p2InputNames[inputOrder] = "Joy 2 Button 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button10))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 10";
                    actualInput = "";
                    p2Commands[inputOrder] = "10";
                    p2InputNames[inputOrder] = "Joy 2 Button 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button11))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 11";
                    actualInput = "";
                    p2Commands[inputOrder] = "11";
                    p2InputNames[inputOrder] = "Joy 2 Button 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button12))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 12";
                    actualInput = "";
                    p2Commands[inputOrder] = "12";
                    p2InputNames[inputOrder] = "Joy 2 Button 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button13))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 13";
                    actualInput = "";
                    p2Commands[inputOrder] = "13";
                    p2InputNames[inputOrder] = "Joy 2 Button 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button14))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 14";
                    actualInput = "";
                    p2Commands[inputOrder] = "14";
                    p2InputNames[inputOrder] = "Joy 2 Button 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button15))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 15";
                    actualInput = "";
                    p2Commands[inputOrder] = "15";
                    p2InputNames[inputOrder] = "Joy 2 Button 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button16))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 16";
                    actualInput = "";
                    p2Commands[inputOrder] = "16";
                    p2InputNames[inputOrder] = "Joy 2 Button 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button17))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 17";
                    actualInput = "";
                    p2Commands[inputOrder] = "17";
                    p2InputNames[inputOrder] = "Joy 2 Button 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button18))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 18";
                    actualInput = "";
                    p2Commands[inputOrder] = "18";
                    p2InputNames[inputOrder] = "Joy 2 Button 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick2Button19))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy2 Button 19";
                    actualInput = "";
                    p2Commands[inputOrder] = "19";
                    p2InputNames[inputOrder] = "Joy 2 Button 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p2keys[inputOrder] = "19";
                    }

                    chooseInput = false;
                }
            }
            else if (p3Turn)
            {
                if (Input.GetAxis("3Axis 1") > 0f || Input.GetAxis("3Axis 1") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 X Axis";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 1";
                    p3InputNames[inputOrder] = "Joy 3 Axis 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 2") > 0f || Input.GetAxis("3Axis 2") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Y Axis";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 2";
                    p3InputNames[inputOrder] = "Joy 3 Axis 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 3") > 0f || Input.GetAxis("3Axis 3") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 3";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 3";
                    p3InputNames[inputOrder] = "Joy 3 Axis 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 4") > 0f || Input.GetAxis("3Axis 4") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 4";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 4";
                    p3InputNames[inputOrder] = "Joy 3 Axis 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 5") > 0f || Input.GetAxis("3Axis 5") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 5";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 5";
                    p3InputNames[inputOrder] = "Joy 3 Axis 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 6") > 0f || Input.GetAxis("3Axis 6") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 6";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 6";
                    p3InputNames[inputOrder] = "Joy 3 Axis 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 7") > 0f || Input.GetAxis("3Axis 7") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 7";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 7";
                    p3InputNames[inputOrder] = "Joy 3 Axis 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 8") > 0f || Input.GetAxis("3Axis 8") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 8";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 8";
                    p3InputNames[inputOrder] = "Joy 3 Axis 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 9") > 0f || Input.GetAxis("3Axis 9") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 9";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 9";
                    p3InputNames[inputOrder] = "Joy 3 Axis 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 10") > 0f || Input.GetAxis("3Axis 10") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 10";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 10";
                    p3InputNames[inputOrder] = "Joy 3 Axis 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 11") > 0f || Input.GetAxis("3Axis 11") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 11";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 11";
                    p3InputNames[inputOrder] = "Joy 3 Axis 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 12") > 0f || Input.GetAxis("3Axis 12") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 12";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 12";
                    p3InputNames[inputOrder] = "Joy 3 Axis 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 13") > 0f || Input.GetAxis("3Axis 13") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 13";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 13";
                    p3InputNames[inputOrder] = "Joy 3 Axis 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 14") > 0f || Input.GetAxis("3Axis 14") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 14";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 14";
                    p3InputNames[inputOrder] = "Joy 3 Axis 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 15") > 0f || Input.GetAxis("3Axis 15") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 15";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 15";
                    p3InputNames[inputOrder] = "Joy 3 Axis 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 16") > 0f || Input.GetAxis("3Axis 16") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 16";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 16";
                    p3InputNames[inputOrder] = "Joy 3 Axis 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 17") > 0f || Input.GetAxis("3Axis 17") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 17";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 17";
                    p3InputNames[inputOrder] = "Joy 3 Axis 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 18") > 0f || Input.GetAxis("3Axis 18") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 18";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 18";
                    p3InputNames[inputOrder] = "Joy 3 Axis 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 19") > 0f || Input.GetAxis("3Axis 19") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 19";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 19";
                    p3InputNames[inputOrder] = "Joy 3 Axis 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 19";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 20") > 0f || Input.GetAxis("3Axis 20") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 20";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 20";
                    p3InputNames[inputOrder] = "Joy 3 Axis 20";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 20";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 21") > 0f || Input.GetAxis("3Axis 21") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 21";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 21";
                    p3InputNames[inputOrder] = "Joy 3 Axis 21";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 21";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 22") > 0f || Input.GetAxis("3Axis 22") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 22";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 22";
                    p3InputNames[inputOrder] = "Joy 3 Axis 22";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 22";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 23") > 0f || Input.GetAxis("3Axis 23") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 23";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 23";
                    p3InputNames[inputOrder] = "Joy 3 Axis 23";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 23";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 24") > 0f || Input.GetAxis("3Axis 24") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 24";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 24";
                    p3InputNames[inputOrder] = "Joy 3 Axis 24";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 24";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 25") > 0f || Input.GetAxis("3Axis 25") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 25";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 25";
                    p3InputNames[inputOrder] = "Joy 3 Axis 25";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 25";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 26") > 0f || Input.GetAxis("3Axis 26") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 26";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 26";
                    p3InputNames[inputOrder] = "Joy 3 Axis 26";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 26";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 27") > 0f || Input.GetAxis("3Axis 27") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 27";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 27";
                    p3InputNames[inputOrder] = "Joy 3 Axis 27";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 27";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("3Axis 28") > 0f || Input.GetAxis("3Axis 28") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Axis 28";
                    actualInput = "";
                    p3Commands[inputOrder] = "3Axis 28";
                    p3InputNames[inputOrder] = "Joy 3 Axis 28";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3Axis 28";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button0))//****
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 0";
                    actualInput = "";
                    p3Commands[inputOrder] = "0";
                    p3InputNames[inputOrder] = "Joy 3 Button 0";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "0";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button1))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 1";
                    actualInput = "";
                    p3Commands[inputOrder] = "1";
                    p3InputNames[inputOrder] = "Joy 3 Button 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button2))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 2";
                    actualInput = "";
                    p3Commands[inputOrder] = "2";
                    p3InputNames[inputOrder] = "Joy 3 Button 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button3))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 3";
                    actualInput = "";
                    p2Commands[inputOrder] = "3";
                    p2InputNames[inputOrder] = "Joy 3 Button 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button4))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 4";
                    actualInput = "";
                    p3Commands[inputOrder] = "4";
                    p3InputNames[inputOrder] = "Joy 3 Button 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button5))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 5";
                    actualInput = "";
                    p3Commands[inputOrder] = "5";
                    p3InputNames[inputOrder] = "Joy 3 Button 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button6))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 6";
                    actualInput = "";
                    p3Commands[inputOrder] = "6";
                    p3InputNames[inputOrder] = "Joy 3 Button 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button7))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 7";
                    actualInput = "";
                    p3Commands[inputOrder] = "7";
                    p3InputNames[inputOrder] = "Joy 3 Button 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button8))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 8";
                    actualInput = "";
                    p3Commands[inputOrder] = "8";
                    p3InputNames[inputOrder] = "Joy 3 Button 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button9))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 9";
                    actualInput = "";
                    p3Commands[inputOrder] = "9";
                    p3InputNames[inputOrder] = "Joy 3 Button 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button10))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 10";
                    actualInput = "";
                    p3Commands[inputOrder] = "10";
                    p3InputNames[inputOrder] = "Joy 3 Button 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button11))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 11";
                    actualInput = "";
                    p3Commands[inputOrder] = "11";
                    p3InputNames[inputOrder] = "Joy 3 Button 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button12))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 12";
                    actualInput = "";
                    p3Commands[inputOrder] = "12";
                    p3InputNames[inputOrder] = "Joy 3 Button 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button13))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 13";
                    actualInput = "";
                    p3Commands[inputOrder] = "13";
                    p3InputNames[inputOrder] = "Joy 3 Button 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button14))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 14";
                    actualInput = "";
                    p3Commands[inputOrder] = "14";
                    p3InputNames[inputOrder] = "Joy 3 Button 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button15))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 15";
                    actualInput = "";
                    p3Commands[inputOrder] = "15";
                    p3InputNames[inputOrder] = "Joy 3 Button 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button16))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 16";
                    actualInput = "";
                    p3Commands[inputOrder] = "16";
                    p3InputNames[inputOrder] = "Joy 3 Button 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button17))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 17";
                    actualInput = "";
                    p3Commands[inputOrder] = "17";
                    p3InputNames[inputOrder] = "Joy 3 Button 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button18))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 18";
                    actualInput = "";
                    p3Commands[inputOrder] = "18";
                    p3InputNames[inputOrder] = "Joy 3 Button 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick3Button19))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy3 Button 19";
                    actualInput = "";
                    p3Commands[inputOrder] = "19";
                    p3InputNames[inputOrder] = "Joy 3 Button 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p3keys[inputOrder] = "19";
                    }

                    chooseInput = false;
                }
            }
            else if (p4Turn)
            {
                if (Input.GetAxis("4Axis 1") > 0f || Input.GetAxis("4Axis 1") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 X Axis";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 1";
                    p4InputNames[inputOrder] = "Joy 4 Axis 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 2") > 0f || Input.GetAxis("4Axis 2") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Y Axis";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 2";
                    p4InputNames[inputOrder] = "Joy 4 Axis 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 3") > 0f || Input.GetAxis("4Axis 3") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 3";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 3";
                    p4InputNames[inputOrder] = "Joy 4 Axis 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 4") > 0f || Input.GetAxis("4Axis 4") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 4";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 4";
                    p4InputNames[inputOrder] = "Joy 4 Axis 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 5") > 0f || Input.GetAxis("4Axis 5") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 5";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 5";
                    p4InputNames[inputOrder] = "Joy 4 Axis 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 6") > 0f || Input.GetAxis("4Axis 6") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 6";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 6";
                    p4InputNames[inputOrder] = "Joy 4 Axis 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 7") > 0f || Input.GetAxis("4Axis 7") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 7";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 7";
                    p4InputNames[inputOrder] = "Joy 4 Axis 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 8") > 0f || Input.GetAxis("4Axis 8") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 8";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 8";
                    p4InputNames[inputOrder] = "Joy 4 Axis 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 9") > 0f || Input.GetAxis("4Axis 9") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 9";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 9";
                    p4InputNames[inputOrder] = "Joy 4 Axis 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 10") > 0f || Input.GetAxis("4Axis 10") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 10";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 10";
                    p4InputNames[inputOrder] = "Joy 4 Axis 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 11") > 0f || Input.GetAxis("4Axis 11") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 11";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 11";
                    p4InputNames[inputOrder] = "Joy 4 Axis 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 12") > 0f || Input.GetAxis("4Axis 12") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 12";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 12";
                    p4InputNames[inputOrder] = "Joy 4 Axis 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 13") > 0f || Input.GetAxis("4Axis 13") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 13";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 13";
                    p4InputNames[inputOrder] = "Joy 4 Axis 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 14") > 0f || Input.GetAxis("4Axis 14") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 14";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 14";
                    p4InputNames[inputOrder] = "Joy 4 Axis 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 15") > 0f || Input.GetAxis("4Axis 15") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 15";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 15";
                    p4InputNames[inputOrder] = "Joy 4 Axis 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 16") > 0f || Input.GetAxis("4Axis 16") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 16";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 16";
                    p4InputNames[inputOrder] = "Joy 4 Axis 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 17") > 0f || Input.GetAxis("4Axis 17") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 17";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 17";
                    p4InputNames[inputOrder] = "Joy 4 Axis 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 18") > 0f || Input.GetAxis("4Axis 18") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 18";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 18";
                    p4InputNames[inputOrder] = "Joy 4 Axis 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 19") > 0f || Input.GetAxis("4Axis 19") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 19";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 19";
                    p4InputNames[inputOrder] = "Joy 4 Axis 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 19";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 20") > 0f || Input.GetAxis("4Axis 20") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 20";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 20";
                    p4InputNames[inputOrder] = "Joy 4 Axis 20";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 20";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 21") > 0f || Input.GetAxis("4Axis 21") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 21";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 21";
                    p4InputNames[inputOrder] = "Joy 4 Axis 21";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 21";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 22") > 0f || Input.GetAxis("4Axis 22") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 22";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 22";
                    p4InputNames[inputOrder] = "Joy 4 Axis 22";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 22";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 23") > 0f || Input.GetAxis("4Axis 23") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 23";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 23";
                    p4InputNames[inputOrder] = "Joy 4 Axis 23";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 23";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 24") > 0f || Input.GetAxis("4Axis 24") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 24";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 24";
                    p4InputNames[inputOrder] = "Joy 4 Axis 24";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 24";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 25") > 0f || Input.GetAxis("4Axis 25") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 25";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 25";
                    p4InputNames[inputOrder] = "Joy 4 Axis 25";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 25";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 26") > 0f || Input.GetAxis("4Axis 26") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 26";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 26";
                    p4InputNames[inputOrder] = "Joy 4 Axis 26";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 26";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 27") > 0f || Input.GetAxis("4Axis 27") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 27";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 27";
                    p4InputNames[inputOrder] = "Joy 4 Axis 27";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 27";
                    }

                    chooseInput = false;
                }
                else if (Input.GetAxis("4Axis 28") > 0f || Input.GetAxis("4Axis 28") < 0f)
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Axis 28";
                    actualInput = "";
                    p4Commands[inputOrder] = "4Axis 28";
                    p4InputNames[inputOrder] = "Joy 4 Axis 28";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4Axis 28";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button0))//****
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 0";
                    actualInput = "";
                    p4Commands[inputOrder] = "0";
                    p4InputNames[inputOrder] = "Joy 4 Button 0";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "0";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button1))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 1";
                    actualInput = "";
                    p4Commands[inputOrder] = "1";
                    p4InputNames[inputOrder] = "Joy 4 Button 1";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "1";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button2))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 2";
                    actualInput = "";
                    p4Commands[inputOrder] = "2";
                    p4InputNames[inputOrder] = "Joy 4 Button 2";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "2";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button3))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 3";
                    actualInput = "";
                    p2Commands[inputOrder] = "3";
                    p2InputNames[inputOrder] = "Joy 4 Button 3";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "3";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button4))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 4";
                    actualInput = "";
                    p4Commands[inputOrder] = "4";
                    p4InputNames[inputOrder] = "Joy 4 Button 4";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "4";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button5))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 5";
                    actualInput = "";
                    p4Commands[inputOrder] = "5";
                    p4InputNames[inputOrder] = "Joy 4 Button 5";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "5";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button6))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 6";
                    actualInput = "";
                    p4Commands[inputOrder] = "6";
                    p4InputNames[inputOrder] = "Joy 4 Button 6";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "6";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button7))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 7";
                    actualInput = "";
                    p4Commands[inputOrder] = "7";
                    p4InputNames[inputOrder] = "Joy 4 Button 7";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "7";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button8))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 8";
                    actualInput = "";
                    p4Commands[inputOrder] = "8";
                    p4InputNames[inputOrder] = "Joy 4 Button 8";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "8";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button9))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 9";
                    actualInput = "";
                    p4Commands[inputOrder] = "9";
                    p4InputNames[inputOrder] = "Joy 4 Button 9";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "9";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button10))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 10";
                    actualInput = "";
                    p4Commands[inputOrder] = "10";
                    p4InputNames[inputOrder] = "Joy 4 Button 10";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "10";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button11))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 11";
                    actualInput = "";
                    p4Commands[inputOrder] = "11";
                    p4InputNames[inputOrder] = "Joy 4 Button 11";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "11";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button12))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 12";
                    actualInput = "";
                    p4Commands[inputOrder] = "12";
                    p4InputNames[inputOrder] = "Joy 4 Button 12";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "12";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button13))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 13";
                    actualInput = "";
                    p4Commands[inputOrder] = "13";
                    p4InputNames[inputOrder] = "Joy 4 Button 13";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "13";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button14))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 14";
                    actualInput = "";
                    p4Commands[inputOrder] = "14";
                    p4InputNames[inputOrder] = "Joy 4 Button 14";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "14";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button15))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 15";
                    actualInput = "";
                    p4Commands[inputOrder] = "15";
                    p4InputNames[inputOrder] = "Joy 4 Button 15";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "15";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button16))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 16";
                    actualInput = "";
                    p4Commands[inputOrder] = "16";
                    p4InputNames[inputOrder] = "Joy 4 Button 16";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "16";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button17))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 17";
                    actualInput = "";
                    p4Commands[inputOrder] = "17";
                    p4InputNames[inputOrder] = "Joy 4 Button 17";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "17";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button18))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 18";
                    actualInput = "";
                    p4Commands[inputOrder] = "18";
                    p4InputNames[inputOrder] = "Joy 4 Button 18";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "18";
                    }

                    chooseInput = false;
                }
                else if (Input.GetKey(KeyCode.Joystick4Button19))
                {
                    selectionBoxes[Array.IndexOf(selectionBoxes, GameObject.Find(actualInput).GetComponent<InputField>())].text = "Joy4 Button 19";
                    actualInput = "";
                    p4Commands[inputOrder] = "19";
                    p4InputNames[inputOrder] = "Joy 4 Button 19";

                    foreach (HeroData hd in playersKeys)
                    {
                        hd.p4keys[inputOrder] = "19";
                    }

                    chooseInput = false;
                }
            }
        }
    }

    public void InputFieldName(string InpFieldName)
    {
        chooseInput = true;
        actualInput = InpFieldName;
        selectionBoxes[Array.IndexOf(selectionBoxes,GameObject.Find(InpFieldName).GetComponent<InputField>())].text = "";
    }

    public void OrderInput(int inputIndex)
    {
        inputOrder = inputIndex;
    }

    public void InvertHA()
    {
        if (p1Turn)
        {
           foreach(HeroData hd in playersKeys)
            {
                hd.horizontalAxisInvertedP1 = !hd.horizontalAxisInvertedP1;
            }
        }
        else if (p2Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.horizontalAxisInvertedP2 = !hd.horizontalAxisInvertedP2;
            }
        }
        else if (p3Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.horizontalAxisInvertedP3 = !hd.horizontalAxisInvertedP3;
            }
        }
        else if (p4Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.horizontalAxisInvertedP4 = !hd.horizontalAxisInvertedP4;
            }
        }
    }

    public void InvertVA()
    {
        if (p1Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAxisInvertedP1 = !hd.verticalAxisInvertedP1;
            }
        }
        else if (p2Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAxisInvertedP2 = !hd.verticalAxisInvertedP2;
            }
        }
        else if (p3Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAxisInvertedP3 = !hd.verticalAxisInvertedP3;
            }
        }
        else if (p4Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAxisInvertedP4 = !hd.verticalAxisInvertedP4;
            }
        }
    }

    public void InvertHAA()
    {
        if (p1Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.horizontalAimAxisInvertedP1 = !hd.horizontalAimAxisInvertedP1;
            }
        }
        else if (p2Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.horizontalAimAxisInvertedP2 = !hd.horizontalAimAxisInvertedP2;
            }
        }
        else if (p3Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.horizontalAimAxisInvertedP3 = !hd.horizontalAimAxisInvertedP3;
            }
        }
        else if (p4Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.horizontalAimAxisInvertedP4 = !hd.horizontalAimAxisInvertedP4;
            }
        }
    }

    public void InvertVAA()
    {
        if (p1Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAimAxisInvertedP1 = !hd.verticalAimAxisInvertedP1;
            }
        }
        else if (p2Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAimAxisInvertedP2 = !hd.verticalAimAxisInvertedP2;
            }
        }
        else if (p3Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAimAxisInvertedP3 = !hd.verticalAimAxisInvertedP3;
            }
        }
        else if (p4Turn)
        {
            foreach (HeroData hd in playersKeys)
            {
                hd.verticalAimAxisInvertedP4 = !hd.verticalAimAxisInvertedP4;
            }
        }
    }
}
