﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIconUI : MonoBehaviour
{
    //Aqui se inclui os sprites da UI do rosto
    public Sprite[] icons;
    public Text faceText;//Texto da face da UI
    public GameObject bgFace;
    public GameObject imageIconFace;
    public GameObject imageIconHead;
    Image imageHeadComponent;//Da cabeça do Player
    Image imageFaceComponent;//Da face do Player
    Image imageFaceBG;// Fundo do BG da Face colorido
    public string actualStatus;

    void Start()
    {
        imageHeadComponent = imageIconHead.GetComponent<Image>();
        imageFaceComponent = imageIconFace.GetComponent<Image>();
        imageFaceBG = bgFace.GetComponent<Image>();
    }

    void Update()
    {
        switch (actualStatus)
        {
            case "Poisoned":
                imageHeadComponent.sprite = icons[0];// Cabeça
                imageFaceComponent.sprite = icons[0];//Face
                imageHeadComponent.color = new Color32(164, 0, 119, 255); //rosa
                imageFaceBG.color = new Color32(164, 0, 119, 255); //rosa BG
                faceText.text = "POISONED";
                break;
            case "Grabbed":
                imageFaceComponent.sprite = icons[1];
                imageFaceBG.color = new Color32(190, 146, 0, 255); //laranja claro
                faceText.text = "GRABBED";
                break;
            case "Recovering":
                imageFaceComponent.sprite = icons[2];
                imageFaceBG.color = new Color32(0, 164, 13, 255); //verde escuro
                faceText.text = "LIFE UP";
                break;
            case "Infected":
                imageHeadComponent.sprite = icons[3];// Cabeça
                imageFaceComponent.sprite = icons[3];
                imageHeadComponent.color = new Color32(164, 58, 0, 255); //Laranja Marrom
                imageFaceBG.color = new Color32(164, 58, 0, 255); //Laranja Marrom
                faceText.text = "INFECTED";
                break;
            case "Damage Up":
                imageFaceComponent.sprite = icons[4];
                imageFaceBG.color = new Color32(164, 0, 0, 255); //vermelho
                faceText.text = "DAMAGE UP";
                break;
            case "Defense Up":
                imageFaceComponent.sprite = icons[5];
                imageFaceBG.color = new Color32(21, 0, 164, 255); //azul escuro
                faceText.text = "DEFENSE UP";
                break;
            case "Speed Up":
                imageFaceComponent.sprite = icons[6];
                imageFaceBG.color = new Color32(113, 191, 0, 255); //azul escuro
                faceText.text = "SPEED UP";
                break;
        }
    }
}
