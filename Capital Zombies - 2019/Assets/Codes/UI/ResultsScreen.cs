﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class ResultsScreen : MonoBehaviour
{
    [Header("ALL STARTS BUTTONS:")]
    private string startP1Keyboard;
    private string startP1Controller;
    private string startP2;
    private string startP3;
    private string startP4;

    [Header("PLAYERS FINAL RESULTS UI:")]
    public GameObject[] resultsBoard2To4;//Os boards de resultados aparecem do 2 ao 4 lugar já que o primeiro sempre será mostrado
    public GameObject[] charFacesLabel;//Labels dos personagens que jogaram
    public GameObject[] P1WinnerChar;
    public GameObject[] P2WinnerChar;
    public GameObject[] P3WinnerChar;
    public GameObject[] P4WinnerChar;

    [Header("ITEM E DIRECTOR SCRIPT:")]
    public GlobalGameSelections itemScript;
    public GameDirector diretorScript;
    public GameObject player1TableRank; //Usando o player 1 pra passar as informaçoes do rank 
    public RankSorter rankScript;

    [Header("LOAD CANVAS SCREEN:")]
    public GameObject loadScreen;

    [Header("RESULTS UI:")]
    public Text[] firstLabelTexts;
    public Text[] playersName;
    public Text[] killsText;
    public Text[] deathsText;
    public Text[] totalText;
    public GameObject[] pontuationsBG;
    public GameObject[] allLabels;    
    public Sprite[] facesLabelImg;

    [Header("THANK YOU BG:")]
    public GameObject TYBG;

    int playersCount = 0;
    int confirmedPlayers = 0;

    bool p1Confirm = false;
    bool p2Confirm = false;
    bool p3Confirm = false;
    bool p4Confirm = false;

    string[] heroNameSelected;

    void Start()
    {
        MissionScript missionCanvs = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
        missionCanvs.missionFailed = true;
        missionCanvs.gameObject.SetActive(false);

        if (!itemScript.multiplayerSelected)
        {
           foreach(GameObject obj in resultsBoard2To4)
           {
               obj.SetActive(false);
           }

           foreach (GameObject obj in charFacesLabel)
           {
               obj.SetActive(false);
           }

            firstLabelTexts[0].text = "Kills";
            firstLabelTexts[1].text = "Missions";
            firstLabelTexts[2].text = "Time";
            firstLabelTexts[3].text = "Classification";
            firstLabelTexts[4].text = "Killer\n**";//NOOB,HERO,KILLER,GAMER
            pontuationsBG[0].SetActive(true);
            P1WinnerChar[Array.FindIndex(P1WinnerChar, s => s.name == "Shadow " + itemScript.heroName[0])].SetActive(true);
            TYBG.SetActive(true);
        }
        else
        {
            for(int i = 1; i < itemScript.controllerType.Length; i++)
            {
                if (itemScript.controllerType[i] != "")
                {
                    charFacesLabel[i-1].SetActive(true);
                    playersCount = i + 1;
                }
            }

            player1TableRank = GameObject.Find("Rank InGame");

            if (player1TableRank != null)
            {
                rankScript = player1TableRank.GetComponent<RankSorter>();                
            }

            //--------------
            if (rankScript.pos1Name == "Player 1")
            {
                P1WinnerChar[Array.FindIndex(P1WinnerChar, s => s.name == "Shadow " + itemScript.heroName[0])].SetActive(true);
            }
            else if (rankScript.pos1Name == "Player 2")
            {
                P1WinnerChar[Array.FindIndex(P1WinnerChar, s => s.name == "Shadow " + itemScript.heroName[1])].SetActive(true);
            }
            else if (rankScript.pos1Name == "Player 3")
            {
                P1WinnerChar[Array.FindIndex(P1WinnerChar, s => s.name == "Shadow " + itemScript.heroName[2])].SetActive(true);
            }
            else if (rankScript.pos1Name == "Player 4")
            {
                P1WinnerChar[Array.FindIndex(P1WinnerChar, s => s.name == "Shadow " + itemScript.heroName[3])].SetActive(true);
            }
            //--------------
            if (rankScript.pos2Name == "Player 1")
            {
                P2WinnerChar[Array.FindIndex(P2WinnerChar, s => s.name == itemScript.heroName[0])].SetActive(true);
            }
            else if (rankScript.pos2Name == "Player 2")
            {
                P2WinnerChar[Array.FindIndex(P2WinnerChar, s => s.name == itemScript.heroName[1])].SetActive(true);
            }
            else if (rankScript.pos2Name == "Player 3")
            {
                P2WinnerChar[Array.FindIndex(P2WinnerChar, s => s.name == itemScript.heroName[2])].SetActive(true);
            }
            else if (rankScript.pos2Name == "Player 4")
            {
                P2WinnerChar[Array.FindIndex(P2WinnerChar, s => s.name == itemScript.heroName[3])].SetActive(true);
            }
            //--------------
            if(playersCount >= 3)
            {
                if (rankScript.pos3Name == "Player 1")
                {
                    P3WinnerChar[Array.FindIndex(P3WinnerChar, s => s.name == itemScript.heroName[0])].SetActive(true);
                }
                else if (rankScript.pos3Name == "Player 2")
                {
                    P3WinnerChar[Array.FindIndex(P3WinnerChar, s => s.name == itemScript.heroName[1])].SetActive(true);
                }
                else if (rankScript.pos3Name == "Player 3")
                {
                    P3WinnerChar[Array.FindIndex(P3WinnerChar, s => s.name == itemScript.heroName[2])].SetActive(true);
                    print("disgraça 3");
                }
                else if (rankScript.pos3Name == "Player 4")
                {
                    P3WinnerChar[Array.FindIndex(P3WinnerChar, s => s.name == itemScript.heroName[3])].SetActive(true);
                }
            }

            if(playersCount >= 4)
            {
                //--------------
                if (rankScript.pos4Name == "Player 1")
                {
                    P4WinnerChar[Array.FindIndex(P4WinnerChar, s => s.name == itemScript.heroName[0])].SetActive(true);
                }
                else if (rankScript.pos4Name == "Player 2")
                {
                    P4WinnerChar[Array.FindIndex(P4WinnerChar, s => s.name == itemScript.heroName[1])].SetActive(true);
                }
                else if (rankScript.pos4Name == "Player 3")
                {
                    P4WinnerChar[Array.FindIndex(P4WinnerChar, s => s.name == itemScript.heroName[2])].SetActive(true);
                }
                else if (rankScript.pos4Name == "Player 4")
                {
                    P4WinnerChar[Array.FindIndex(P4WinnerChar, s => s.name == itemScript.heroName[3])].SetActive(true);
                    print("disgraça 4");
                }
            }            
        }

        for(int i = 0; i < itemScript.heroName.Length; i++)
        {
            if (itemScript.heroName[i] != "")
            {
                confirmedPlayers++;
            }
        }
    }

    private void Update()
    {
        if (itemScript.multiplayerSelected)
        {
            if (player1TableRank != null)
            {
                playersName[0].text = rankScript.pos1Name;
                playersName[1].text = rankScript.pos2Name;
                playersName[2].text = rankScript.pos3Name;
                playersName[3].text = rankScript.pos4Name;
            }

            //----------- Controles 1 Confirm ------------------------
            if (itemScript.controllerType[0] == "Wireless")
            {
                if (Input.GetKeyDown("joystick 1 button 0") && !p1Confirm)
                {
                    if (playersName[0].text == "Player 1")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[0].ToString();
                        deathsText[0].text = diretorScript.deathsP[0].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 1")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[0].ToString();
                        deathsText[1].text = diretorScript.deathsP[0].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 1")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[0].ToString();
                        deathsText[2].text = diretorScript.deathsP[0].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 1")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[0].ToString();
                        deathsText[3].text = diretorScript.deathsP[0].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p1Confirm = true;
                    confirmedPlayers--;
                }
            }
            else if (itemScript.controllerType[0] == "Xbox" || itemScript.controllerType[0] == "xbox" || itemScript.controllerType[0] == "XBOX")
            {
                if (Input.GetKeyDown("joystick 1 button 0") && !p1Confirm)
                {
                    if (playersName[0].text == "Player 1")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[0].ToString();
                        deathsText[0].text = diretorScript.deathsP[0].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 1")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[0].ToString();
                        deathsText[1].text = diretorScript.deathsP[0].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 1")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[0].ToString();
                        deathsText[2].text = diretorScript.deathsP[0].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 1")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[0].ToString();
                        deathsText[3].text = diretorScript.deathsP[0].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p1Confirm = true;
                    confirmedPlayers--;
                }
            }
            else
            {
                if (Input.GetKeyDown("joystick 1 button 0") && !p1Confirm)
                {
                    if (playersName[0].text == "Player 1")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[0].ToString();
                        deathsText[0].text = diretorScript.deathsP[0].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 1")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[0].ToString();
                        deathsText[1].text = diretorScript.deathsP[0].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 1")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[0].ToString();
                        deathsText[2].text = diretorScript.deathsP[0].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 1")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[0].ToString();
                        deathsText[3].text = diretorScript.deathsP[0].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p1Confirm = true;
                    confirmedPlayers--;
                }
            }

            //----------- Controles 2 Confirm ------------------------
            if (itemScript.controllerType[1] == "Wireless")
            {
                if (Input.GetKeyDown("joystick 2 button 0") && !p2Confirm)
                {
                    if (playersName[0].text == "Player 2")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[1].ToString();
                        deathsText[0].text = diretorScript.deathsP[1].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 2")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[1].ToString();
                        deathsText[1].text = diretorScript.deathsP[1].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 2")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[1].ToString();
                        deathsText[2].text = diretorScript.deathsP[1].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 2")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[1].ToString();
                        deathsText[3].text = diretorScript.deathsP[1].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p2Confirm = true;
                    confirmedPlayers--;
                }
            }
            else if (itemScript.controllerType[1] == "Xbox" || itemScript.controllerType[1] == "xbox" || itemScript.controllerType[1] == "XBOX")
            {
                if (Input.GetKeyDown("joystick 2 button 0") && !p2Confirm)
                {
                    if (playersName[0].text == "Player 2")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[1].ToString();
                        deathsText[0].text = diretorScript.deathsP[1].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 2")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[1].ToString();
                        deathsText[1].text = diretorScript.deathsP[1].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 2")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[1].ToString();
                        deathsText[2].text = diretorScript.deathsP[1].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 2")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[1].ToString();
                        deathsText[3].text = diretorScript.deathsP[1].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p2Confirm = true;
                    confirmedPlayers--;
                }
            }
            else
            {
                if (Input.GetKeyDown("joystick 2 button 0") && !p2Confirm)
                {
                    if (playersName[0].text == "Player 2")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[1].ToString();
                        deathsText[0].text = diretorScript.deathsP[1].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 2")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[1].ToString();
                        deathsText[1].text = diretorScript.deathsP[1].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 2")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[1].ToString();
                        deathsText[2].text = diretorScript.deathsP[1].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 2")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[1].ToString();
                        deathsText[3].text = diretorScript.deathsP[1].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p2Confirm = true;
                    confirmedPlayers--;
                }
            }

            //----------- Controles 3 Confirm ------------------------
            if (itemScript.controllerType[2] == "Wireless")
            {
                if (Input.GetKeyDown("joystick 3 button 0") && !p3Confirm)
                {
                    if (playersName[0].text == "Player 3")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[2].ToString();
                        deathsText[0].text = diretorScript.deathsP[2].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 3")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[2].ToString();
                        deathsText[1].text = diretorScript.deathsP[2].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 3")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[2].ToString();
                        deathsText[2].text = diretorScript.deathsP[2].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 3")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[2].ToString();
                        deathsText[3].text = diretorScript.deathsP[2].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p3Confirm = true;
                    confirmedPlayers--;
                }
            }
            else if (itemScript.controllerType[2] == "Xbox" || itemScript.controllerType[2] == "xbox" || itemScript.controllerType[2] == "XBOX")
            {
                if (Input.GetKeyDown("joystick 3 button 0") && !p3Confirm)
                {
                    if (playersName[0].text == "Player 3")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[2].ToString();
                        deathsText[0].text = diretorScript.deathsP[2].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 3")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[2].ToString();
                        deathsText[1].text = diretorScript.deathsP[2].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 3")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[2].ToString();
                        deathsText[2].text = diretorScript.deathsP[2].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 3")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[2].ToString();
                        deathsText[3].text = diretorScript.deathsP[2].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p3Confirm = true;
                    confirmedPlayers--;
                }
            }
            else
            {
                if (Input.GetKeyDown("joystick 3 button 0") && !p3Confirm)
                {
                    if (playersName[0].text == "Player 3")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[2].ToString();
                        deathsText[0].text = diretorScript.deathsP[2].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 3")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[2].ToString();
                        deathsText[1].text = diretorScript.deathsP[2].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 3")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[2].ToString();
                        deathsText[2].text = diretorScript.deathsP[2].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 3")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[2].ToString();
                        deathsText[3].text = diretorScript.deathsP[2].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p3Confirm = true;
                    confirmedPlayers--;
                }
            }

            //----------- Controles 4 Confirm ------------------------
            if (itemScript.controllerType[3] == "Wireless")
            {
                if (Input.GetKeyDown("joystick 4 button 0") && !p4Confirm)
                {
                    if (playersName[0].text == "Player 4")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[3].ToString();
                        deathsText[0].text = diretorScript.deathsP[3].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 4")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[3].ToString();
                        deathsText[1].text = diretorScript.deathsP[3].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 4")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[3].ToString();
                        deathsText[2].text = diretorScript.deathsP[3].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 4")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[3].ToString();
                        deathsText[3].text = diretorScript.deathsP[3].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p4Confirm = true;
                    confirmedPlayers--;
                }
            }
            else if (itemScript.controllerType[3] == "Xbox" || itemScript.controllerType[3] == "xbox" || itemScript.controllerType[3] == "XBOX")
            {
                if (Input.GetKeyDown("joystick 3 button 0") && !p4Confirm)
                {
                    if (playersName[0].text == "Player 4")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[3].ToString();
                        deathsText[0].text = diretorScript.deathsP[3].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 4")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[3].ToString();
                        deathsText[1].text = diretorScript.deathsP[3].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 4")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[3].ToString();
                        deathsText[2].text = diretorScript.deathsP[3].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 4")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[3].ToString();
                        deathsText[3].text = diretorScript.deathsP[3].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p4Confirm = true;
                    confirmedPlayers--;
                }
            }
            else
            {
                if (Input.GetKeyDown("joystick 4 button 0") && !p4Confirm)
                {
                    if (playersName[0].text == "Player 4")
                    {
                        pontuationsBG[0].SetActive(true);
                        killsText[0].text = diretorScript.killsP[3].ToString();
                        deathsText[0].text = diretorScript.deathsP[3].ToString();
                        totalText[0].text = diretorScript.rank[0].ToString();
                    }
                    else if (playersName[1].text == "Player 4")
                    {
                        pontuationsBG[1].SetActive(true);
                        killsText[1].text = diretorScript.killsP[3].ToString();
                        deathsText[1].text = diretorScript.deathsP[3].ToString();
                        totalText[1].text = diretorScript.rank[1].ToString();
                    }
                    else if (playersName[2].text == "Player 4")
                    {
                        pontuationsBG[2].SetActive(true);
                        killsText[2].text = diretorScript.killsP[3].ToString();
                        deathsText[2].text = diretorScript.deathsP[3].ToString();
                        totalText[2].text = diretorScript.rank[2].ToString();
                    }
                    else if (playersName[3].text == "Player 4")
                    {
                        pontuationsBG[3].SetActive(true);
                        killsText[3].text = diretorScript.killsP[3].ToString();
                        deathsText[3].text = diretorScript.deathsP[3].ToString();
                        totalText[3].text = diretorScript.rank[3].ToString();
                    }

                    p4Confirm = true;
                    confirmedPlayers--;
                }
            }

            if (confirmedPlayers <= 0)
            {
                loadScreen.SetActive(true);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown("joystick 1 button 0"))
            {
                loadScreen.SetActive(true);
            }
        }
    }
}
