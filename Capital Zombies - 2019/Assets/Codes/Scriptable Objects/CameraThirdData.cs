﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Camera Name", menuName = "Camera Third Person")]
public class CameraThirdData : ScriptableObject
{
    [Header("CAMERA SPEED:")]
    public float cameraMouseXSpeed = 8f;
    public float cameraMouseYSpeed = 8f;
}
