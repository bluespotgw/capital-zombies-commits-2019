﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Camera Name", menuName = "Camera")]
public class CameraData : ScriptableObject
{
    [Header("CAMERA SPEED:")]
    public float cameraMouseSpeed = 8f;
    public float cameraJoystickSpeed = 4f;
    public float lerpTime = 0.1f;
    [Header("CLAMP LIMITS:")]
    public Vector2 clampMouseLimit;
    public Vector2 clampJoystickLimit;
    public Vector2 rotationClampJoystick;
}
