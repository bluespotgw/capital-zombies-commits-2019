﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Selecao de Itens", menuName = "Selecao")]
public class GlobalGameSelections : ScriptableObject
{
    [Header("PLAYERS SELECTIONS:")]
    public string[] heroName;
    public string[] controllerOrder;
    public string[] controllerType;
    public int[] rifleGun;
    public int[] pistolGun;
    public int[] meleeGun;
    public int[] grenadeSelected;

    [Header("MATCH SELECTIONS:")]
    public int totalLife = 0;
    public int totalTime = 0;
    public string modeSelection = "";
    public bool multiplayerSelected = false;

    [Header("RANDOM E TEAM SWITCHS:")]
    public string randomStage = "";
    public string teamAttack = "";

    public bool canSpawn = false;
}
