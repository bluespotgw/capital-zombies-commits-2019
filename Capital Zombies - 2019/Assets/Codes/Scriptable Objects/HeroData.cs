﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Nome do personagem",menuName = "Personagem")]
public class HeroData : ScriptableObject
{
    [Header("HERO LIFE:")]
    public float totalLife = 100;
    [Header("LEFT AXIS - JOYSTICK:")]
    public string leftJoystickHorizontalAxis = "LeftHorizontalJoystick";
    public string leftJoystickVerticalAxis = "LeftVerticalJoystick";
    public string leftJoystickHorizontalAxisP2 = "LeftHorizontalJoystickP2";
    public string leftJoystickVerticalAxisP2 = "LeftVerticalJoystickP2";
    public string leftJoystickHorizontalAxisP3 = "LeftHorizontalJoystickP3";
    public string leftJoystickVerticalAxisP3 = "LeftVerticalJoystickP3";
    public string leftJoystickHorizontalAxisP4 = "LeftHorizontalJoystickP4";
    public string leftJoystickVerticalAxisP4 = "LeftVerticalJoystickP4";
    [Header("RIGHT AXIS - JOYSTICK:")]
    public string rightJoystickHorizontalAxis = "RightHorizontalJoystick";
    public string rightJoystickVerticalAxis = "RightVerticalJoystick";
    public string rightJoystickHorizontalAxisP2 = "RightHorizontalJoystickP2";
    public string rightJoystickVerticalAxisP2 = "RightVerticalJoystickP2";
    public string rightJoystickHorizontalAxisP3 = "RightHorizontalJoystickP3";
    public string rightJoystickVerticalAxisP3 = "RightVerticalJoystickP3";
    public string rightJoystickHorizontalAxisP4 = "RightHorizontalJoystickP4";
    public string rightJoystickVerticalAxisP4 = "RightVerticalJoystickP4";
    [Header("KEYBOARD AXIS:")]
    public string keyboardHorizontalAxis = "HorizontalKeyboard";
    public string keyboardVerticalAxis = "VerticalKeyboard";
    [Header("MOUSE AXIS:")]
    public string mouseHorizontalAxis = "Mouse X";
    public string mouseVerticalAxis = "Mouse Y";
    [Header("P1 BUTTONS:")]
    public string[] p1KeyboardKeys;
    public string[] p1JoystickKeys;
    [Header("P2 BUTTONS:")]
    public string[] p2keys;
    [Header("P3 BUTTONS:")]
    public string[] p3keys;
    [Header("P4 BUTTONS:")]
    public string[] p4keys;
    [Header("HABILITIES VALUE:")]
    public float jumpStrenght = 500f;
    public float forwardJumpForce = 2f;
    public float rotationSpeed = 5.0f;
    [Header("GRENADE FORCE VALUES:")]
    public float weakGrenadeLimit = 3f;
    public float strongGrenadeLimit = 6f;
    [Header("GUNS RIFLE VALUES:")]
    public int ak47Ammo = 30;
    public int m4Ammo = 30;
    public int stonerAmmo = 30;
    public int g28Ammo = 25;
    public int awpAmmo = 20;
    public int scoutAmmo = 20;
    public int shotgunAmmo = 15;
    public int mp5Ammo = 30;
    public int mp40Ammo = 30;
    public int bizonAmmo = 30;
    [Header("GUNS PISTOL VALUES:")]
    public int glockAmmo = 25;
    public int coltAmmo = 25;
    public int p38Ammo = 25;
    public int coltAnaAmmo = 25;
    public int socomAmmo = 25;
    [Header("RIFLE DAMAGE:")]
    public float akDamage = 25;
    public float m4Damage = 25;
    public float stonerDamage = 25;
    public float g28Damage = 25;
    public float awpDamage = 25;
    public float scoutDamage = 25;
    public float shotgunDamage = 25;
    public float mp5Damage = 25;
    public float mp40Damage = 25;
    public float bizonDamage = 25;
    [Header("PISTOL DAMAGE:")]
    public float glockDamage = 25;
    public float coltDamage = 25;
    public float p38Damage = 25;
    public float coltAnaDamage = 25;
    public float socomDamage = 25;
    [Header("GRENADEs VALUES:")]
    public int grenadeQuant = 3;
    public float poisonedTime = 5f;
    public float poisonDamage = 1f;
    public float heDamage = 50f;
    [Header("RECOVER LIFE VALUES:")]
    public float recoverTime = 3f;
    public float recoverValue = 1f;
    [Header("INVERTED AXIS:")]
    public bool horizontalAxisInvertedP1 = false;
    public bool horizontalAxisInvertedP2 = false;
    public bool horizontalAxisInvertedP3 = false;
    public bool horizontalAxisInvertedP4 = false;

    public bool verticalAxisInvertedP1 = false;
    public bool verticalAxisInvertedP2 = false;
    public bool verticalAxisInvertedP3 = false;
    public bool verticalAxisInvertedP4 = false;

    public bool horizontalAimAxisInvertedP1 = false;
    public bool horizontalAimAxisInvertedP2 = false;
    public bool horizontalAimAxisInvertedP3 = false;
    public bool horizontalAimAxisInvertedP4 = false;

    public bool verticalAimAxisInvertedP1 = false;
    public bool verticalAimAxisInvertedP2 = false;
    public bool verticalAimAxisInvertedP3 = false;
    public bool verticalAimAxisInvertedP4 = false;
}

