﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]

public class IkPass : MonoBehaviour
{
    [Header("BONES ROTATION VALUES:")]
    public float weightSpineIK;
    public float weightSpineWithPistolIK;
    public float weightSpineWithGreandeThirdModeIK;

    [Header("RIFLES IK GAMEOBJECTS:")]
    public Transform ikLeftHand;
    public Transform ikRightHand;

    [Header("PISTOL IK GAMEOBJECTS:")]
    public Transform ikLeftHandPistol;
    public Transform ikRightHandPistol;

    [Header("GRENADE IK GAMEOBJECTS:")]
    public Transform ikLeftHandGrenade;
    public Transform ikRightHandGrenade;

    [Header("CHAINSAW IK GAMEOBJECTS:")]
    public Transform ikLeftHandChainsaw;
    public Transform ikRightHandChainsaw;

    [Header("SHIELD IK GAMEOBJECTS:")]
    public Transform ikLeftHandShield;
    public Transform ikRightHandShield;

    [Header("PUBLIC SCRIPTS:")]
    public HeroBehavior heroBehavior;

    [Header("PUBLIC VARIABLES:")]
    public bool handActive = true;
    public bool rifleInHands = true;
    public bool pistolInHands = false;
    public bool meleeInHands = false;
    public bool grenadeInHands = false;

    float axisAngleY;
    public GameObject characterToRotate;
    bool lockState = false;

    private float countWeight;
    protected Animator anim;
    
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(!heroBehavior.joystickActivated && !anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Crouch_Walk_Tree"))//Só funciona normal assim por que o joystick tem peso 0 na rotação
        {
            if (anim.GetCurrentAnimatorStateInfo(1).normalizedTime < 1 && anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Grenade_Toss"))
            {
                if (countWeight < weightSpineIK)
                {
                    countWeight += 1f * Time.deltaTime;
                }
            }

            if (anim.GetCurrentAnimatorStateInfo(1).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Grenade_Toss"))
            {
                if (countWeight > 0)
                {
                    countWeight -= 2.5f * Time.deltaTime;                   
                }
                else
                {
                    countWeight = 0;                  
                }
            }
        }       
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (anim.GetCurrentAnimatorStateInfo(1).normalizedTime < 1 && anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Grenade_Toss") ||
            anim.GetCurrentAnimatorStateInfo(1).normalizedTime < 1 && anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Reloading_Gun"))
        {
            anim.SetBoneLocalRotation(HumanBodyBones.Spine, new Quaternion(0, countWeight, 0, 1f));
            handActive = false; //Pra não entrar em conflito com a hand ik e o lançamento da granada
        }
        else
        {
            handActive = true;
        }

        /*if (heroBehavior.pistols[heroBehavior.pistolSelected])//anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Holding_Pistol")
        {
            anim.SetBoneLocalRotation(HumanBodyBones.Spine, new Quaternion(weightSpineWithPistolIK, 0, 0, 1f));
            print("maer");
        }*/

        if (heroBehavior.changeCam && heroBehavior.GunSelected == 3 && !heroBehavior.meleeGuns[4].activeInHierarchy)
        {
            if (heroBehavior.keyboardActivated)
            {
                axisAngleY += Input.GetAxis("Mouse Y") * 0.025f;
            }
            else if (heroBehavior.joystickActivated)
            {
                axisAngleY += heroBehavior.verticalRightJoy * 0.05f;
            }
            
            if(axisAngleY >= 0.85)
            {
                axisAngleY = 0.85f;
            }

            if (axisAngleY <= -0.2)
            {
                axisAngleY = -0.2f;
            }


            anim.SetBoneLocalRotation(HumanBodyBones.Spine, new Quaternion(axisAngleY, 0, 0, 1f));
        }

        if (heroBehavior.changeCam && heroBehavior.GunSelected == 4)
        {
            anim.SetBoneLocalRotation(HumanBodyBones.Spine, new Quaternion(0, weightSpineWithGreandeThirdModeIK, 0, 1f));
        }

        if (handActive && !heroBehavior.isGrabbing && heroBehavior.releasedWall) //&& !heroBehavior.changeCam)
        {
            if (rifleInHands)
            {
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKPosition(AvatarIKGoal.RightHand, ikRightHand.position);
                anim.SetIKRotation(AvatarIKGoal.RightHand, ikRightHand.rotation);

                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKPosition(AvatarIKGoal.LeftHand, ikLeftHand.position);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, ikLeftHand.rotation);
            }

            if (pistolInHands)
            {
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKPosition(AvatarIKGoal.RightHand, ikRightHandPistol.position);
                anim.SetIKRotation(AvatarIKGoal.RightHand, ikRightHandPistol.rotation);

                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKPosition(AvatarIKGoal.LeftHand, ikLeftHandPistol.position);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, ikLeftHandPistol.rotation);
            }

            if (grenadeInHands)
            {
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKPosition(AvatarIKGoal.RightHand, ikRightHandGrenade.position);
                anim.SetIKRotation(AvatarIKGoal.RightHand, ikRightHandGrenade.rotation);

                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKPosition(AvatarIKGoal.LeftHand, ikLeftHandGrenade.position);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, ikLeftHandGrenade.rotation);
            }

            if (heroBehavior.meleeGuns[3].activeInHierarchy)//Se a motoserra estiver selecionada na cena o ik dela é ativo
            {
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKPosition(AvatarIKGoal.RightHand, ikRightHandChainsaw.position);
                anim.SetIKRotation(AvatarIKGoal.RightHand, ikRightHandChainsaw.rotation);

                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKPosition(AvatarIKGoal.LeftHand, ikLeftHandChainsaw.position);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, ikLeftHandChainsaw.rotation);
            }

            if (heroBehavior.meleeGuns[4].activeInHierarchy)//Se o escudo estiver selecionada na cena o ik dela é ativo
            {
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                anim.SetIKPosition(AvatarIKGoal.RightHand, ikRightHandShield.position);
                anim.SetIKRotation(AvatarIKGoal.RightHand, ikRightHandShield.rotation);

                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                anim.SetIKPosition(AvatarIKGoal.LeftHand, ikLeftHandShield.position);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, ikLeftHandShield.rotation);
            }
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
            anim.SetLookAtWeight(0);
        }
    }
}
