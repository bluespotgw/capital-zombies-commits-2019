﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBox : MonoBehaviour
{
    GameObject hero;
    public GameObject brainParticles;
    public GameObject bloodParticles;
    public DG.Tweening.DOTweenAnimation bloodScreen;
    public HeroBehavior heroScript;
    public string playerTag = "";
    public string gunType = "";
    public int gunID = 0;
    public bool isHead = false;

    private void Start()
    {
        hero = heroScript.gameObject;
        heroScript = hero.GetComponent<HeroBehavior>();
    }

    public void TakeDamage(float value)
    {
        heroScript.life -= value;
        heroScript.oponentTag = playerTag;
        heroScript.enemyGunType = gunType;
        heroScript.enemyGunID = gunID;
        bloodScreen.DOPlayById("1");

        if (isHead)
        {
            heroScript.hitOnHead = true;
            Instantiate(brainParticles, transform.position, transform.rotation);
        }
        else
        {
            heroScript.hitOnHead = false;
            Instantiate(bloodParticles, transform.position, transform.rotation);
        }
    }
}
