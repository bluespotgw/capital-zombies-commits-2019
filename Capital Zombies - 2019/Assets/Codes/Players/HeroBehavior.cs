﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class HeroBehavior : MonoBehaviour
{
    [Header("SCRIPTABLE OBJECT:")]
    public HeroData heroData;

    [Header("ROOT GAMEOBJECT:")]
    public GameObject rootHeroObject;

    [Header("GRENADE CIRCLE OBJECT:")]
    public GameObject circle;

    [Header("IK SCRIPT:")]
    public IkPass ikCode;

    [Header("CONTROLLERS:")]
    /*public Control gameControlToPlay;
    public JoystickOrderID joystickOrderID;*/
    public string gameControlToPlay;
    public string joystickOrderID;

    [Header("CONTROLLER SELECTED:")]
    public bool joystickActivated;//Variavel do controle ativo
    public bool keyboardActivated;//Variavel do teclado ativo

    [Header("RAYCAST OBJECT:")]
    public GameObject rayHeadObject;
    public GameObject rayRHandObject;
    public GameObject rayLHandObject;
    public GameObject rayFootsObject;
    public LayerMask rayMaskForHands;//O raycast das mãos colide apenas com quem pertence a essa layer
    public LayerMask raycastDamage; //Raycast para identificar hitbox dos inimigos

   [Header("CONTROLLER ACTION KEYS:")]
    public string controllerOrder;
    public string walkButton;
    public string shootButton;
    public string reloadButton;
    public string crouchButton;
    public string rollButton;
    public string jumpAndReleaseGrabButton;
    public string gunSelectionButton;
    public string grenadeJoyCircleButton;
    public string grenadeReleaseButton;
    public string cameraSelectedButton;
    public string aimCameraButton;
    public string showRankButton;
    public string pauseButton;

    [Header("UTILITIES VALUES:")]
    public int GunSelected = 1;
    public GameObject grenade;
    public GameObject grenadeSpawner;   
    private float recoverAnimTime = 1f;
    private bool joystickCircleControl = false;

    [Header("PUBLIC VARIABLES TO ACESS IN OTHERS SCRIPTS:")]
    public bool rotateCamera;//Serve para checar se pode rotacionar a camera em volta do player
    public bool isGrabbing;//Verifica se o personagem está agarrando algum muro no cenário
    public string utilitieForce = "Weak";

    [Header("GUN ANIMATOR:")]
    Animator gunAnim;
    Animator gunAnimPistol;
    Animator gunSniperAnim;
    public GameObject gunAnimGameobject;
    public GameObject gunAnimGameobjectPistols;
    public GameObject gunSniperGameobjectAnim;

    //Variaveis de munição
    [Header("GUNS POOL:")]
    public GameObject[] rifles;
    public GameObject[] pistols;
    public GameObject[] meleeGuns;
    public GameObject[] grenades;
    public int rifleSelected = 0;
    public int pistolSelected = 0;
    public int meleeGunSelected = 0;
    public int grenadeSelected = 0;

    [Header("PARTICLES:")]
    public GameObject dustSpawnRun;
    public GameObject footRunParticle;
    public GameObject lifeUpParticle;
    public GameObject ammoUpParticle;
    public GameObject rifleParticle;
    public GameObject pistolParticle;
    public GameObject rayGunsThirdMode;
    public GameObject[] decals;
    float time = 0.1f;
    float timeAmmo = 2f;
    public bool showAmmoParticle = false;
    public bool spawn = false;
    bool canSpawnDustFootRun = false;

    [Header("VARIABLE TO PREVENT SMEAR IN ANIMATION WHILE MELEE IS ACTIIVE ON WALL:")]
    public bool releasedWall = true;

    [Header("CAMERAS:")]
    public GameObject cameraTop;
    public GameObject cameraThird;
    public Camera cameraFirst;// Componente FOV para aumentar zoom da camera
    public GameObject aimFirstPersonTexture;
    public GameObject lookPoint;//Para onde a camera olha em terceira pessoa
    public bool changeCam = false;
    int scopeState = 0; // Controla o zoom da scope

    [Header("IK HAND CONTROLLERS")]
    bool lockState = false;
    float axisX = 0;
    float axisY = 0;
    float axisYToCamPos = 0;
    float axisYToGrenadePos = 0;
    public GameObject rifleIkObject;
    public GameObject pistolIkObject;
    public GameObject grenadeIkObject;

    [Header("TOTAL GUNS AMMO:")]
    public float akAmmo;
    public float m4Ammo;
    public float stonerAmmo;
    public float g28Ammo;
    public float awpAmmo;
    public float scoutAmmo;
    public float shotgunAmmo;
    public float mp5Ammo;
    public float mp40Ammo;
    public float bizonAmmo;
    public float glockAmmo;
    public float coltAmmo;
    public float p38Ammo;
    public float coltAnaAmmo;
    public float socomAmmo;
    public int grenadeLimit;
    public bool isShooting = false;
    bool isRunning = false;
    public RectTransform ammoIcon;   
    bool lastAmmoRifles = false;
    bool lastAmmoPistols = false;
    public int ammoIconValueForRifle = 385;
    public int ammoIconValueForPistol= 385;

    [Header("LIFE BAR:")]
    public float life = 0;   
    public bool isRecovering = false;
    float recoverLifeTime = 0;

    [Header("RAGDOLL MODEL:")]
    public GameObject ragModel;
    bool isInstancing = false; // Para spawnar o modelo apenas uma vez

    [Header("TOP OF WALL CHECKER TO ZOMBIES KNOW:")]
    public bool playerIsOnTop = false;

    [Header("UI ELEMENTS:")] // Elementos de interface do jogo
    public Text bulletCounter;
    public Sprite[] rifleIcons;
    public Sprite[] pistolIcons;
    public Sprite[] meleeIcons;
    public Sprite[] grenadeIcons;
    public Image rifleUI;
    public Image pistolUI;
    public Image meleeUI;
    public Image grenadeUI;
    public Image lifeIcon;
    public Text lifeCounter;
    public Image headIconsUI;//Icones de Head
    public GameObject headIconUIReloading; //Icone isolado do reloading
    public GameObject getFaceAnimatorUI;//Tambem é usado para pegar o script da Face UI
    public GameObject mainCanvas;//Canvas da interface geral
    public DG.Tweening.DOTweenAnimation bloodScreen;//Info de sangue da tela
    Canvas canvs;//Canvas da interface geral
    public Animator faceUIAnimation;
    public FadeIconUI faceUIScript;
    public bool flipFaceSmoke;// Interface da cara do personagem virando e mostrando o status

    [Header("GAME DIRECTOR SCRIPT:")]
    public GameDirector directorScript;

    [Header("KILL CANVAS:")]
    public Canvas canvsKill;
    public string oponentTag = "";
    public bool hitOnHead = false;
    public string enemyGunType = "";
    public int enemyGunID = 0;

    [Header("RANK CANVAS:")]
    public GameObject rank;

    [Header("ITEM SCRIPT:")]
    public GlobalGameSelections itemScript;

    [Header("MISSION ITENS:")]
    public bool isInMission = false;
    MissionScript missionScript;
    //Como esse lixo é tosco tem que ter um time pra destruir se a missao de pistola for ativa
    bool dieAfterCheckPistolMission = false;
    float pisTime = 0.3f;
    bool breathPoison = false;//Evita que a missao da granada de mais de um ponto por vez pro oponente
    public float damageMultiplier = 1;//Para a missao de dano aumentado

    [Header("WAVE MISSION:")]
    public GameObject wave;
    public GameObject speedTrail;
    public Material particleWaveColor;
    public Light particleLight;
    
    [Header("IS ZOMBIE:")]
    public bool isZombie = false;
    public bool animToInfection = false;//Pra nao loopar infinito o label de transformação 
    bool changeSkinColor = false;
    public GameObject headMaskZombie;
    public int howManyZombiesContacts = 0;
    public InfectionAnimation infectionAnimScript;

    [Header("ELEMENTS TO HIDE FOR CAMERA:")]//Serve pra quando a mira for ativa os elementos especificos serem desativados
    public GameObject[] toHideObjs;

    [Header("JUMP PARTICLES:")]
    public GameObject jumpParticles;

    [Header("RAGDOLL FOR SINGLE PLAYER DEAD:")]//No gamedirector iria dar mais trabalho
    public GameObject ragdoll;

    //Toxic Grenade Valores
    private float timerPoisoned;

    //Variáveis para controlar o tempo random dos ataques das armas melee
    private float timerMelee = 1;
    private int meleeAttackChoosed = 0;

    private float runWalkFloat;//Controle de animação walk e run
    private float dampTime = .25f;//Valor de interpolação das animações
    private bool crouchActivated;//Controle do Crouch  
    public bool jump;//Controle do Pulo.É publico por causa da missão Jump/Kill
    private bool controlRigJump;//Controla quando pode ser mover durante o pulo pelo Raycast
    private bool activeHeadRay;//Ativa o raycast durante o pulo
    private bool activeHandRay;//Controla os raycast das mãos do personagem
    private bool isInTop;//Compara se o personagem está no topo de um objeto
    private bool gunActiveControl;//Controla o tempo que a arma fica ativa até ser recarregada
    private bool isAwping = false; //Variavel que faz a animação de mira não loopar
    private bool isHostaged = false;//Serve para identificar quando o inimigo agarrar o player
    private bool underZombieAttack = false;//Controla se algum zumbi entrou em colisão

    //Controle do grab dos zumbis
    GameObject actualZombie;
    int incomingZombieAttack = 0;
    bool checkZombieGrabFunction = false;
    bool isNotHostage = false;
    bool canRegrabedByZombies = false;
    float releaseTime = 0;
    float invulnerableGrab = 3f;
    float timerRegrab = 0;
    float recheckGrab = 3;//Pra ter certeza que o zombie grab foi desativado

    //Eixos de controle
    float horizontalLeftKey = 0;
    float verticalLeftKey = 0;
    float horizontalLeftJoy = 0;
    public float verticalLeftJoy = 0; // Esse ta publico por causa dos menus 
    public float horizontalRightJoy = 0;//Publico para a camera
    public float verticalRightJoy = 0; // Esse ta publico por causa da coluna

    public Animator anim;
    private Rigidbody rig;
    private CapsuleCollider capsuleCol;    

    public bool isInSelectionZone = false;
    bool grenadeCircleTrigger = false;

    GameObject pauseCanvas;
    GameObject gameoverCanvas;

    public bool testerChar = false;

    private void Start()
    {
        anim = GetComponent<Animator>();
        gunAnim = gunAnimGameobject.GetComponent<Animator>();
        gunAnimPistol = gunAnimGameobjectPistols.GetComponent<Animator>();
        gunSniperAnim = gunSniperGameobjectAnim.GetComponent<Animator>();
        rig = GetComponent<Rigidbody>();
        axisYToCamPos = cameraThird.transform.localPosition.y;
        capsuleCol = GetComponent<CapsuleCollider>();
        canvs = mainCanvas.GetComponent<Canvas>();
        pauseCanvas = GameObject.Find("Pause Canvas");

        if (!itemScript.multiplayerSelected)
        {
            gameoverCanvas = GameObject.Find("GameOver Canvas");
        }

        /*if (!isInSelectionZone && !directorScript.notMoreTied && !testerChar)
        {
            missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
        }*/

        if (!isInSelectionZone && !testerChar)
        {
            missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
        }

        if (meleeGunSelected != 3 && meleeGunSelected != 4)
        {
            meleeAttackChoosed = Random.Range(1, 3);
        }

        switch (joystickOrderID)
        {
            case "First":
                controllerOrder = "joystick 1";
                break;
            case "Second":
                controllerOrder = "joystick 2";
                break;
            case "Third":
                controllerOrder = "joystick 3";
                break;
            case "Fourth":
                controllerOrder = "joystick 4";
                break;
            default:
                controllerOrder = "none";
                break;
        }

        circle.transform.localScale = Vector3.zero;
        akAmmo = heroData.ak47Ammo;
        m4Ammo = heroData.m4Ammo;
        stonerAmmo = heroData.stonerAmmo;
        g28Ammo = heroData.g28Ammo;
        awpAmmo = heroData.awpAmmo;
        scoutAmmo = heroData.scoutAmmo;
        shotgunAmmo = heroData.shotgunAmmo;
        mp5Ammo = heroData.mp5Ammo;
        mp40Ammo = heroData.mp40Ammo;
        bizonAmmo = heroData.bizonAmmo;
        glockAmmo = heroData.glockAmmo;
        coltAmmo = heroData.coltAmmo;
        p38Ammo = heroData.p38Ammo;
        coltAnaAmmo = heroData.coltAnaAmmo;
        socomAmmo = heroData.socomAmmo;
        grenadeLimit = heroData.grenadeQuant;
        rifles[rifleSelected].gameObject.SetActive(true);
        life = heroData.totalLife;

        rifleUI.sprite = rifleIcons[rifleSelected];
        pistolUI.sprite = pistolIcons[pistolSelected];
        meleeUI.sprite = meleeIcons[meleeGunSelected];
        grenadeUI.sprite = grenadeIcons[grenadeSelected];

        faceUIScript = getFaceAnimatorUI.GetComponent<FadeIconUI>();
        faceUIAnimation = getFaceAnimatorUI.GetComponent<Animator>();
        timerPoisoned = heroData.poisonedTime;

        recoverLifeTime = heroData.recoverTime;        
    }

    private void Update()
    {
        if (!isInSelectionZone)
        {
            switch (gameControlToPlay)
            {
                case "Keyboard":
                    joystickActivated = false;
                    keyboardActivated = true;
                    joystickOrderID = "None";
                    GameControl(heroData.keyboardHorizontalAxis, heroData.keyboardVerticalAxis,
                                heroData.mouseHorizontalAxis, heroData.mouseVerticalAxis, heroData.forwardJumpForce, heroData.rotationSpeed);

                    walkButton = heroData.p1KeyboardKeys[5];
                    shootButton = heroData.p1KeyboardKeys[4];
                    reloadButton = heroData.p1KeyboardKeys[9];
                    crouchButton = heroData.p1KeyboardKeys[7];
                    rollButton = heroData.p1KeyboardKeys[8];
                    jumpAndReleaseGrabButton = heroData.p1KeyboardKeys[6];
                    gunSelectionButton = heroData.p1KeyboardKeys[10];
                    cameraSelectedButton = heroData.p1KeyboardKeys[12];
                    aimCameraButton = heroData.p1KeyboardKeys[13];
                    showRankButton = heroData.p1KeyboardKeys[14];
                    pauseButton = heroData.p1KeyboardKeys[15];
                    break;
                default:
                    joystickActivated = true;
                    keyboardActivated = false;

                    if (joystickOrderID == "First")
                    {
                        GameControl(heroData.p1JoystickKeys[0], heroData.p1JoystickKeys[1],
                                heroData.p1JoystickKeys[2], heroData.p1JoystickKeys[3],
                                heroData.forwardJumpForce, heroData.rotationSpeed);

                        walkButton = heroData.p1JoystickKeys[5];
                        shootButton = heroData.p1JoystickKeys[4];
                        reloadButton = heroData.p1JoystickKeys[9];
                        crouchButton = heroData.p1JoystickKeys[7];
                        rollButton = heroData.p1JoystickKeys[8];
                        jumpAndReleaseGrabButton = heroData.p1JoystickKeys[6];
                        gunSelectionButton = heroData.p1JoystickKeys[10];
                        grenadeJoyCircleButton = heroData.p1JoystickKeys[11];
                        grenadeReleaseButton = heroData.p1JoystickKeys[4];
                        cameraSelectedButton = heroData.p1JoystickKeys[12];
                        aimCameraButton = heroData.p1JoystickKeys[13];
                        showRankButton = heroData.p1JoystickKeys[14];
                        pauseButton = heroData.p1JoystickKeys[15];
                    }
                    else if (joystickOrderID == "Second")
                    {
                        GameControl(heroData.p2keys[0], heroData.p2keys[1],
                                heroData.p2keys[2], heroData.p2keys[3],
                                heroData.forwardJumpForce, heroData.rotationSpeed);

                        walkButton = heroData.p2keys[5];
                        shootButton = heroData.p2keys[4];
                        reloadButton = heroData.p2keys[9];
                        crouchButton = heroData.p2keys[7];
                        rollButton = heroData.p2keys[8];
                        jumpAndReleaseGrabButton = heroData.p2keys[6];
                        gunSelectionButton = heroData.p2keys[10];
                        grenadeJoyCircleButton = heroData.p2keys[11];
                        grenadeReleaseButton = heroData.p2keys[4];
                        cameraSelectedButton = heroData.p2keys[12];
                        aimCameraButton = heroData.p2keys[13];
                        showRankButton = heroData.p2keys[14];
                        pauseButton = heroData.p2keys[15];
                    }
                    else if (joystickOrderID == "Third")
                    {
                        GameControl(heroData.p3keys[0], heroData.p3keys[1],
                                 heroData.p3keys[2], heroData.p3keys[3],
                                 heroData.forwardJumpForce, heroData.rotationSpeed);

                        walkButton = heroData.p3keys[5];
                        shootButton = heroData.p3keys[4];
                        reloadButton = heroData.p3keys[9];
                        crouchButton = heroData.p3keys[7];
                        rollButton = heroData.p3keys[8];
                        jumpAndReleaseGrabButton = heroData.p3keys[6];
                        gunSelectionButton = heroData.p3keys[10];
                        grenadeJoyCircleButton = heroData.p3keys[11];
                        grenadeReleaseButton = heroData.p3keys[4];
                        cameraSelectedButton = heroData.p3keys[12];
                        aimCameraButton = heroData.p3keys[13];
                        showRankButton = heroData.p3keys[14];
                        pauseButton = heroData.p3keys[15];
                    }
                    else if (joystickOrderID == "Fourth")
                    {
                        GameControl(heroData.p4keys[0], heroData.p4keys[1],
                                 heroData.p4keys[2], heroData.p4keys[3],
                                 heroData.forwardJumpForce, heroData.rotationSpeed);

                        walkButton = heroData.p4keys[5];
                        shootButton = heroData.p4keys[4];
                        reloadButton = heroData.p4keys[9];
                        crouchButton = heroData.p4keys[7];
                        rollButton = heroData.p4keys[8];
                        jumpAndReleaseGrabButton = heroData.p4keys[6];
                        gunSelectionButton = heroData.p4keys[10];
                        grenadeJoyCircleButton = heroData.p4keys[11];
                        grenadeReleaseButton = heroData.p4keys[4];
                        cameraSelectedButton = heroData.p4keys[12];
                        aimCameraButton = heroData.p4keys[13];
                        showRankButton = heroData.p4keys[14];
                        pauseButton = heroData.p4keys[15];
                    }

                    break;
            }
        }
        else
        {
            switch (gameControlToPlay)
            {
                case "Keyboard":
                    joystickActivated = false;
                    keyboardActivated = true;
                    joystickOrderID = "None";
                    GameControl2(heroData.keyboardHorizontalAxis, heroData.keyboardVerticalAxis,
                                heroData.mouseHorizontalAxis, heroData.mouseVerticalAxis, heroData.forwardJumpForce, heroData.rotationSpeed);
                    break;                
                default:
                    joystickActivated = true;
                    keyboardActivated = false;

                    if (joystickOrderID == "First")
                    {
                        GameControl(heroData.p1JoystickKeys[0], heroData.p1JoystickKeys[1],
                                heroData.p1JoystickKeys[2], heroData.p1JoystickKeys[3],
                                heroData.forwardJumpForce, heroData.rotationSpeed);
                    }
                    else if (joystickOrderID == "Second")
                    {
                        GameControl(heroData.p2keys[0], heroData.p2keys[1],
                                heroData.p2keys[2], heroData.p2keys[3],
                                heroData.forwardJumpForce, heroData.rotationSpeed);
                    }
                    else if (joystickOrderID == "Third")
                    {
                        GameControl(heroData.p3keys[0], heroData.p3keys[1],
                                heroData.p3keys[2], heroData.p3keys[3],
                                heroData.forwardJumpForce, heroData.rotationSpeed);
                    }
                    else if (joystickOrderID == "Fourth")
                    {
                        GameControl(heroData.p4keys[0], heroData.p4keys[1],
                                 heroData.p4keys[2], heroData.p4keys[3],
                                 heroData.forwardJumpForce, heroData.rotationSpeed);
                    }
                    break;
            }
        }
       
        //Raycast do personagem no topo de um objeto
        FootRaycast();
        life = Mathf.Clamp(life, 0, heroData.totalLife);
        lifeIcon.fillAmount = life / heroData.totalLife;
        //float x = life;
        lifeCounter.text = life.ToString("F1") + "%"; 

        if(life >= 75)
        {
            lifeIcon.color = Color.green;
            lifeCounter.color = Color.green;
        }
        if (life < 75 && life > 50)
        {
            lifeIcon.color = new Color32(242, 163, 46, 255);
            lifeCounter.color = new Color32(242, 163, 46, 255);
        }
        if (life < 50 && life > 25)
        {
            lifeIcon.color = Color.yellow;
            lifeCounter.color = Color.yellow;
        }
        if (life < 25)
        {
            lifeIcon.color = Color.red;
            lifeCounter.color = Color.red;
        }

        /*if (Input.GetKeyDown("9") && life < 100 && faceUIScript.actualStatus != "Poisoned" && faceUIScript.actualStatus != "Grabbed")
        {
            isRecovering = true;            
        }*/

        if (isRecovering)
        {
            recoverLifeTime -= Time.deltaTime;
            life += heroData.recoverValue * Time.deltaTime;
            faceUIAnimation.SetBool("Flip", true);
            faceUIScript.actualStatus = "Recovering";
            lifeUpParticle.SetActive(true);          

            if(recoverLifeTime <= 0 || life >= 100)
            {               
                recoverLifeTime = heroData.recoverTime;
                faceUIAnimation.SetBool("Flip", false);
                faceUIScript.actualStatus = "";
                isRecovering = false;
                lifeUpParticle.SetActive(false);
            }
        }

        if (showAmmoParticle)
        {           
            timeAmmo -= Time.deltaTime;
            ammoUpParticle.SetActive(true);

            if(timeAmmo <= 0)
            {
                ammoUpParticle.SetActive(false);
                timeAmmo = 2f;
                showAmmoParticle = false;
            }
        }

        if (life <= 0 && !isInstancing)
        {                   
            if (isInMission)
            {                
                missionScript.missionFailed = true;
                missionScript.gameObject.GetComponent<Canvas>().enabled = false;
            }

            if (!dieAfterCheckPistolMission && itemScript.multiplayerSelected)
            {
                directorScript.Spawning_DeadPlayer(this.gameObject.tag, oponentTag, enemyGunType, enemyGunID, hitOnHead, isZombie, this.transform.position, this.transform.rotation);
                Destroy(rootHeroObject.gameObject);
            }

            if (!itemScript.multiplayerSelected && !testerChar)
            {
                missionScript.gameObject.GetComponent<Canvas>().enabled = false;
                missionScript.missionFailed = true;
                missionScript.enabled = false;

                gameoverCanvas.transform.GetChild(0).gameObject.SetActive(true);
                RagDoll rag = Instantiate(ragdoll, transform.position, transform.rotation).transform.GetChild(0).GetComponent<RagDoll>();
                rag.singlePlayer = true;
                rag.tag = this.tag;
                Destroy(rootHeroObject.gameObject);
            }

            isInstancing = true;
        }

        if (dieAfterCheckPistolMission)
        {
            pisTime -= Time.deltaTime;

            if (pisTime <= 0)
            {
                directorScript.Spawning_DeadPlayer(this.gameObject.tag, oponentTag, enemyGunType, enemyGunID, hitOnHead, isZombie, this.transform.position, this.transform.rotation);
                Destroy(rootHeroObject.gameObject);
            }
        }

        //Serve para chamar a função do grab toda hora do zumbi escolhido
        if (checkZombieGrabFunction)
        {
            ZombieGrab(actualZombie);
        }
        else
        {
            timerRegrab -= 1f * Time.deltaTime;
            
            if(timerRegrab <= 0)
            {
                timerRegrab = 0;
                canRegrabedByZombies = true;
            }
        }

        UiElements();

        if (flipFaceSmoke)
        {       
            if (faceUIScript.actualStatus == "Poisoned" || faceUIScript.actualStatus == "Grabbed" ||
                faceUIScript.actualStatus == "Damage Up" || faceUIScript.actualStatus == "Speed Up")
            {
                faceUIAnimation.SetBool("Flip", true);
                timerPoisoned -= Time.deltaTime;
                life -= heroData.poisonDamage * Time.deltaTime;
                breathPoison = true;

                if(timerPoisoned <= 0)
                {
                    faceUIScript.actualStatus = "";
                    timerPoisoned = heroData.poisonedTime;
                    faceUIAnimation.SetBool("Flip", false);
                    headIconsUI.gameObject.SetActive(false);
                    flipFaceSmoke = false;
                    breathPoison = false;
                }
            }
        }

        if (GunSelected == 1)
        {
            if(rifles[0].gameObject.activeInHierarchy && akAmmo > 0 || 
                rifles[1].gameObject.activeInHierarchy && m4Ammo > 0 ||
                rifles[2].gameObject.activeInHierarchy && stonerAmmo > 0 ||
                rifles[3].gameObject.activeInHierarchy && g28Ammo > 0 ||
                rifles[4].gameObject.activeInHierarchy && awpAmmo > 0 ||
                rifles[5].gameObject.activeInHierarchy && scoutAmmo > 0 ||
                rifles[6].gameObject.activeInHierarchy && shotgunAmmo > 0 ||
                rifles[7].gameObject.activeInHierarchy && mp5Ammo > 0 ||
                rifles[8].gameObject.activeInHierarchy && mp40Ammo > 0 ||
                rifles[9].gameObject.activeInHierarchy && bizonAmmo > 0)
            {
                lastAmmoRifles = false;
            }
            else
            {
                lastAmmoRifles = true;
            }           
        }
        else if(GunSelected == 2)
        {
            if(pistols[0].gameObject.activeInHierarchy && glockAmmo > 0 ||
                pistols[1].gameObject.activeInHierarchy && coltAmmo > 0 ||
                pistols[2].gameObject.activeInHierarchy && p38Ammo > 0 ||
                pistols[3].gameObject.activeInHierarchy && coltAnaAmmo > 0 ||
                pistols[4].gameObject.activeInHierarchy && socomAmmo > 0)
            {
                lastAmmoPistols = false;
            }
            else
            {
                lastAmmoPistols = true;
            }
        }

        if (itemScript.multiplayerSelected)
        {
            if (howManyZombiesContacts >= 15)
            {
                isZombie = true;
            }
            else
            {
                isZombie = false;
            }

            if (isZombie && !isRecovering)
            {
                headMaskZombie.SetActive(true);
                anim.speed = 0.65f;
                faceUIAnimation.SetBool("Flip", true);
                faceUIScript.actualStatus = "Infected";
                headIconsUI.gameObject.SetActive(true);

                if (tag == "Player1")
                {
                    infectionAnimScript.whoIsTheChar = itemScript.heroName[0];
                }
                else if (tag == "Player2")
                {
                    infectionAnimScript.whoIsTheChar = itemScript.heroName[1];
                }
                else if (tag == "Player3")
                {
                    infectionAnimScript.whoIsTheChar = itemScript.heroName[2];
                }
                else if (tag == "Player4")
                {
                    infectionAnimScript.whoIsTheChar = itemScript.heroName[3];
                }

                if (!animToInfection)
                {
                    infectionAnimScript.infected = true;
                    animToInfection = true;
                }
            }
            else
            {
                headMaskZombie.SetActive(false);
                anim.speed = 1;
            }
        }
        
        Debug.DrawRay(cameraFirst.transform.position, cameraFirst.transform.forward * 300f, Color.green) ;
    }

    private void FixedUpdate()
    {
        //Controle da direção do pulo com os direcionais caso o personagem não esteja encostado em alguma parede
        if (jump && controlRigJump && !isGrabbing)
        {
            if (keyboardActivated)
            {
                horizontalLeftKey = Input.GetAxis(heroData.keyboardHorizontalAxis);
                verticalLeftKey = Input.GetAxis(heroData.keyboardVerticalAxis);

                rig.velocity = new Vector3(horizontalLeftKey * heroData.forwardJumpForce, rig.velocity.y,
                verticalLeftKey * heroData.forwardJumpForce);
            }
            else
            {
                rig.velocity = new Vector3(horizontalLeftJoy * heroData.forwardJumpForce, rig.velocity.y,
                verticalLeftJoy * heroData.forwardJumpForce);
            }           
        }
    }

    private void GameControl(string leftcontrollerHor, string leftcontrollerVert, string rightControllerHor, string rightControllerVert,
        float forwardJumpForce, float rotationSpeed)
    {
        if(gameControlToPlay == "Keyboard")
        {
            horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            horizontalRightJoy = Input.GetAxis(rightControllerHor);
            verticalRightJoy = Input.GetAxis(rightControllerVert);
        }
        else if(controllerOrder == "joystick 1")
        {
            if (heroData.horizontalAxisInvertedP1)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP1)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP1)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP1)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }
        else if (controllerOrder == "joystick 2")
        {
            if (heroData.horizontalAxisInvertedP2)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP2)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP2)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP2)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }
        else if (controllerOrder == "joystick 3")
        {
            if (heroData.horizontalAxisInvertedP3)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP3)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP3)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP3)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }
        else if (controllerOrder == "joystick 4")
        {
            if (heroData.horizontalAxisInvertedP4)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP4)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP4)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP4)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }

        //Ativando a granada
        GrenadeCode();

        //Código da troca das armas 
        changeGun();

        //Quando a munição acabar a animação de reloading é ativada
        reloadGun();

        //Ativando as cameras
        ActiveCamera(rightControllerHor, rightControllerVert);

        //Mostrar Rank e pausa
        if (joystickActivated && itemScript.multiplayerSelected)
        {
            if (Input.GetKey(controllerOrder + " button " + showRankButton))
            {
                rank.SetActive(true);                
            }
            else
            {
                rank.GetComponent<RankSorter>().hasShowed = false;
                rank.SetActive(false);
            }
        }
        if (keyboardActivated)
        {
            if (Input.GetKey(showRankButton) && itemScript.multiplayerSelected)
            {
                rank.SetActive(true);
            }
            else
            {
                rank.GetComponent<RankSorter>().hasShowed = false;
                rank.SetActive(false);
            }
        }
        if (joystickActivated && Input.GetKeyDown(controllerOrder + " button " + pauseButton) ||
            keyboardActivated && Input.GetKeyDown(pauseButton))
        {
            pauseCanvas.transform.GetChild(0).gameObject.SetActive(true);
            pauseCanvas.transform.GetChild(0).GetComponent<MenusNavigation>().pauseMenu = true;
            pauseCanvas.transform.GetChild(0).GetComponent<MenusNavigation>().playerScript = this.GetComponent<HeroBehavior>();
        }

        //Checa se a animação de hit finalizou para nao Loopar
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("HitBack") || anim.GetCurrentAnimatorStateInfo(0).IsName("HitLeft") ||
            anim.GetCurrentAnimatorStateInfo(0).IsName("HitRight"))
        {
            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                anim.SetBool("HitLeft", false);
                anim.SetBool("HitRight", false);
                anim.SetBool("HitBack", false);
            }
        }

        //Movimento padrão do personagem com os controles
        if (horizontalLeftJoy != 0 || verticalLeftJoy != 0)
        {
            if (horizontalRightJoy == 0 && verticalRightJoy == 0 && !Input.GetMouseButton(0) && !isGrabbing && !activeHandRay && !changeCam && !isHostaged)
            {
                anim.SetBool("Idle", false);
                anim.SetBool("Aim_Crouch", false);
                anim.SetBool("Aim_Walk", false);
                anim.SetBool("Aim_Run", false);
                anim.SetBool("Run_and_Stop", false);

                if (!jump)
                {
                   anim.SetBool("Normal_RunWalk", true);
                }
                              
                anim.SetFloat("Speed", 1f);
                isRunning = true;

                if (canSpawnDustFootRun)
                {
                    GameObject dust = Instantiate(footRunParticle, dustSpawnRun.transform.position, dustSpawnRun.transform.rotation);
                    dust.transform.SetParent(this.transform);
                    canSpawnDustFootRun = false;
                }            

                //Rotação com analógico esquerdo
                Vector3 axis = new Vector3(horizontalLeftJoy, 0, verticalLeftJoy);
                Quaternion rotLeft = Quaternion.LookRotation(axis);
                transform.localRotation = Quaternion.Slerp(transform.localRotation, rotLeft, rotationSpeed * Time.deltaTime);

                //Raycast da cabeça para agarrar
                HeadRaycast();
            }

            //Verifica se o grab está ativo
            if (isGrabbing)
            {
                HandRaycast();
                anim.SetFloat("Hor_Float", horizontalLeftJoy, dampTime, Time.deltaTime);
                anim.SetFloat("Vert_Float", -verticalLeftJoy, dampTime, Time.deltaTime);
                anim.SetFloat("Climb_Movement", 1f);
            }

            //Roll e Rank do personagem
            if (joystickActivated) 
            {
                if (Input.GetKey(controllerOrder + " button " + rollButton))
                {
                    anim.SetBool("Roll", true);
                }
                else
                {
                    anim.SetBool("Roll", false);
                }

                if (Input.GetKey(controllerOrder + " button " + showRankButton))
                {
                    rank.SetActive(true);
                }
                else
                {
                    rank.SetActive(false);
                }
            }
            if (keyboardActivated)
            {
                if (Input.GetKey(rollButton))
                {
                    anim.SetBool("Roll", true);
                }
                else
                {
                    anim.SetBool("Roll", false);
                }

                if (Input.GetKey(showRankButton))
                {
                    rank.SetActive(true);
                }
                else
                {
                    rank.SetActive(false);
                }
            }

            if (!changeCam && !crouchActivated) //Fazendo rifle ter a posição reta 
            {
                rifleIkObject.transform.localEulerAngles = new Vector3(rifleIkObject.transform.localEulerAngles.x, rifleIkObject.transform.localEulerAngles.y, -8.1f);
            }
        }
        else
        {
            //Volta para o idle animation
            anim.SetFloat("Speed", 0);
            anim.SetFloat("Climb_Movement", 0);
            isRunning = false;
            canSpawnDustFootRun = true;

            if (!crouchActivated && !changeCam)
            {
                rifleIkObject.transform.localEulerAngles = new Vector3(rifleIkObject.transform.localEulerAngles.x, rifleIkObject.transform.localEulerAngles.y, 0);
            }          
        }

        //Ativando o crouch para todos os controladores do jogo
        if (crouchActivated)
        {
            anim.SetBool("Crouch", true);
            anim.SetBool("Aim_Run", false);
            anim.SetBool("Aim_Walk", false);
            anim.SetBool("Aim_Stop", false);
            anim.SetBool("Normal_RunWalk", false);

            if (horizontalLeftJoy != 0 || verticalLeftJoy != 0) //Fazendo rifle ter a posição reta 
            {
                if (!changeCam)
                {
                    rifleIkObject.transform.localEulerAngles = new Vector3(rifleIkObject.transform.localEulerAngles.x, rifleIkObject.transform.localEulerAngles.y, 0);
                }
              
            }
            else
            {
                if (!changeCam)
                {
                    rifleIkObject.transform.localEulerAngles = new Vector3(rifleIkObject.transform.localEulerAngles.x, rifleIkObject.transform.localEulerAngles.y, -9.7f);
                }               
            }
        }
        else
        {
            anim.SetBool("Crouch", false);
        }

        //Desativando a animação de recoil das armas de mira e shotgun
        if (gunSniperAnim.GetCurrentAnimatorStateInfo(0).IsName("Sniper_Recoil") &&
                gunSniperAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
        {
            gunSniperAnim.SetBool("Aim_Shake", false);
            isAwping = false;
        }

        //Movimento de rotação e teclas de ação - Joystick e Keyboard
        if (joystickActivated)
        {
            //Troca de arma (variável de controle)
            if (Input.GetKeyDown(controllerOrder + " button " + gunSelectionButton) && !anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Reloading_Gun"))
            {
                GunSelected += 1;

                if (GunSelected > 4)
                {
                    GunSelected = 1;
                }
            }

            //Ativando o Crouch (Tem que ficar aqui pois o crouch é ativado em qualquer momento)
            if (Input.GetKey(controllerOrder + " button " + crouchButton))
            {
                crouchActivated = true;
            }
            else
            {
                crouchActivated = false;
            }

            //Se o analógico direito for usado os eventos de animação estão aqui
            if (horizontalRightJoy != 0 || verticalRightJoy != 0)
            {
                if (!isGrabbing)
                {
                    if (!changeCam)
                    {
                        //Rotação com o analógico direito 
                        Vector3 axis = new Vector3(horizontalRightJoy, 0, verticalRightJoy);
                        Quaternion rotRight = Quaternion.LookRotation(axis);
                        transform.localRotation = Quaternion.Slerp(transform.localRotation, rotRight, rotationSpeed * Time.deltaTime);

                        //Personagem movendo e mirando
                        if (horizontalLeftJoy != 0 || verticalLeftJoy != 0)
                        {
                            //Se as teclas de movimentos especiais WALK e CROUCH não forem usadas,o personagem apenas corre
                            if (!Input.GetKey(controllerOrder + " button " + walkButton) && !crouchActivated)
                            {
                                anim.SetBool("Aim_Crouch", false);
                                anim.SetBool("Aim_Run", true);
                                anim.SetBool("Aim_Walk", false);
                                anim.SetBool("Aim_Stop", false);
                                anim.SetBool("Normal_RunWalk", false);
                            }
                            else if (Input.GetKey(controllerOrder + " button " + walkButton) && !crouchActivated)
                            {
                                anim.SetBool("Aim_Crouch", false);
                                anim.SetBool("Aim_Run", false);
                                anim.SetBool("Aim_Walk", true);
                                anim.SetBool("Aim_Stop", false);
                                anim.SetBool("Normal_RunWalk", false);
                            }
                            else if (crouchActivated)
                            {
                                anim.SetBool("Aim_Crouch", true);
                                anim.SetBool("Aim_Run", false);
                                anim.SetBool("Aim_Walk", false);
                                anim.SetBool("Aim_Stop", false);
                                anim.SetBool("Normal_RunWalk", false);
                            }
                        }

                        //Eventos de movimentos e animação do personagem
                        if (verticalRightJoy < 0)
                        {
                            anim.SetFloat("Vert_Float", -verticalLeftJoy, dampTime, Time.deltaTime);
                            anim.SetFloat("Hor_Float", horizontalLeftJoy, dampTime, Time.deltaTime);
                        }
                        else if (verticalRightJoy > 0)
                        {
                            anim.SetFloat("Vert_Float", verticalLeftJoy, dampTime, Time.deltaTime);
                            anim.SetFloat("Hor_Float", -horizontalLeftJoy, dampTime, Time.deltaTime);
                        }
                        else if (horizontalRightJoy < 0)
                        {
                            anim.SetFloat("Vert_Float", -horizontalLeftJoy, dampTime, Time.deltaTime);
                            anim.SetFloat("Hor_Float", -verticalLeftJoy, dampTime, Time.deltaTime);
                        }
                        else if (horizontalRightJoy > 0)
                        {
                            anim.SetFloat("Vert_Float", horizontalLeftJoy, dampTime, Time.deltaTime);
                            anim.SetFloat("Hor_Float", verticalLeftJoy, dampTime, Time.deltaTime);
                        }
                    }
                    else
                    {
                        if (horizontalLeftJoy != 0 && verticalLeftJoy != 0)
                        {
                            anim.SetBool("Aim_Crouch", false);
                            anim.SetBool("Aim_Walk", false);
                            anim.SetBool("Aim_Stop", true);
                            anim.SetBool("Aim_Run", false);
                            anim.SetBool("Idle", false);
                            anim.SetBool("Normal_RunWalk", false);
                            gunAnim.SetBool("Shake", false);
                        }
                    }
                    
                    //Personagem parado mirando
                    if (horizontalLeftJoy == 0 && verticalLeftJoy == 0)
                    {
                        anim.SetBool("Aim_Crouch", false);
                        anim.SetBool("Aim_Walk", false);
                        anim.SetBool("Aim_Stop", true);
                        anim.SetBool("Aim_Run", false);
                        anim.SetBool("Idle", false);
                        anim.SetBool("Normal_RunWalk", false);
                        gunAnim.SetBool("Shake", false);
                    }   
                }

                //Shake do Rifle
                if(joystickOrderID == "First" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                //------------------------------------
                else if (joystickOrderID == "Second" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                //------------------------------------
                else if (joystickOrderID == "Third" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                //------------------------------------
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;
                        //isAwping = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
            }
                
            //Analógicos sem movimento
            if (horizontalRightJoy == 0 && verticalRightJoy == 0)
            {
                if (horizontalLeftJoy == 0 && verticalLeftJoy == 0 && !crouchActivated && !changeCam)
                {                  
                    anim.SetBool("Crouch", false);
                    anim.SetBool("Aim_Crouch", false);
                    anim.SetBool("Aim_Walk", false);
                    anim.SetBool("Aim_Run", false);
                    anim.SetBool("Normal_RunWalk", false);
                    anim.SetBool("Aim_Stop", false);
                    anim.SetBool("Idle", true);
                    anim.SetBool("Run_and_Stop", true);

                    if (Input.GetKey(controllerOrder + " button " + walkButton) && !isGrabbing)
                    {
                        anim.SetBool("Run_and_Stop", false);
                    }
                    else
                    {
                        //anim.SetBool("Run_and_Stop", false);
                    }
                }                    
            }

            if (changeCam)
            {
                if(joystickOrderID == "First" && gameControlToPlay == "Generic")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Xbox")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Playstation")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                //----------------
                else if (joystickOrderID == "Second" && gameControlToPlay == "Generic")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Xbox")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Playstation")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                //----------------
                else if (joystickOrderID == "Third" && gameControlToPlay == "Generic")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Xbox")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Playstation")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                //----------------
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Generic")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Xbox")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetAxis(shootButton) > 0 && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Playstation")
                {
                    //Shake do Rifle mas quando ele estiver em terceira pessoa
                    if (Input.GetKey(controllerOrder + " button " + shootButton) && !isGrabbing)
                    {
                        if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                        {
                            //Se houver munição e não estiver lançando granada o personagem atira
                            shootingGuns();
                            isShooting = true;
                        }
                        else
                        {
                            isShooting = false;
                        }
                    }
                    else
                    {
                        gunAnim.SetBool("Shake", false);
                        gunAnimPistol.SetBool("Shake_Pistol", false);
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                        gunAnim.SetBool("Sniper_Recoil", false);
                        isShooting = false;

                        if (GunSelected == 3)
                        {
                            anim.SetBool("Active_Melee_1", false);
                            anim.SetBool("Active_Melee_2", false);
                            anim.SetBool("Melee_Idle", true);
                        }
                    }
                }
            }

           //Botões de ação e eventos do personagem
           //Walk/Crouch Button
            if (Input.GetKey(controllerOrder + " button " + walkButton) || crouchActivated && !changeCam)
            {
                if (verticalLeftJoy != 0 || horizontalLeftJoy != 0)
                {
                    if(runWalkFloat < 1f)
                    {
                        runWalkFloat += 3f * Time.deltaTime;
                        anim.SetFloat("Dir", runWalkFloat);
                    }                                     
                }
                else 
                {
                    if (runWalkFloat > 0)
                    {
                        runWalkFloat -= 2f * Time.deltaTime;
                        anim.SetFloat("Dir", runWalkFloat);
                    }                        
                }
            }
            else
            {
                if (runWalkFloat > 0)
                {
                    runWalkFloat -= 1f * Time.deltaTime;
                    anim.SetFloat("Dir", runWalkFloat);
                }
            }

            //Jump Button
            if (Input.GetKeyDown(controllerOrder + " button " + jumpAndReleaseGrabButton) && !crouchActivated)
            {
                jump = true;
                rotateCamera = false;

                if(isInMission && missionScript.jumpMission)
                {
                    missionScript.missionFailed = true;
                }

                //Caso o grab estiver ativo o código de release é executado
                if (isGrabbing)
                {
                    anim.SetBool("Grab", false);
                    anim.applyRootMotion = true;
                }

                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Normal_Run_and_Walk_Tree") ||
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Idle_Normal") ||
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Stay_Aim") ||
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Aim_Walk_Tree") ||
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Aim_Run_Tree "))
                {
                    if (canRegrabedByZombies)
                    {
                        anim.SetBool("Jumping", true);
                    }                   
                }
            } 
        }

        if (keyboardActivated)
        {            
            //Ativando o Crouch (Tem que ficar aqui pois o crouch é ativado em qualquer momento)
            if (Input.GetKey(crouchButton))
            {
                crouchActivated = true;
            }
            else
            {
                crouchActivated = false;
            }

            //Troca de arma (variável de controle)
            if (Input.GetKeyDown(gunSelectionButton) && !anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Reloading_Gun"))
            {
                GunSelected += 1;

                if (GunSelected > 4)
                {
                    GunSelected = 1;
                }
            }

            //Rotação com o mouse ativado se o grab não estiver ativo 
            if (Input.GetMouseButton(0) && !isGrabbing)
            {
                print(lastAmmoRifles);
                if (GunSelected == 1 && !lastAmmoRifles || GunSelected == 2 && !lastAmmoPistols || GunSelected == 3)
                {
                    //Se houver munição e não estiver lançando granada o personagem atira
                    shootingGuns();
                    isShooting = true;
                    
                }
                else
                {
                    isShooting = false;
                }
                
                //Shake do Rifle (Só ativa quando a arma não está recarregando)                              
                anim.SetBool("Aim_Crouch", false);
                anim.SetBool("Aim_Walk", false);
                anim.SetBool("Aim_Stop", true);
                anim.SetBool("Aim_Run", false);
                anim.SetBool("Idle", false);

                if (!changeCam)
                {                  
                    // Generate a plane that intersects the transform's position with an upwards normal.
                    Plane playerPlane = new Plane(Vector3.up, transform.position);

                    // Generate a ray from the cursor position
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                    // Determine the point where the cursor ray intersects the plane.
                    // This will be the point that the object must look towards to be looking at the mouse.
                    // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
                    //   then find the point along that ray that meets that distance.  This will be the point
                    //   to look at.
                    float hitdist = 0.0f;
                    // If the ray is parallel to the plane, Raycast will return false.
                    if (playerPlane.Raycast(ray, out hitdist))
                    {
                        // Get the point along the ray that hits the calculated distance.
                        Vector3 targetPoint = ray.GetPoint(hitdist);

                        // Determine the target rotation.  This is the rotation if the transform looks at the target point.
                        Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

                        // Smoothly rotate towards the target point.
                        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                    }

                    //Personagem movendo e mirando***********************
                    if (horizontalLeftJoy != 0 || verticalLeftJoy != 0)
                    {
                        //Se as tecla de movimento WALK,CROUCH ou SHOOT não forem usadas,o personagem apenas corre
                        if (!Input.GetKey(walkButton) && !crouchActivated)
                        {
                            anim.SetBool("Aim_Crouch", false);
                            anim.SetBool("Aim_Walk", false);
                            anim.SetBool("Aim_Run", true);
                            anim.SetBool("Aim_Stop", false);
                            anim.SetBool("Normal_RunWalk", false);
                        }
                        else if (Input.GetKey(walkButton) && !crouchActivated)
                        {
                            anim.SetBool("Aim_Crouch", false);
                            anim.SetBool("Aim_Walk", true);
                            anim.SetBool("Aim_Run", false);
                            anim.SetBool("Aim_Stop", false);
                            anim.SetBool("Normal_RunWalk", false);
                        }
                        else if (crouchActivated)
                        {
                            anim.SetBool("Aim_Crouch", true);
                            anim.SetBool("Aim_Walk", false);
                            anim.SetBool("Aim_Run", false);
                            anim.SetBool("Aim_Stop", false);
                            anim.SetBool("Normal_RunWalk", false);
                        }
                    }

                    //Eventos de movimentos e animação do personagem
                    Vector2 mp = Input.mousePosition;
                    Vector2 playerPos = Camera.main.WorldToScreenPoint(transform.position);

                    if (transform.eulerAngles.y > 290 && transform.eulerAngles.y < 360)
                    {
                        anim.SetFloat("Vert_Float", verticalLeftJoy, dampTime, Time.deltaTime);
                        anim.SetFloat("Hor_Float", -horizontalLeftJoy, dampTime, Time.deltaTime);
                    }
                    else if (transform.eulerAngles.y > 0 && transform.eulerAngles.y < 35)
                    {
                        anim.SetFloat("Vert_Float", verticalLeftJoy, dampTime, Time.deltaTime);
                        anim.SetFloat("Hor_Float", -horizontalLeftJoy, dampTime, Time.deltaTime);
                    }
                    else if (transform.eulerAngles.y > 35 && transform.eulerAngles.y < 120)
                    {
                        anim.SetFloat("Vert_Float", horizontalLeftJoy, dampTime, Time.deltaTime);
                        anim.SetFloat("Hor_Float", verticalLeftJoy, dampTime, Time.deltaTime);
                    }
                    else if (transform.eulerAngles.y > 120 && transform.eulerAngles.y < 230)
                    {
                        anim.SetFloat("Vert_Float", -verticalLeftJoy, dampTime, Time.deltaTime);
                        anim.SetFloat("Hor_Float", horizontalLeftJoy, dampTime, Time.deltaTime);
                    }
                    else if (transform.eulerAngles.y > 230 && transform.eulerAngles.y < 290)
                    {
                        anim.SetFloat("Vert_Float", -horizontalLeftJoy, dampTime, Time.deltaTime);
                        anim.SetFloat("Hor_Float", -verticalLeftJoy, dampTime, Time.deltaTime);
                    }
                }
            }
            else
            {
                if(verticalLeftJoy == 0 && horizontalLeftJoy == 0 && !changeCam)
                {                   
                    anim.SetBool("Aim_Crouch", false);
                    anim.SetBool("Aim_Walk", false);
                    anim.SetBool("Aim_Stop", false);
                    anim.SetBool("Aim_Run", false);
                    anim.SetBool("Idle", true);
                    anim.SetBool("Normal_RunWalk", false);
                    anim.SetBool("Run_and_Stop", true);

                    if (Input.GetKey(walkButton) && !isGrabbing)
                    {
                        anim.SetBool("Run_and_Stop", false);
                    }

                    //Ativa se as armas de mira e shotgun nao estiverem ativas
                    if (!rifles[4].activeInHierarchy && !rifles[5].activeInHierarchy && !rifles[6].activeInHierarchy)
                    {
                        gunAnim.SetBool("Shake", false);
                    }                    
                }

                if (GunSelected == 3)
                {
                    anim.SetBool("Active_Melee_1", false);
                    anim.SetBool("Active_Melee_2", false);
                    anim.SetBool("Melee_Idle", true);
                }

                gunAnim.SetBool("Shake", false);
                gunAnim.SetBool("Sniper_Recoil", false);
                gunSniperAnim.SetBool("Rifle_Aim_Scope", false);
                gunAnimPistol.SetBool("Shake_Pistol", false);

                isShooting = false;
                //isAwping = false;
            }

            //Botões de ação e eventos do personagem
            //Walk/Crouch Button
            if (Input.GetKey(walkButton) || crouchActivated && !changeCam)
            {
                if (verticalLeftJoy != 0 || horizontalLeftJoy != 0)
                {
                    if (runWalkFloat < 1f)
                    {
                        runWalkFloat += 3f * Time.deltaTime;
                        anim.SetFloat("Dir", runWalkFloat);
                    }
                }
                else
                {
                    if (runWalkFloat > 0)
                    {
                        runWalkFloat -= 2f * Time.deltaTime;
                        anim.SetFloat("Dir", runWalkFloat);
                    }
                }
            }
            else
            {
                if (runWalkFloat > 0)
                {
                    runWalkFloat -= 1f * Time.deltaTime;
                    anim.SetFloat("Dir", runWalkFloat);
                }
            }

            //Jump Button
            if (Input.GetKeyDown(jumpAndReleaseGrabButton) && !crouchActivated)
            {
                jump = true;
                activeHandRay = false;
                rotateCamera = false;;

                if (isInMission && missionScript.jumpMission)
                {
                    missionScript.missionFailed = true;
                }

                //Caso o grab estiver ativo o código de release é executado
                if (isGrabbing)
                {                    
                    anim.SetBool("Grab", false);
                    anim.applyRootMotion = true;
                }
                //O personagem pula apenas com essas animações ativas
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Normal_Run_and_Walk_Tree") ||
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Idle_Normal") || 
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Stay_Aim") ||
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Aim_Walk_Tree") ||
                 anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Aim_Run_Tree "))
                {
                    if (canRegrabedByZombies)
                    {
                        anim.SetBool("Jumping", true);
                    }                    
                }               
            }
        }
    }

    private void GameControl2(string leftcontrollerHor, string leftcontrollerVert, string rightControllerHor, string rightControllerVert,
        float forwardJumpForce, float rotationSpeed)
    {
        if (gameControlToPlay == "Keyboard")
        {
            horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            horizontalRightJoy = Input.GetAxis(rightControllerHor);
            verticalRightJoy = Input.GetAxis(rightControllerVert);
        }
        else if (controllerOrder == "joystick 1")
        {
            if (heroData.horizontalAxisInvertedP1)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP1)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP1)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP1)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }
        else if (controllerOrder == "joystick 2")
        {
            if (heroData.horizontalAxisInvertedP2)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP2)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP2)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP2)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }
        else if (controllerOrder == "joystick 3")
        {
            if (heroData.horizontalAxisInvertedP3)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP3)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP3)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP3)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }
        else if (controllerOrder == "joystick 4")
        {
            if (heroData.horizontalAxisInvertedP4)
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor) * -1;
            }
            else
            {
                horizontalLeftJoy = Input.GetAxis(leftcontrollerHor);
            }

            if (heroData.verticalAxisInvertedP4)
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert) * -1;
            }
            else
            {
                verticalLeftJoy = Input.GetAxis(leftcontrollerVert);
            }

            if (heroData.horizontalAimAxisInvertedP4)
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor) * -1;
            }
            else
            {
                horizontalRightJoy = Input.GetAxis(rightControllerHor);
            }

            if (heroData.verticalAimAxisInvertedP4)
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert) * -1;
            }
            else
            {
                verticalRightJoy = Input.GetAxis(rightControllerVert);
            }
        }

        //Movimento padrão do personagem com os controles
        if (horizontalLeftJoy != 0 || verticalLeftJoy != 0)
        {
            if (horizontalRightJoy == 0 && verticalRightJoy == 0 && !Input.GetMouseButton(0) && !isGrabbing && !activeHandRay && !changeCam && !isHostaged)
            {
                anim.SetBool("Idle", false);
                anim.SetBool("Aim_Crouch", false);
                anim.SetBool("Aim_Walk", false);
                anim.SetBool("Aim_Run", false);
                anim.SetBool("Run_and_Stop", false);

                if (!jump)
                {
                    anim.SetBool("Normal_RunWalk", true);
                }

                anim.SetFloat("Speed", 1f);
                isRunning = true;

                //Rotação com analógico esquerdo
                Vector3 axis = new Vector3(horizontalLeftJoy, 0, verticalLeftJoy);
                Quaternion rotLeft = Quaternion.LookRotation(axis);
                transform.localRotation = Quaternion.Slerp(transform.localRotation, rotLeft, rotationSpeed * Time.deltaTime);
            }
        }

        //Analógicos sem movimento
        if (horizontalLeftJoy == 0 && verticalLeftJoy == 0)
        {
            anim.SetFloat("Speed", 0);
        }

    }

    //A coroutina faz com que o IK da mão só se ative de novo quando o personagem cair no chão
    IEnumerator ReactivateIK()
    {
        yield return new WaitForSeconds(0.5f);
        releasedWall = true;             
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Desativando a animação do pulo e controlando o grab e os raycasts
        if (collision.gameObject.tag == "Ground")
        {
            StartCoroutine(ReactivateIK());
            anim.SetBool("Release_Grab", true);
            anim.SetBool("Grab", false);
            anim.SetBool("Jumping", false);
            anim.SetBool("Is_In_Floor", true);//Se o personagem estiver no chão após cair de um lugar alto a animação de fall é executada
            anim.SetBool("Falling", false);
            anim.SetBool("Release_Top_Grabber", false);
            
            jump = false;
            isInTop = false;
            activeHeadRay = false;
            activeHandRay = false;
            isGrabbing = false; //Se o personagem cai cedo demais no chão já desativa o grab pra não dar merda    
            playerIsOnTop = false;

            capsuleCol.height = 4.9f;
            capsuleCol.center = new Vector3(0, 2.4f, 0);

            Instantiate(jumpParticles, transform.position, Quaternion.Euler(-90f,0,0));

            rig.drag = 1f;
        }

        //Verifica se o personagem está em cima de um objeto alto
        if (collision.gameObject.tag == "Grabber")
        {
            anim.SetBool("Jumping", false);
            anim.applyRootMotion = true;
        }

        if (collision.gameObject.tag == "ZombiePrefab" && canRegrabedByZombies)
        {
            //Checa o script do zumbi que atacou para ver se ele está agarrando o player ou não
            actualZombie = collision.gameObject;
            checkZombieGrabFunction = true;
            isNotHostage = false;
            canRegrabedByZombies = false;
            releaseTime = 2f;           
        }
    }

    private void ZombieGrab(GameObject mopa)
    {
        if (!isHostaged)
        {
            StandardZombie stanZombieScript = mopa.gameObject.GetComponent<StandardZombie>();
            actualZombie = stanZombieScript.gameObject;
            incomingZombieAttack = stanZombieScript.attackChoosed;
        }
        
        //Refere a array de ataque do zumbi
        if (incomingZombieAttack == 4 || incomingZombieAttack == 5)
        {
            isHostaged = true;
            anim.SetBool("Hostaged", true);
            anim.SetBool("HitRight", false);
            anim.SetBool("HitLeft", false);
            anim.SetBool("HitBack", false);
            transform.position = new Vector3(actualZombie.transform.position.x - 0.5f, transform.position.y, actualZombie.transform.position.z - 0.5f);

            releaseTime -= 1f * Time.deltaTime;

            faceUIAnimation.SetBool("Flip", true);
            faceUIScript.actualStatus = "Grabbed";
            print("pego");

            if (releaseTime <= 0)
            {
                anim.SetBool("Hostaged", false);
                print("Released");
                flipFaceSmoke = false;
                faceUIScript.actualStatus = "";
                faceUIAnimation.SetBool("Flip", false);

                if (!isRunning && !isGrabbing)
                {
                    anim.SetBool("HitBack", true);
                    isHostaged = false;
                    checkZombieGrabFunction = false;
                    timerRegrab = invulnerableGrab;
                }
                else
                {
                    int hitSelected = Random.Range(0, 2);

                    if (hitSelected == 0 && !isGrabbing)
                    {
                        anim.SetBool("HitLeft", true);
                        anim.SetBool("HitRight", false);
                        hitSelected = 1;
                        isHostaged = false;
                        checkZombieGrabFunction = false;
                        timerRegrab = invulnerableGrab;
                    }
                    else if (hitSelected == 2 && !isGrabbing)
                    {
                        anim.SetBool("HitRight", true);
                        anim.SetBool("HitLeft", false);
                        hitSelected = 0;
                        isHostaged = false;
                        checkZombieGrabFunction = false;
                        timerRegrab = invulnerableGrab;
                    }
                }                
            }
        }
        else
        {
            if (!isRunning && !isGrabbing && !isNotHostage)
            {
                isNotHostage = true;
                anim.SetBool("HitBack", true);
                checkZombieGrabFunction = false;
            }
            else
            {
                int hitSelected = Random.Range(0, 2);

                if (hitSelected == 0 && !isGrabbing && !isNotHostage)
                {
                    isNotHostage = true;
                    anim.SetBool("HitLeft", true);
                    anim.SetBool("HitRight", false);
                    hitSelected = 1;
                    checkZombieGrabFunction = false;
                }
                else if (hitSelected == 2 && !isGrabbing && !isNotHostage)
                {
                    isNotHostage = true;
                    anim.SetBool("HitRight", true);
                    anim.SetBool("HitLeft", false);
                    hitSelected = 0;
                    checkZombieGrabFunction = false;
                }
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            anim.applyRootMotion = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        //Ativando os direcionais do pulo com o rigidbody e muda o tamanho do colisor
        if (collision.gameObject.tag == "Ground" && jump)
        {
            activeHeadRay = true;
            anim.applyRootMotion = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Verifica se o personagem está em cima ou caiu de algum lugar alto para a animação de fall ser executada
        if (other.tag == "TopGrabber")
        {
            anim.SetBool("Is_In_Floor", true);
            anim.SetBool("Falling", false);
        }

        if (other.tag == "LowDamageBox")
        {
            //print("Tomou dano do: " + other.name);
            howManyZombiesContacts += 1;
            life -= 1.5f;
            bloodScreen.DOPlayById("1");
        }

        if (other.gameObject.tag == "ZombieStan" && life > 0)
        {
            if (!isRunning && !isGrabbing && !isHostaged)
            {
                anim.SetBool("HitBack", true);
            }
            else
            {
                int hitSelected = Random.Range(0, 2);

                if (hitSelected == 0 && !isGrabbing && !isHostaged)
                {
                    anim.SetBool("HitLeft", true);
                    anim.SetBool("HitRight", false);
                    hitSelected = 1;
                }
                else if (hitSelected == 2 && !isGrabbing && !isHostaged)
                {
                    anim.SetBool("HitRight", true);
                    anim.SetBool("HitLeft", false);
                    hitSelected = 0;
                }
            }

            life -= 0.01f;
        }

        if(other.gameObject.name == "Hammer")
        {
            life -= 3;

            if (!isRunning && !isGrabbing && !isHostaged)
            {
                anim.SetBool("HitBack", true);
            }
            else
            {
                int hitSelected = Random.Range(0, 2);

                if (hitSelected == 0 && !isGrabbing && !isHostaged)
                {
                    anim.SetBool("HitLeft", true);
                    anim.SetBool("HitRight", false);
                    hitSelected = 1;
                }
                else if (hitSelected == 2 && !isGrabbing && !isHostaged)
                {
                    anim.SetBool("HitRight", true);
                    anim.SetBool("HitLeft", false);
                    hitSelected = 0;
                }
            }
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        
    }

    private void HeadRaycast()
    {
        if (activeHeadRay)
        {
            //Desenha o raycast do personagem no editor
            Vector3 lookRay = transform.TransformDirection(Vector3.forward) * 0.25f;
            Debug.DrawRay(rayHeadObject.transform.position, lookRay, Color.green);

            RaycastHit hit;
            if (Physics.Raycast(rayHeadObject.transform.position, transform.forward, out hit, 0.25f,rayMaskForHands))
            {
                //O raio verifica se está perto de alguma parede normal para evitar o bug de ficar preso no ar
                if (hit.collider.tag == "Wall")
                {
                    //controlRigJump = false;
                    //anim.applyRootMotion = true;
                }

                //Se o raio estiver em cima de um objeto escalável e a variável booleana de controle for falso,a ação do grab
                //é executada
                if (hit.collider.tag == "Grabber" && !isGrabbing && !anim.GetCurrentAnimatorStateInfo(0).IsName("Climb_Jump_Out"))
                {
                    isGrabbing = true;
                    rotateCamera = true;
                    rig.useGravity = false;
                    anim.applyRootMotion = true;
                    anim.SetBool("Grab", true);
                    anim.SetBool("Jumping", false);
                    anim.SetBool("Release_Grab", false);
                    anim.SetBool("Normal_RunWalk", false);
                    transform.rotation = Quaternion.LookRotation(-hit.normal);
                    releasedWall = false;
                }
            }
            else
            {
                //Caso não esteja perto de alguma parede o personagem é controlado pelo rigidbody
                controlRigJump = true;
            }
        }
    }

    private void HandRaycast()
    {
        //Desenho do raycast das mãos
        Vector3 lookRay = transform.TransformDirection(Vector3.forward) * 0.5f;
        Debug.DrawRay(rayRHandObject.transform.position, lookRay, Color.red);
        Debug.DrawRay(rayLHandObject.transform.position, lookRay, Color.red);

        //Controle do Raycast das mãos
        if (activeHandRay)
        {
            RaycastHit lHandHit;
            if (Physics.Raycast(rayLHandObject.transform.position, transform.forward, out lHandHit, 0.5f, rayMaskForHands))
            {
                //Se atingir o objeto do topo,o personagem executa o código para subir
                if (lHandHit.collider.tag == "TopGrabber")
                {
                    isGrabbing = false;
                    jump = false;
                    playerIsOnTop = true;

                    anim.SetBool("Grab_to_Climb", true);
                    anim.SetBool("Grab", false);
                    anim.SetBool("Release_Grab", false);
                    releasedWall = true;

                    transform.position = lHandHit.point - new Vector3(0, 0.5f, 0);
                    transform.rotation = Quaternion.LookRotation(-lHandHit.normal);

                    capsuleCol.height = 4.9f;
                    capsuleCol.center = new Vector3(0, 2.4f, 0);
                }

                if (lHandHit.collider.tag == "TopAutoJump")
                {
                    anim.SetBool("Release_Top_Grabber", true);
                    transform.position = lHandHit.point - new Vector3(0, 0.5f, 0);
                    transform.rotation = Quaternion.LookRotation(-lHandHit.normal);
                }
            }
            else
            {
                //Senão existir algo mais para agarrar,o personagem automaticamente solta o objeto
                rotateCamera = false;
                anim.SetBool("Grab", false);
                //playerIsOnTop = false;
            }

            RaycastHit rHandHit;
            if (Physics.Raycast(rayRHandObject.transform.position, transform.forward, out rHandHit, 0.5f, rayMaskForHands))
            {

            }
            else
            {
                //Senão existir algo mais para agarrar,o personagem automaticamente solta o objeto
                rotateCamera = false;
                anim.SetBool("Grab", false);
            }
        }
    }

    private void FootRaycast()
    {
        if (isInTop)
        {
            Vector3 footRay = transform.TransformDirection(-transform.up) * 2f;
            Debug.DrawRay(rayFootsObject.transform.position, footRay, Color.blue);

            RaycastHit footHit;
            if (Physics.Raycast(rayFootsObject.transform.position, -transform.up, out footHit, 2f, rayMaskForHands))
            {
                if (footHit.collider.tag == "TopGrabber")
                {
                    anim.SetBool("Falling", false);
                    anim.SetBool("Is_In_Floor", true);                    
                }
            }
            else
            {
                anim.SetBool("Is_In_Floor", false);
                anim.SetBool("Falling", true);
                anim.SetBool("Aim_Crouch", false);
                anim.SetBool("Aim_Walk", false);
                anim.SetBool("Aim_Run", false);
                anim.SetBool("Run_and_Stop", false);
                anim.SetBool("Normal_RunWalk", false);

                jump = true;
                controlRigJump = true;                
                anim.applyRootMotion = false;
                rig.useGravity = true;//Se tiver dando merda a gravidade APAGA ISSO
            }
        }
    }

    private void GrenadeCode()
    {
        //Código para Lançar a Granada
        if (GunSelected == 4 && grenadeLimit > 0)
        {
            if (keyboardActivated)
            {
                //O raycast verifica se o mouse está colidindo com alguma coisa.O mouse precisa tocar em algo no cenario
                if (Input.GetMouseButton(0))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        float minimumGrenadeLimit = heroData.weakGrenadeLimit - 0.1f;

                        var position = hit.point;
                        var relativePos = circle.transform.position - position;

                        if (relativePos.x > -minimumGrenadeLimit && relativePos.x < heroData.weakGrenadeLimit &&
                            relativePos.z > -minimumGrenadeLimit && relativePos.z < heroData.weakGrenadeLimit)
                        {
                            utilitieForce = "Weak";
                            Vector3 newForce = new Vector3(1, 0, 1);
                            circle.transform.localScale = Vector3.Lerp(circle.transform.localScale, newForce, 0.25f);

                            Renderer circleMat = circle.GetComponent<Renderer>();
                            circleMat.material.color = new Color32(0, 180, 23, 71);
                        }
                        else if (relativePos.x > heroData.strongGrenadeLimit && relativePos.z > heroData.strongGrenadeLimit ||
                            relativePos.x < heroData.strongGrenadeLimit && relativePos.z > heroData.strongGrenadeLimit ||
                            relativePos.x > heroData.strongGrenadeLimit && relativePos.z < heroData.strongGrenadeLimit ||
                            relativePos.x < heroData.strongGrenadeLimit && relativePos.z < heroData.strongGrenadeLimit)
                        {
                            utilitieForce = "Strong";
                            Vector3 newForce = new Vector3(2, 0, 2);
                            circle.transform.localScale = Vector3.Lerp(circle.transform.localScale, newForce, 0.25f);

                            Renderer circleMat = circle.GetComponent<Renderer>();
                            circleMat.material.color = new Color32(180, 0, 6, 71);
                        }
                    }
                }
                //Quando o botão é solto a animação da granada é executada
                if (Input.GetMouseButtonUp(0))
                {
                    if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || awpAmmo > 0 || scoutAmmo > 0 ||
                       shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                       p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                    {
                        circle.transform.localScale = Vector3.zero;
                        anim.SetBool("Toss_Grenade", true);
                        anim.SetLayerWeight(1, 1);
                        anim.Play("Armature_Grenade_Toss", -1, 0f);
                        recoverAnimTime = 1f;
                    }
                }
            }

            if (joystickActivated)
            {
                if (joystickOrderID == "First" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) == -1 && !grenadeCircleTrigger)
                    {
                        grenadeCircleTrigger = true;
                        joystickCircleControl = !joystickCircleControl;
                    }

                    else if (Input.GetAxis(shootButton) == 0)
                    {
                        grenadeCircleTrigger = false;
                    }
                }
                //--------------
                else if (joystickOrderID == "Second" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) == -1 && !grenadeCircleTrigger)
                    {
                        grenadeCircleTrigger = true;
                        joystickCircleControl = !joystickCircleControl;
                    }

                    else if (Input.GetAxis(shootButton) == 0)
                    {
                        grenadeCircleTrigger = false;
                    }
                }
                //----------------------
                else if (joystickOrderID == "Third" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Xbox")
                {                    
                    if (Input.GetAxis(shootButton) == -1 && !grenadeCircleTrigger)
                    {
                        grenadeCircleTrigger = true;
                        joystickCircleControl = !joystickCircleControl;
                    }

                    else if (Input.GetAxis(shootButton) == 0) 
                    {
                        grenadeCircleTrigger = false;
                    }
                }
                //--------------
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + grenadeJoyCircleButton))
                    {
                        joystickCircleControl = !joystickCircleControl;
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) == -1 && !grenadeCircleTrigger)
                    {
                        grenadeCircleTrigger = true;
                        joystickCircleControl = !joystickCircleControl;
                    }

                    else if (Input.GetAxis(shootButton) == 0)
                    {
                        grenadeCircleTrigger = false;
                    }
                }

                if (!joystickCircleControl)
                {
                    utilitieForce = "Weak";
                    Vector3 newForce = new Vector3(1, 0, 1);
                    circle.transform.localScale = Vector3.Lerp(circle.transform.localScale, newForce, 0.25f);

                    Renderer circleMat = circle.GetComponent<Renderer>();
                    circleMat.material.color = new Color32(0, 180, 23, 71);
                }

                if (joystickCircleControl)
                {
                    utilitieForce = "Strong";
                    Vector3 newForce = new Vector3(2, 0, 2);
                    circle.transform.localScale = Vector3.Lerp(circle.transform.localScale, newForce, 0.25f);

                    Renderer circleMat = circle.GetComponent<Renderer>();
                    circleMat.material.color = new Color32(180, 0, 6, 71);
                }
               //---------------------------
                if (joystickOrderID == "First" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0)
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "First" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                //---------------------------
                if (joystickOrderID == "Second" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0)
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "Second" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                //---------------------------
                if (joystickOrderID == "Third" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0)
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "Third" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                //---------------------------
                if (joystickOrderID == "Fourth" && gameControlToPlay == "Xbox")
                {
                    if (Input.GetAxis(shootButton) > 0)
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Playstation")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
                else if (joystickOrderID == "Fourth" && gameControlToPlay == "Generic")
                {
                    if (Input.GetKeyDown(controllerOrder + " button " + shootButton))
                    {
                        //Isso é pra não bugar o reload das armas
                        if (akAmmo > 0 || m4Ammo > 0 || stonerAmmo > 0 || g28Ammo > 0 || scoutAmmo > 0 ||
                          shotgunAmmo > 0 || mp5Ammo > 0 || mp40Ammo > 0 || bizonAmmo > 0 || glockAmmo > 0 || coltAmmo > 0 ||
                          p38Ammo > 0 || coltAnaAmmo > 0 || socomAmmo > 0)
                        {
                            circle.transform.localScale = Vector3.zero;
                            anim.SetBool("Toss_Grenade", true);
                            anim.SetLayerWeight(1, 1);
                            anim.Play("Armature_Grenade_Toss", -1, 0f);
                            recoverAnimTime = 1f;
                        }
                    }
                }
            }
        }
        //Quando a animação acaba a layer do mecanim volta ao normal
        if (anim.GetCurrentAnimatorStateInfo(1).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Grenade_Toss"))
        {
            if (recoverAnimTime > 0)
            {
                recoverAnimTime -= 3f * Time.deltaTime;
            }
            else
            {
                recoverAnimTime = 0;
                GunSelected = 1;
                anim.SetBool("Toss_Grenade", false);
            }
            anim.SetLayerWeight(1, recoverAnimTime);
        }
    }

    //Função de pulo
    public void JumpFunction()
    {
        rig.AddForce(transform.up * heroData.jumpStrenght);     
    }

    //Pra animação de pulo desativar o root motion sozinho
    public void TurnOffRootMotion()
    {
        anim.applyRootMotion = false;
    }

    public void TurnOnRootMotion()
    {
        anim.applyRootMotion = true;
        rig.useGravity = true;
    }

    //Função de parar a força do pulo quando agarrar
    public void StopForce()
    {
        rig.drag = 10f;
    }

    //Função de soltar do grab
    public void ReleasedGrab()
    {
        isGrabbing = false;
        anim.applyRootMotion = false;
        rig.useGravity = true;
        rig.drag = 1f;
    }

    //Ativando o raycast da mão depois que a variavel isGrabbing é ativada
    public void ActiveHandRays()
    {
        activeHandRay = true;
    }

    //Quando o personagem alcança o topo do objeto é executado esse código
    public void ClimbToTop()
    {
        rig.useGravity = true;
        anim.SetBool("Grab_to_Climb", false);
        isInTop = true;
        rig.drag = 1f;
    }

    public void AutoJumpOfTop()
    {
        rig.useGravity = true;
        anim.applyRootMotion = false;
        rig.drag = 1f;
    }

    //Quando a animação de subir e idle é completada,o personagem volta a ser controlado normalmente
    public void ReactivatedNormalMovement()
    {
        activeHandRay = false;       
    }

    //Ativando a granada
    public void GrenadeLauncher()
    {
        //O script que adiciona força na granada está no objeto spawnador
        grenadeSpawner.SetActive(true);
    }

    //Mudar de arma
    public void changeGun()
    {
        if (GunSelected == 1 && !gunActiveControl)
        {
            ikCode.rifleInHands = true;
            ikCode.grenadeInHands = false;
            circle.transform.localScale = Vector3.zero;
            rifles[rifleSelected].gameObject.SetActive(true);
            grenades[grenadeSelected].gameObject.SetActive(false);
            meleeGuns[meleeGunSelected].gameObject.SetActive(false);
        }
        else if (GunSelected == 2 && !gunActiveControl)
        {
            ikCode.rifleInHands = false;
            ikCode.pistolInHands = true;
            pistols[pistolSelected].gameObject.SetActive(true);
            rifles[rifleSelected].gameObject.SetActive(false);
            meleeGuns[meleeGunSelected].gameObject.SetActive(false);
            cameraFirst.gameObject.SetActive(false);
            scopeState = 0;
        }
        else if (GunSelected == 3 && !gunActiveControl)
        {
            gunAnimPistol.SetBool("Shake_Pistol", false);
            ikCode.pistolInHands = false;
            pistols[pistolSelected].gameObject.SetActive(false);
            meleeGuns[meleeGunSelected].gameObject.SetActive(true);
            cameraFirst.gameObject.SetActive(false);
            scopeState = 0;

            if (!isGrabbing && releasedWall)
            {
                anim.SetLayerWeight(2, 0.8f);
            }
            else
            {
                anim.SetLayerWeight(2, 0);
            }
            
        }
        else if (GunSelected == 4 && !gunActiveControl)
        {
            gunAnimPistol.SetBool("Shake_Pistol", false);
            ikCode.grenadeInHands = true;
            meleeGuns[meleeGunSelected].gameObject.SetActive(false);
            grenades[grenadeSelected].gameObject.SetActive(true);
            cameraFirst.gameObject.SetActive(false);
            scopeState = 0;
            anim.SetLayerWeight(2, 0);
        }
    }

    //Recarregar armas
    public void reloadGun()
    {
        //Se a granada não estiver selecionada chama a animação
        if (!gunActiveControl && GunSelected != 3  && GunSelected != 4 && ammoIcon.rect.width > 0)
        {
            if (akAmmo <= 0 && !lastAmmoRifles || m4Ammo <= 0 && !lastAmmoRifles || stonerAmmo <=0 && !lastAmmoRifles || g28Ammo <=0 && !lastAmmoRifles || awpAmmo <= 0 && !lastAmmoRifles || scoutAmmo <=0 && !lastAmmoRifles || 
               shotgunAmmo <= 0 && !lastAmmoRifles || mp5Ammo <= 0 && !lastAmmoRifles || mp40Ammo <= 0 && !lastAmmoRifles || bizonAmmo <= 0 && !lastAmmoRifles || glockAmmo <= 0 && !lastAmmoPistols || coltAmmo <= 0 && !lastAmmoPistols ||
               p38Ammo <= 0 && !lastAmmoPistols || coltAnaAmmo <= 0 && !lastAmmoPistols || socomAmmo <= 0 && !lastAmmoPistols)
            {
                gunActiveControl = true;
                anim.SetBool("Reloading_Gun", true);
                anim.Play("Armature_Reloading_Gun", -1, 0f);
                anim.SetLayerWeight(1, 1);
                recoverAnimTime = 1f;
                scopeState = 0;
                headIconUIReloading.gameObject.SetActive(true); // Ativa e passa a informação de qual icone ativar no headUI 
                
                if(GunSelected == 1)
                {
                    ammoIconValueForRifle -= 77;
                }
                else if(GunSelected == 2)
                {
                    ammoIconValueForPistol -= 77;
                }

                if (scopeState >= 1)
                {
                    scopeState = 0;
                }
            }

            if (keyboardActivated && Input.GetKeyDown(reloadButton) || joystickActivated && Input.GetKeyDown(controllerOrder + " button " + reloadButton))
            {
                if (akAmmo < heroData.ak47Ammo && GunSelected == 1 || m4Ammo < heroData.m4Ammo && GunSelected == 1 ||
                    stonerAmmo < heroData.stonerAmmo && GunSelected == 1|| g28Ammo < heroData.g28Ammo && GunSelected == 1 || 
                    awpAmmo < heroData.awpAmmo && GunSelected == 1 || scoutAmmo < heroData.scoutAmmo && GunSelected == 1 ||
                    shotgunAmmo < heroData.shotgunAmmo && GunSelected == 1 || mp5Ammo < heroData.mp5Ammo && GunSelected == 1 ||
                    mp40Ammo < heroData.mp40Ammo && GunSelected == 1 || bizonAmmo < heroData.bizonAmmo && GunSelected == 1 ||
                    glockAmmo < heroData.glockAmmo && GunSelected == 2 || coltAmmo < heroData.coltAmmo && GunSelected == 2 ||
                    p38Ammo < heroData.p38Ammo && GunSelected == 2 || coltAnaAmmo < heroData.coltAnaAmmo && GunSelected == 2 ||
                    socomAmmo < heroData.socomAmmo && GunSelected == 2)
                {
                    gunActiveControl = true;
                    anim.SetBool("Reloading_Gun", true);
                    anim.Play("Armature_Reloading_Gun", -1, 0f);
                    anim.SetLayerWeight(1, 1);
                    recoverAnimTime = 1f;
                    headIconUIReloading.gameObject.SetActive(true); // Ativa e passa a informação de qual icone ativar no headUI

                    if (GunSelected == 1 && !lastAmmoRifles)
                    {
                        ammoIconValueForRifle -= 77;
                    }
                    else if (GunSelected == 2 && !lastAmmoPistols)
                    {
                        ammoIconValueForPistol -= 77;
                    }

                    if (scopeState >= 1)
                    {
                        scopeState = 0;
                    }
                }                   
            }
        }

        //Quando a animação acaba a layer do mecanim volta ao normal
        if (anim.GetCurrentAnimatorStateInfo(1).normalizedTime > 1 && anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Reloading_Gun"))
        {
            if (recoverAnimTime > 0)
            {
                recoverAnimTime -= 12f * Time.deltaTime;
                anim.SetBool("Reloading_Gun", false);
            }
            else
            {
                gunActiveControl = false;
                recoverAnimTime = 0;
                headIconUIReloading.gameObject.SetActive(false); 

                //Recarrega as armas selecionadas
                if (rifleSelected == 0 && GunSelected == 1)
                {
                    akAmmo = heroData.ak47Ammo;
                }
                else if (rifleSelected == 1 && GunSelected == 1)
                {
                    m4Ammo = heroData.m4Ammo;
                }
                else if (rifleSelected == 2 && GunSelected == 1)
                {
                    stonerAmmo = heroData.stonerAmmo;
                }
                else if (rifleSelected == 3 && GunSelected == 1)
                {
                    g28Ammo = heroData.g28Ammo;
                }
                else if (rifleSelected == 4 && GunSelected == 1)
                {
                    awpAmmo = heroData.awpAmmo;
                }
                else if (rifleSelected == 5 && GunSelected == 1)
                {
                    scoutAmmo = heroData.scoutAmmo;
                }
                else if (rifleSelected == 6 && GunSelected == 1)
                {
                    shotgunAmmo = heroData.shotgunAmmo;
                }
                else if (rifleSelected == 7 && GunSelected == 1)
                {
                    mp5Ammo = heroData.mp5Ammo;
                }
                else if (rifleSelected == 8 && GunSelected == 1)
                {
                    mp40Ammo = heroData.mp40Ammo;
                }
                else if (rifleSelected == 9 && GunSelected == 1)
                {
                    bizonAmmo = heroData.bizonAmmo;
                }
                else if(pistolSelected == 0 && GunSelected == 2)
                {
                    glockAmmo = heroData.glockAmmo;
                }
                else if (pistolSelected == 1 && GunSelected == 2)
                {
                    coltAmmo = heroData.coltAmmo;
                }
                else if (pistolSelected == 2 && GunSelected == 2)
                {
                    p38Ammo = heroData.p38Ammo;
                }
                else if (pistolSelected == 3 && GunSelected == 2)
                {
                    coltAnaAmmo = heroData.coltAnaAmmo;
                }
                else if (pistolSelected == 4 && GunSelected == 2)
                {
                    socomAmmo = heroData.socomAmmo;
                }
            }

            anim.SetLayerWeight(1, recoverAnimTime);
        }
    }

    //Decremento da munição
    public void shootingGuns()
    {
        if(!anim.GetCurrentAnimatorStateInfo(1).IsName("Armature_Reloading_Gun") && !gunSniperAnim.GetCurrentAnimatorStateInfo(0).IsName("Sniper_Recoil"))
        {
            // Animação das armas de um tiro
            if (!rifles[4].activeInHierarchy || !rifles[5].activeInHierarchy || !rifles[6].activeInHierarchy)
            {
                gunAnim.SetBool("Shake", true);
                gunAnim.SetBool("Sniper_Recoil", false);
            }

            if (rifles[4].activeInHierarchy || rifles[5].activeInHierarchy || rifles[6].activeInHierarchy)
            {
                if (!isAwping)
                {
                    gunAnim.SetBool("Sniper_Recoil", true);
                   
                }

                gunAnim.SetBool("Shake", false);
            }            

            if (!gunActiveControl && GunSelected == 1 && !lastAmmoRifles)
            {
                //Ativando a mira da arma
                if (scopeState >= 1)
                {
                    if (!rifles[3].activeInHierarchy)
                    {
                        if (!isAwping)
                        {
                            gunSniperAnim.SetBool("Aim_Shake", true);
                            isAwping = true; //Pra atirar apenas uma vez de awp
                        }
                    }
                    else
                    {
                        gunSniperAnim.SetBool("Rifle_Aim_Scope", true);
                       
                    }
                }

                Transform[] fireSpawn = rifles[rifleSelected].GetComponentsInChildren<Transform>();
                foreach (Transform item in fireSpawn)
                {
                    if (item.name == "Fire_Spawn")
                    {
                        rifleParticle.transform.position = item.transform.position;
                        rifleParticle.transform.rotation = item.transform.rotation;
                    }
                }           
            }
            else if (!gunActiveControl && GunSelected == 2 && !lastAmmoPistols)
            {                
                gunAnimPistol.SetBool("Shake_Pistol", true);
                    
                Transform[] fireSpawn = pistols[pistolSelected].GetComponentsInChildren<Transform>();
                foreach (Transform item in fireSpawn)
                {
                    if (item.name == "Fire_Spawn")
                    {
                        pistolParticle.transform.position = item.transform.position;
                        pistolParticle.transform.rotation = item.transform.rotation;
                    }
                }
            }
            else if (!gunActiveControl && GunSelected == 3)
            {
                if (meleeGunSelected != 3 && meleeGunSelected != 4)
                {
                    timerMelee -= 1f * Time.deltaTime;
                }

                if (timerMelee <= 0)
                {
                    meleeAttackChoosed = Random.Range(1, 3);
                    timerMelee = 1;
                    print(meleeAttackChoosed);
                }
                if (meleeAttackChoosed == 1)
                {
                    anim.SetBool("Active_Melee_1", true);
                    anim.SetBool("Active_Melee_2", false);
                    anim.SetBool("Melee_Idle", false);
                }
                if (meleeAttackChoosed == 2)
                {
                    anim.SetBool("Active_Melee_1", false);
                    anim.SetBool("Active_Melee_2", true);
                    anim.SetBool("Melee_Idle", false);
                }
            }
        } 
    }

    public void RayToGuns()
    {
        RaycastHit hit;

        if (Physics.Raycast(rayGunsThirdMode.transform.position, rayGunsThirdMode.transform.up, out hit,300f,rayMaskForHands) && scopeState == 0)// O raycast das maos serve para identificar também qual objeto será afetado pelos tiros
        {
            if(hit.collider.tag == "Wall" || hit.collider.tag == "Grabber")
            {             
                Instantiate(decals[0], hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
                print("Third Mode");
            }
        }

        if (Physics.Raycast(rayGunsThirdMode.transform.position, rayGunsThirdMode.transform.up, out hit, 300f) && scopeState == 0)// O raycast das maos serve para identificar também qual objeto será afetado pelos tiros
        {
            //Nessa versão apenas o canvas de quem matou aparece a label de kill
            if (hit.collider.tag == "DamageBox")
            {
                if(GunSelected == 1)
                {                    
                    print("P1 sendo atingindo pela arma " + rifles[rifleSelected] + " do player " + this.gameObject.tag);
                    DamageBox dmgObj = hit.collider.GetComponent<DamageBox>();
                    dmgObj.playerTag = this.gameObject.tag;
                    dmgObj.gunType = "Rifle";
                    dmgObj.gunID = rifleSelected;

                    if (rifles[0].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.akDamage * damageMultiplier);
                    }
                    else if (rifles[1].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.m4Damage * damageMultiplier);
                    }
                    else if(rifles[2].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.stonerDamage * damageMultiplier);
                    }
                    else if(rifles[3].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.g28Damage * damageMultiplier);
                    }
                    else if(rifles[4].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.awpDamage * damageMultiplier);
                    }
                    else if(rifles[5].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.scoutDamage * damageMultiplier);
                    }
                    else if(rifles[6].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.shotgunDamage * damageMultiplier);
                    }
                    else if(rifles[7].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.mp5Damage * damageMultiplier);
                    }
                    else if(rifles[8].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.mp40Damage * damageMultiplier);
                    }
                    else if(rifles[9].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.bizonDamage * damageMultiplier);
                    }                  
                }
                else if(GunSelected == 2)
                {
                    print("P1 sendo atingindo pela arma " + pistols[pistolSelected] + " do player " + this.gameObject.tag);
                    DamageBox dmgObj = hit.collider.GetComponent<DamageBox>();
                    dmgObj.playerTag = this.gameObject.tag;
                    dmgObj.gunType = "Pistol";
                    dmgObj.gunID = pistolSelected;

                    print(dmgObj.heroScript.life + "//" +
                       missionScript.pistolMission + "//" + isInMission);

                    if (dmgObj.heroScript.life <= 25 && missionScript.pistolMission
                        && isInMission && !dmgObj.heroScript.dieAfterCheckPistolMission)//Missao pistola
                    {                       
                        missionScript.pistolKills++;                        
                        hit.collider.GetComponent<DamageBox>().heroScript.dieAfterCheckPistolMission = true;
                    }

                    if (pistols[0].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.glockDamage * damageMultiplier);
                    }
                    else if (pistols[1].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.coltDamage * damageMultiplier);
                    }
                    else if (pistols[2].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.p38Damage * damageMultiplier);
                    }
                    else if (pistols[3].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.coltAnaDamage * damageMultiplier);
                    }
                    else if (pistols[4].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.socomDamage * damageMultiplier);
                    }
                }                
            }
        }

        if (Physics.Raycast(cameraFirst.transform.position, cameraFirst.transform.forward, out hit, 300f, rayMaskForHands) && scopeState > 0)
        {
            if (hit.collider.tag == "Wall" || hit.collider.tag == "Grabber")
            {
                Instantiate(decals[0], hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
                print("First Mode");
            }
        }

        if (Physics.Raycast(cameraFirst.transform.position, cameraFirst.transform.forward, out hit, 300f) && changeCam && scopeState != 0)
        {
            //Nessa versão apenas o canvas de quem matou aparece a label de kill
            if (hit.collider.tag == "DamageBox")
            {
                if (GunSelected == 1)
                {
                    print("P1 sendo atingindo pela arma " + rifles[rifleSelected] + " do player " + this.gameObject.tag);
                    DamageBox dmgObj = hit.collider.GetComponent<DamageBox>();
                    dmgObj.playerTag = this.gameObject.tag;
                    dmgObj.gunType = "Rifle";
                    dmgObj.gunID = rifleSelected;

                    if (rifles[0].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.akDamage * damageMultiplier);
                    }
                    else if (rifles[1].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.m4Damage * damageMultiplier);
                    }
                    else if (rifles[2].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.stonerDamage * damageMultiplier);
                    }
                    else if (rifles[3].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.g28Damage * damageMultiplier);
                    }
                    else if (rifles[4].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.awpDamage * damageMultiplier);
                    }
                    else if (rifles[5].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.scoutDamage * damageMultiplier);
                    }
                    else if (rifles[6].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.shotgunDamage * damageMultiplier);
                    }
                    else if (rifles[7].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.mp5Damage * damageMultiplier);
                    }
                    else if (rifles[8].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.mp40Damage * damageMultiplier);
                    }
                    else if (rifles[9].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.bizonDamage * damageMultiplier);
                    }
                }
                else if (GunSelected == 2)
                {
                    print("P1 sendo atingindo pela arma " + pistols[pistolSelected] + " do player " + this.gameObject.tag);
                    DamageBox dmgObj = hit.collider.GetComponent<DamageBox>();
                    dmgObj.playerTag = this.gameObject.tag;
                    dmgObj.gunType = "Pistol";
                    dmgObj.gunID = pistolSelected;

                    print(dmgObj.heroScript.life + "//" +
                       missionScript.pistolMission + "//" + isInMission);

                    if (dmgObj.heroScript.life <= 25 && missionScript.pistolMission
                        && isInMission && !dmgObj.heroScript.dieAfterCheckPistolMission)//Missao pistola
                    {
                        missionScript.pistolKills++;
                        hit.collider.GetComponent<DamageBox>().heroScript.dieAfterCheckPistolMission = true;
                    }

                    if (pistols[0].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.glockDamage * damageMultiplier);
                    }
                    else if (pistols[1].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.coltDamage * damageMultiplier);
                    }
                    else if (pistols[2].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.p38Damage * damageMultiplier);
                    }
                    else if (pistols[3].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.coltAnaDamage * damageMultiplier);
                    }
                    else if (pistols[4].gameObject.activeInHierarchy)
                    {
                        hit.collider.GetComponent<DamageBox>().TakeDamage(heroData.socomDamage * damageMultiplier);
                    }
                }
            }
        }

        if (Physics.Raycast(rayGunsThirdMode.transform.position, rayGunsThirdMode.transform.up, out hit, 300f,raycastDamage) && scopeState == 0)// O raycast das maos serve para identificar também qual objeto será afetado pelos tiros
        {
            if (hit.collider.tag == "ZombieStan")
            {
                HitboxStandardZombie zombieHitbox = hit.collider.gameObject.GetComponent<HitboxStandardZombie>();               

                if (rifles[0].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.akDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[1].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.m4Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[2].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.stonerDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[3].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.g28Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[4].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.awpDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[5].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.scoutDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[6].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.shotgunDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[7].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.mp5Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[8].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.mp40Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[9].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.bizonDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }

                else if (pistols[0].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.glockDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[1].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.coltDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[2].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.p38Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[3].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.coltAnaAmmo;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[4].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.socomDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
            }

            if(hit.collider.tag == "SpecialEnemyBigZ" && scopeState == 0)
            {
                BigZ specialBigZ = hit.collider.gameObject.GetComponent<BigZ>();

                if (rifles[0].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.akDamage;                   
                }
                else if (rifles[1].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.m4Damage;
                }
                else if (rifles[2].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.stonerDamage;
                }
                else if(rifles[3].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.g28Damage;
                }
                else if (rifles[4].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.awpDamage;
                }
                else if(rifles[5].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.scoutDamage;
                }
                else if(rifles[6].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.shotgunDamage;
                }
                else if(rifles[7].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.mp5Damage;
                }
                else if(rifles[8].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.mp40Damage;
                }
                else if(rifles[9].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.bizonDamage;
                }

                else if (pistols[0].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.glockDamage;
                }
                else if (pistols[1].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.coltDamage;
                }
                else if (pistols[2].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.p38Damage;
                }
                else if (pistols[3].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.coltAnaDamage;
                }
                else if (pistols[4].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.socomDamage;
                }
            }

            if (hit.collider.tag == "ShieldBigZ")
            {
                GameObject decal = Instantiate(decals[0], hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
                decal.transform.parent = hit.collider.transform;
            }
        }

        if (Physics.Raycast(cameraFirst.transform.position, cameraFirst.transform.forward, out hit, 300f, raycastDamage) && changeCam && scopeState != 0)
        {
            if (hit.collider.tag == "ZombieStan")
            {
                HitboxStandardZombie zombieHitbox = hit.collider.gameObject.GetComponent<HitboxStandardZombie>();
                //print(hit.collider.name);                

                if (rifles[0].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.akDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[1].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.m4Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[2].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.stonerDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[3].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.g28Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[4].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.awpDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[5].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.scoutDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[6].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.shotgunDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[7].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.mp5Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[8].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.mp40Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }
                else if (rifles[9].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.bizonDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();
                }

                else if (pistols[0].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.glockDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[1].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.coltDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[2].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.p38Damage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[3].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.coltAnaAmmo;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
                else if (pistols[4].gameObject.activeInHierarchy)
                {
                    zombieHitbox.damageValue = heroData.socomDamage;
                    zombieHitbox.whoShooting = this.gameObject;
                    zombieHitbox.ReceiveDamage();

                    if (zombieHitbox.zombieScript.lifeEnemy <= 0 && isInMission && missionScript.pistolMission &&
                        !zombieHitbox.zombieScript.pistolMission)
                    {
                        missionScript = GameObject.Find("Mission Canvas").GetComponent<MissionScript>();
                        missionScript.pistolKills++;
                        zombieHitbox.zombieScript.pistolMission = true;
                    }
                }
            }

            if (hit.collider.tag == "SpecialEnemyBigZ" && scopeState == 0)
            {
                BigZ specialBigZ = hit.collider.gameObject.GetComponent<BigZ>();

                if (rifles[0].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.akDamage;
                }
                else if (rifles[1].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.m4Damage;
                }
                else if (rifles[2].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.stonerDamage;
                }
                else if (rifles[3].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.g28Damage;
                }
                else if (rifles[4].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.awpDamage;
                }
                else if (rifles[5].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.scoutDamage;
                }
                else if (rifles[6].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.shotgunDamage;
                }
                else if (rifles[7].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.mp5Damage;
                }
                else if (rifles[8].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.mp40Damage;
                }
                else if (rifles[9].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.bizonDamage;
                }

                else if (pistols[0].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.glockDamage;
                }
                else if (pistols[1].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.coltDamage;
                }
                else if (pistols[2].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.p38Damage;
                }
                else if (pistols[3].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.coltAnaDamage;
                }
                else if (pistols[4].gameObject.activeInHierarchy)
                {
                    specialBigZ.lifeBigZ -= heroData.socomDamage;
                }
            }

            if (hit.collider.tag == "ShieldBigZ")
            {
                GameObject decal = Instantiate(decals[0], hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
                decal.transform.parent = hit.collider.transform;
            }
        }
    }

    public void UiElements()
    {
        //Valor de munição dentro da interface
        if (rifleSelected == 0 && GunSelected == 1)
        {                     
           bulletCounter.text = akAmmo + "<color=orange>/" + heroData.ak47Ammo + "</color>";
        }
        else if (rifleSelected == 1 && GunSelected == 1)
        {
            bulletCounter.text = m4Ammo + "<color=orange>/" + heroData.m4Ammo + "</color>";
        }
        else if (rifleSelected == 2 && GunSelected == 1)
        {
            bulletCounter.text = stonerAmmo + "<color=orange>/" + heroData.stonerAmmo + "</color>";
        }
        else if (rifleSelected == 3 && GunSelected == 1)
        {
            bulletCounter.text = g28Ammo + "<color=orange>/" + heroData.g28Ammo + "</color>";
        }
        else if (rifleSelected == 4 && GunSelected == 1)
        {
            bulletCounter.text = awpAmmo + "<color=orange>/" + heroData.awpAmmo + "</color>";
        }
        else if (rifleSelected == 5 && GunSelected == 1)
        {
            bulletCounter.text = scoutAmmo + "<color=orange>/" + heroData.scoutAmmo + "</color>";
        }
        else if (rifleSelected == 6 && GunSelected == 1)
        {
            bulletCounter.text = shotgunAmmo + "<color=orange>/" + heroData.shotgunAmmo + "</color>";
        }
        else if (rifleSelected == 7 && GunSelected == 1)
        {
            bulletCounter.text = mp5Ammo + "<color=orange>/" + heroData.mp5Ammo + "</color>";
        }
        else if (rifleSelected == 8 && GunSelected == 1)
        {
            bulletCounter.text = mp40Ammo + "<color=orange>/" + heroData.mp40Ammo + "</color>";
        }
        else if (rifleSelected == 9 && GunSelected == 1)
        {
            bulletCounter.text = bizonAmmo + "<color=orange>/" + heroData.bizonAmmo + "</color>";
        }
        else if (pistolSelected == 0 && GunSelected == 2)
        {
           bulletCounter.text = glockAmmo + "<color=orange>/" + heroData.glockAmmo + "</color>";
        }
        else if (pistolSelected == 1 && GunSelected == 2)
        {
            bulletCounter.text = coltAmmo + "<color=orange>/" + heroData.coltAmmo + "</color>";
        }
        else if (pistolSelected == 2 && GunSelected == 2)
        {
            bulletCounter.text = p38Ammo + "<color=orange>/" + heroData.p38Ammo + "</color>";
        }
        else if (pistolSelected == 3 && GunSelected == 2)
        {
            bulletCounter.text = coltAnaAmmo + "<color=orange>/" + heroData.coltAnaAmmo + "</color>";
        }
        else if (pistolSelected == 4 && GunSelected == 2)
        {
            bulletCounter.text = socomAmmo + "<color=orange>/" + heroData.socomAmmo + "</color>";
        }

        //Troca de cor dos icones
        if (GunSelected == 1)
        {
            //cinza (55, 55, 55, 255)
            rifleUI.color = new Color32(242, 163, 46, 255); //laranja
            pistolUI.color = new Color32(77, 0, 0, 150); //vermelho 
            meleeUI.color = new Color32(77, 0, 0, 150); //vermelho 
            grenadeUI.color = new Color32(77, 0, 0, 150); //vermelho
            ammoIcon.gameObject.SetActive(true);
            ammoIcon.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ammoIconValueForRifle);
        }
        else if (GunSelected == 2)
        {
            rifleUI.color = new Color32(77, 0, 0, 150); //vermelho 
            pistolUI.color = new Color32(242, 163, 46, 255); //laranja
            meleeUI.color = new Color32(77, 0, 0, 150); //vermelho 
            grenadeUI.color = new Color32(77, 0, 0, 150); //vermelho   
            ammoIcon.gameObject.SetActive(true);
            ammoIcon.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ammoIconValueForPistol);
        }
        else if (GunSelected == 3)
        {
            rifleUI.color = new Color32(77, 0, 0, 150); //vermelho 
            pistolUI.color = new Color32(77, 0, 0, 150); //vermelho 
            meleeUI.color = new Color32(242, 163, 46, 255); //laranja
            grenadeUI.color = new Color32(77, 0, 0, 150); //vermelho 
            bulletCounter.text = "infinite";
            ammoIcon.gameObject.SetActive(false);
        }
        else if (GunSelected == 4)
        {
            rifleUI.color = new Color32(77, 0, 0, 150); //vermelho 
            pistolUI.color = new Color32(77, 0, 0, 150); //vermelho 
            meleeUI.color = new Color32(77, 0, 0, 150); //vermelho 
            grenadeUI.color = new Color32(242, 163, 46, 255); //laranja
            bulletCounter.text = grenadeLimit + "<color=orange>/" + heroData.grenadeQuant + "</color>";
            ammoIcon.gameObject.SetActive(false);
        }
    }

    public void ActiveCamera(string xAxis, string yAxis)
    {
        if(keyboardActivated && Input.GetMouseButtonDown(1) || joystickActivated && Input.GetKeyDown(controllerOrder + " button " + cameraSelectedButton)) 
        {
            changeCam = !changeCam;

            if(changeCam && isInMission && missionScript.fpmMission == true)//Missao de nao usar a camera FP
            {
                missionScript.missionFailed = true;
            }
        }

        if (changeCam)
        {
            cameraTop.SetActive(false);
            cameraThird.SetActive(true);
            gunAnimGameobject.transform.position = new Vector3(0.1f, 0.1f, 0.1f);          

            //Shake do Rifle (Só ativa quando a arma não está recarregando)                              
            anim.SetFloat("Speed", 0);
            anim.SetBool("Normal_RunWalk", false);
            anim.SetBool("Run_and_Stop", true);

            //Ativa a animação automatica de mira
            anim.SetBool("Aim_Crouch", false);
            anim.SetBool("Aim_Walk", false);
            anim.SetBool("Aim_Stop", true);
            anim.SetBool("Aim_Run", false);
            anim.SetBool("Idle", false);
            gunAnimPistol.SetBool("Shake_Pistol", false);

            //Aqui a camera e o player são manipulados pelo mouse. A camera tem a posição em Y modificada por aqui
            axisX += Input.GetAxis(xAxis) * 2;
            axisY += Input.GetAxis(yAxis) * 2;
            axisYToCamPos += Input.GetAxis(yAxis) * 0.5f;
            axisYToGrenadePos += Input.GetAxis(yAxis) * 0.05f;

            float limitYPos = Mathf.Clamp(axisYToCamPos, 0, 10); // Limita posição em Y da camera
            float limitYGrenadePos = Mathf.Clamp(axisYToGrenadePos, -1.15f, 0.8f); // Limita posição em Y da granada
            //float limitZRot = Mathf.Clamp(axisY, -30, 30); // Limita rotação em Z das armas   

            if (axisYToCamPos >= 20)
            {
                axisYToCamPos = 20;
            }

            if (axisYToCamPos <= -10)
            {
                axisYToCamPos = -10;
            }

            if(axisY >= 70)
            {
                axisY = 70;
            }

            if(axisY <= -30)
            {
                axisY = -30;
            }

            if (axisYToGrenadePos >= 0.8)
            {
                axisYToGrenadePos = 0.8f;
            }

            if (axisYToGrenadePos <= -1.15)
            {
                axisYToGrenadePos = -1.15f;
            }

            if (rifles[3].activeInHierarchy || rifles[4].activeInHierarchy || rifles[5].activeInHierarchy)
            {
                if (Input.GetMouseButtonDown(2) || joystickActivated && Input.GetKeyDown(controllerOrder + " button " + aimCameraButton))
                {
                    scopeState += 1;
                }

                if (scopeState > 2)
                {
                    scopeState = 0;
                }

                if (scopeState == 0)
                {
                    cameraFirst.gameObject.SetActive(false);
                    cameraThird.SetActive(true);
                    canvs.worldCamera = cameraThird.GetComponent<Camera>();
                    canvsKill.worldCamera = cameraThird.GetComponent<Camera>();
                    //aimFirstPersonTexture.SetActive(false);                    
                }
                else if (scopeState == 1)
                {
                    cameraFirst.gameObject.SetActive(true);
                    cameraThird.SetActive(false);
                    canvs.worldCamera = cameraFirst.GetComponent<Camera>();
                    canvsKill.worldCamera = cameraFirst.GetComponent<Camera>();
                    //aimFirstPersonTexture.SetActive(true);
                    cameraFirst.fieldOfView = 45;                   
                }
                else if (scopeState == 2)
                {
                    cameraFirst.gameObject.SetActive(true);
                    cameraThird.SetActive(false);
                    canvs.worldCamera = cameraFirst.GetComponent<Camera>();
                    canvsKill.worldCamera = cameraFirst.GetComponent<Camera>();
                    cameraFirst.fieldOfView = 30;                    
                }
            }

            //cameraThird.transform.LookAt(lookPoint.transform.position, Vector3.up);
            cameraThird.transform.localEulerAngles = new Vector3(axisYToCamPos, cameraThird.transform.localEulerAngles.y, cameraThird.transform.localEulerAngles.z);
            //rifleIkObject.transform.localEulerAngles = new Vector3(rifleIkObject.transform.localEulerAngles.x, rifleIkObject.transform.localEulerAngles.y, -axisY);

            //cameraFirst.gameObject.transform.LookAt(lookPoint.transform.position, Vector3.up);

            cameraFirst.transform.localEulerAngles = new Vector3(axisY, transform.localEulerAngles.x, cameraFirst.transform.localEulerAngles.z);

            rifleIkObject.transform.localEulerAngles = new Vector3(rifleIkObject.transform.localEulerAngles.x, rifleIkObject.transform.localEulerAngles.y, -axisY);
            pistolIkObject.transform.localEulerAngles = new Vector3(pistolIkObject.transform.localEulerAngles.x, pistolIkObject.transform.localEulerAngles.y, -axisY);
            grenadeIkObject.transform.localPosition = new Vector3(axisYToGrenadePos, grenadeIkObject.transform.localPosition.y, grenadeIkObject.transform.localPosition.z);

            transform.localEulerAngles = Vector3.up * axisX;

            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            scopeState = 0;
            cameraTop.SetActive(true);
            cameraThird.SetActive(false);
            cameraFirst.gameObject.SetActive(false);
            //aimFirstPersonTexture.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            gunAnimGameobject.transform.position = new Vector3(0, 0, 0);
            canvs.worldCamera = cameraTop.GetComponent<Camera>();
            canvsKill.worldCamera = cameraTop.GetComponent<Camera>();

            if (pistols[pistolSelected].activeInHierarchy)//Para a pistola ficar com a rotação correta
            {
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Stay_Aim") || anim.GetCurrentAnimatorStateInfo(0).IsName("Rifle_Aim_Crouch_Tree"))
                {
                    pistolIkObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
                }
                else
                {
                    pistolIkObject.transform.localRotation = Quaternion.Euler(0, 0, -10);
                }
            }
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if(other.tag == "Smoke")
        {
            ID smokeID = other.gameObject.GetComponent<ID>();
            oponentTag = smokeID.objID;
            enemyGunType = "Utilities";
            enemyGunID = 0; //ID da smoke na array de icones
            flipFaceSmoke = true;
            isRecovering = false;
            faceUIScript.actualStatus = "Poisoned";
            headIconsUI.gameObject.SetActive(true);
            lifeUpParticle.SetActive(false);

            if(faceUIScript.actualStatus == "Poisoned" && 
                GameObject.FindWithTag(smokeID.objID).GetComponent<HeroBehavior>().isInMission && 
                missionScript.throwGrenadeMission && !breathPoison)//Missao da granada
            {
                missionScript.grenadesEnemyCount++;
            }
        }
    }
}
