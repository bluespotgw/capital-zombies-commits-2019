﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagDoll : MonoBehaviour
{
    //public heroID heroName;
    //public GameObject[] heroModel;
    public GameObject ragdollCam;
    public GameObject zombieHead;
    public GameObject tieCanvas;
    //public GameObject rootRagdoll;
    public float timeToRevive = 0;
    GameObject gameDirector;
    GameDirector directorScript;
    public bool singlePlayer = false;//Usa essa variavel para empate tambem

    /*public enum heroID
    {
        Jack,Julie,Jimmy,Josh
    }*/

    void Start()
    {
        ragdollCam.transform.rotation = Quaternion.identity;
        gameDirector = GameObject.Find("Director");
        directorScript = gameDirector.GetComponent<GameDirector>();
        timeToRevive = directorScript.reviveTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (!singlePlayer)
        {
            timeToRevive -= Time.deltaTime;

            if (timeToRevive <= 0)
            {
                directorScript.Spawning_NewPlayer(this.gameObject.tag);
                Destroy(this.transform.parent.gameObject);

                /*switch (heroName)
                {
                    case heroID.Jack:
                        Instantiate(heroModel[0], this.transform.position, Quaternion.identity);
                        Destroy(rootRagdoll.gameObject);
                        break;
                    default:
                        break;
                }*/
            }
        }        
    }
}
